# Presentation Draft
## General
        * Trial-run live in meeting
        * 10 minutes max.
        * 5 presenters
        * Audience: OOPP lecturers
        * Goal: convincing audience that our product should be picked and used by TU Delft for future lectures.
        
## Content
*Pay attention that this presentation is mainly focussed on presenting our **product** as if we're trying to sell it, so focus on what makes our application different from the other applications.*

 

* Introduction
    * Give a small introduction of the project and group 63.
    * *30 sec* **name:** Michelle 


* Brief overview of process 
    * Finding requirements, design, development, testing
    * *60 sec* **name:** Tan

* Showcase of the resulting product *(most important)*
    * Make sure you show the features that make your application unique.
        * See script
        * Show whatever you can show in the amount of time you get
    * *7 minutes* **name:** Michelle, Tan, Matheusz, Ivan, Juul


* Possible improvements for the future
    * Show what can be improved in the feature, take into consideration what is stated in the Design Document.
    * *60 sec* **name:** Juul

* Closing
    * 15-30 seconds
    * *30 sec* **name:** Ivan











# Script

## Introduction - 30 sec (Michelle)
*Give a small introduction of the project and group 63.*

## Overview - 60 sec (Tan)
The application process first started with meetings with the client. Based on the needs of the users, we have come up with a design that best fits the requirements as well as bring the best learning experience to both the teachers and learners. 

Beside the design process that only give the pleasant look, our application is also based on researches about ethics values and HCI so that we can understand better about the both the user and non-so-obvious stakeholder. In a nut shell, the applicaton is well designed for all potential users. 

The application process spread out evenly in 5 weeks/ sprints. That in each sprint, the application will be implemented part by part, modulely, so that we as the developers can follow and understand how the progress is going. 

In the final phase, the application will be tested fully. With the client side, we have document test to show how the GUI is used. And the logic in the server side will be tested with JUNIT tests. 

Brief overview of process 
*How did we came up with the requirements? Refer to the client TA and how we came up with questions etc.
How did our design process go? Briefly refer to the included design document (more on that in a later part of the presentation)
How did our development process go (dividing issues etc)?
How did we tested our application properly*

## Showing product - 7 mins
Showcase of the resulting product *(most important)* 
*Plan: show the whole application and as many features as possible in a 7 min video. Per stage there will be a student and moderator.*


### Stage 1: (1 minutes) 
Creating + Scheduling the lecture (share invite link), scheduled lectures (for the future).
**Moderator/Lecturer role: Juul
Student role: Michelle**
* Lecturer opens application
* Lecturer creates a lecture in the future and clicks create lecture
* File Explorer will open and the lecturer saves the lecture .txt file locally. 
* The lecturer will share the invite links
* The student will copy the shared link and the application will be opened automatically (launched by pasting the invite link in Chrome, link Zoom)
* The student cannot access the lecture (pop-up will show)
* At the scheduled day the lecturer will open the application and get into the lecture room 
* The lecture will copy the moderator link and send it privatly to a moderator
*  The student will try to get into the lecture with the copied link again
*  The student can enter the lecture by inserting his/her name. This immediately directs the student to the main page.

### Stage 2 (1.5 minutes)
Creating a poll , ending it (showing statistics)
**Moderator/Lecturer role: Michelle
Student role: Juul**

Stage 4.1: 
* The moderator sees that questions have previously been insensible (questions need editing and students need banning)
* The moderator decides to create a poll to check everyone's understanding up to this point.
* The moderator creates a poll with 5 options and 2 answers are marked as correct. Deletes one of the options as it was confusing. The poll is published.

Stage 4.2: 
* The student sees the poll options and struggles to identify the correct answer. 
* The student clicks one of the options
*  The moderator recieves the updated statistics of responses from students.
* The moderator sees that students performed well (based on the statistics) and closes the poll
* The student realises the poll is closed and sees whether they answered correctly and statistics from the whole class.
* The moderator is happy and closes the poll.
* The poll disappears from both the student and moderator screen. 



### Stage 3 (2 minutes)
Speed poll, upvote, Ask a question + response by text
**Moderator/Lecturer role: Ivan**
**Student role: Tan**

Stage 2.1:
* For both student/moderator, the main page is open (state that list of questions is empty at beginning of a lecture).
* The moderator/student checks the participants in the lecture. 
* The student asks a question (with title and content). 
* The moderator sees the question: moderator claims the question and writes a written response. State that you can only write a response after you have claimed a question so that there won't be any duplicate work. 
* The student sees the answer on their screen.

Stage 2.2: 
* There are several questions in the lecture
* Some questions have more upvotes than other (the upvote numbers should be different)
* Students start upvoting the good questions.
* The moderator filters by unanswered
* The student filters by answered to check the answers
* The moderator filters by relevancy to see the most popular questions
* The top relevant question receives a verbal answer in the lecture.
* The moderator marks the questions as answered (no written response).

Stage 2.3:
* The student thinks that the lecture is going too slow. She/He clicks on the speed button to inform the lecturer
* The lecturer sees the updated speed statistics.
* The lecturer switches to presentation mode to focus better and speak more quickly. Describe the features that are offered by presentation mode. 
* The lecturer switches back to normal mode.
* The stats have been cleared after 5 minutes.
* The student now thinks that the lecturer is going too slow and clicks the slow down button
* The lecturer sees new stats and slows down. 


### Stage 4 (45 seconds)
banning, editing, deleting
**Moderator/Lecturer role: Mateusz
Student role: Ivan**

Stage 3.1
* The moderator clicks on options drop-down menu on question-tile
* The moderator clicks "banning" and bans that student
* The student banned tries to get into lecture but cannot enter anymore

Stage 3.2
* The moderator clicks on options drop-down menu on question-tile
* The moderator clicks "editing" 
* The moderator edits a question and submits
* The student/moderator will see the edit immediately 

Stage 3.3
* The moderator clicks on option drop-down menu on question-tile
* The moderator clicks "delete"
* The question will be deleted
* The student clicks on another student's question
* A pop-up shows "you cannot delete another student's question"
* The student clicks on his own delete question button 
* The question is deleted on both student/moderator side


### Stage 5 (1.5 minutes)
Exporting questions, archive of questions.
**Moderator/Lecturer role: Mateusz
Student role: Ivan**


* The lecturer ends the lecture 
* The student can't ask a question anymore
* The lecturer exports the questions from the lecture
* The admin/lecturer closes the application and checks the event log to see what happened throughout the whole process.
* The admin restarts the server
* (Since that h2 is made locally persistent) The lecturer restarts the application and the data from the previous lecture still exists.
* The lecturer lost the previous file of questions and searches for it in the question archive by filling in a valid moderator link.
* The lecturer browses again to find even more past questions
* The lecturer exports the files once more to upload it to Bright Space. 

## Improvements - 1 min (Juul)

During the development phase we set-up a design document. This document includes a value sensitive design as well as human-computer interaction report. With the data from these  reports we can derive some improvements for the future.

*Value sensitive design* 
In the value-sensitive design we mainly focussed on dyslexic students that are not always taken into consideration while designing an application. To make the application more accessible for them we could include a presentation mode to the student homepage. This will allow dyslexic student to focus better in the lecture by providing larger text, highlighting tools, and less questions.

*HCI report*
To make our interface adhere even more to the basic principles of HCI we could include the following features according to the results of our HCI-report: 
-	Making sure users can undo their actions in the application
o	Undo upvoting a question
o	Undo editing a question
o	Undo banning a user
o	Undo deleting their question
-	Add a ‘back’ button on the entrance screen.
-	Add documentation that allows users to see how they can use the application.



## Closing - 30 sec  (Ivan)
*Thank the lecturers for listening, close-up, etc.* 
    









