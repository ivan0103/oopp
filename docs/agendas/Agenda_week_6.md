# Agenda week 6
Date:           Tuesday, 16 march 2021\
Main focus:     What to do for the application\
Chair:          Juul Schnitzler\
Note taker:     Michelle Chao Chen

### General remarks
* How does everybody think it is going in general?
    * Is the work divided equally?
    * Are we behind or on schedule? 
    * Are there improvements possible?

* Discussion yesterday
    * How to prevent this
    * Communication should be better
    * Plan a meeting when adjusting a feature which results in major adjustments in all codes.

## 1. Points of action

- **Application**
    - Are we finished with all the must haves?
    - Can we divide the should haves? 

- **Division**
    - Need to divide the issues further (mostly must haves)
    - Need to make sure to divide the backend and frontend in a proper way (people who have not worked on frontend should now work on backend and the other way around)

- **CTA meeting**
    - Who will present the application and the features we have implemented until now?
    - Anything we want from the application

## 2. Coming up
- Second BuddyCheck deadline coming friday
- Advise: 50% completion in week 7 (basic system + as much should haves as possible)

## 3. Questions for the TA

* Any questions for the TA?

## 4. Question round

* Are there any questions? 
