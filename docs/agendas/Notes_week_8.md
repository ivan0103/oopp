# Notes week 8


## Last week
We fixed some bugs that were still there. People finished up their assigned issues. What do we think of our progress?
We’ve got big things done, but there is also some big things left. 

## What do we need to finish
* Wrapping up things
    * Jan Jaaps issues 
    * Should haves
* ***Deadline**: finishing all features this Friday*
* Testing
    * We might be a bit behind schedule on testing
    * We should make more measurable tasks divisions about this
    * Make sure we cover all the code while testing
    * Testing data: we need to add @Transactonal before every test
    * How much test coverage
        * Test entire server side
        * If time left: write manual test for UI (you click this button, this should happen)
        * ***Deadline**: finishing all features this Friday*

* Documentation
    * HCI: 
        * Not finished yet
        * Michelle made a merge request for seeing the progress
        * Progress so far:
            * Wrote the document so far until the results 
            * Suzanne: that amount of work is not suitable for one person 
            * Juul will help Michelle out
            * Matheuz will also help

    * Ethics:
        * Is uploaded to gitlab 
        * Make another merge request so people can give feedback
        
* Presentation
    * Assignment tab with draft presentation
    * Follow draft presentation
    * We will do the live presentation (on Tuesday)


## Jan Jaap
The project should be doable for 5 people, you should be able to finish the entire product even without Jan Jaap. The issues that were assigned to him have to be distributed over the rest of the group. The biggest issue Jan Jaap had left, was scheduling a lecture ahead. Most of his issues were closed. 

## Presentation draft
You can hand in a presentation draft this Friday, or we can do a presentation next Tuesday. Instead of having a video on brightspace. We dicided to do the presentation next tuesday due time. Juul will divide the presentation into 5 equal parts, everybody can choose their own part.  

## Points by Suzanne:

### General
If you still want to do the video, move the deadline of finishing all to Sunday/Monday. Friday is not very realistic for this deadline, but we can estimate best yourself. This Friday is the last deadline for the third buddycheck and final deadline for Code of Conduct. Next Friday is the big deadline for code and documents. In week 10 on Thursday or we have a Q&A with questions about the product, you have to be available. Do you guys think that we have 75% of the application done? Depends if we include testing, is included and that means we are not there yet… 

### Feedback code
Amount of code reviewing that is done could still be higher. It is not done by everyone. Compared to other groups they are reviewing a lot more. Big merge requests with a lot of commits are not properly reviewed. Testing is our biggest concern right now. Michelle closed some issues that were assigned to Jan Jaap, there are still some open, what will we do with them? We should divide them. Sometimes there is more changed that can be read from the commit message, might be bit unclear for the reader/reviewer. Thus the commit messages should include everything that is changed. Attributes of the lecturePageController are not being used, delete them is not needed, provides cleaner code. Lot of methods still do not have JavaDoc. 

## Conclusion 
This week:
1. **Friday**
    - DEADLINE: Finishing your own issues
    - DEADLINE: Finishing testing of your own issues
    - DEADLINE: Finish HCI and Ethics report 
    - Deadline: BuddyCheck 3
    - Code of Conduct (already handed in)
2. **Saturday**
    - Progress meeting 
3. **Tuesday**
    - Finish presentation draft

