## Example agenda

This is a template agenda. It gives an overview of what could be in your weekly agenda.
In the 'Points of action' part you will also find some topics to cover in/after your first meeting. 

---

Date:       23/02/2021   
Main focus: Discussion about working strategies as a team and completing documents\ Chair:      Tan Nguyen\
Note taker: Ivan Virovski  

In italics you will find some additional explanations of the agenda points. You will find mostly the same points in the next agendas.

# Opening
- Quick updates from TA if the TA is here :) 

# Approval of the agenda
*Make sure everything that needs to be discussed is in the agenda or add it if something is missing.*

# Points of action
*The items below are things you should look into after the first meeting. During the meeting you can divide (some of) the work between the team members, so that everybody has something to do afterwards.*
 - Inform everyone about what have been done:
    + Code of Conduct
    + Application Prototype
    + Draft Backlog

 - Inform everyone about what are still need to be done. 
    # Note down the opinions
    + Draft Design Document (Brightspace Assignment - DEADLINE 15 March 2021):
        + HCI Report 
            + In order to complete this report, a detailed instruction is needed to guide the experts (In our case, student from other grous).
                + Make a clear instruction: step by step to guide what should the experts do. 
                + Determine what Heuristics are they using. 
            + How to collect data:
                + Google form?
                + Ask them to give us a pdf reports?
                + .. ?
        + Responsible Computer Science Report 
            + This will be a table reviewing the stakeholders. 
            + Find 8 stakeholders (at least) including direct and indirect. Determine one stakeholder (non-obvious) to focus on. 
            + Decides 3 ethical values? Why these values? Each values has 3 norms
    + Getting feedbacks from CTA about the draft backlogs
    + Making backlogs issues on Gitlab. 

 - Remind everyone about the Information Literacy Deadline (26 Friday 2021)
    # Note down if someone has not finished that yet.

 - Discuss about the strategy how to do the software as a team. 
    # Note down the opinions of the members
    + Meet at every tuesday to discuss the about the works of each members
    + Emergency meeting at Thursday 
    + Rush meeting at Sunday 
    + Assigning tasks/ features at every tuesdays if the features are not yet claimed by any members
    + How many approval is needed to merge on gitlab
    + Discuss about the strategy to code as a team: 
        + Strategy 1 - GUI First
            + Option 1: 
                + First build the GUI structure/ skeleton of the project together
                + Then, Distributing the tasks and features.
            + Option 2: Divides the team into 3 smaller groups (Each group has 2 members),
                + One group will design the Student GUI
                + One group will design the Moderator GUI/ Lecturer
                + One group will design the GUI for splash screen
                + Then, Distributing the tasks and features

            + When the basic GUI is completed, features will be added. 

        + Strategy 2 - Structure only
            + Option 1: 
                + First build the structure containing the features, empty methods that need to be implemented
                + Then, Distributing the task for team members
            + Option 2: Divides the team into 3 smaller groups (Each group has 2 members), 
                + One group will design the Student structure
                + One group will design the Moderator/ Lecturer structure
                + One group will design the structure for splash screen
                + Then, Distributing the tasks and features
            + When the basic code structure is completed, Features and GUI will be added
        + other strategies ..?
 - Things to research:
	 - Scrum (see lecture 1) 
        - How are you going to use scrum in the team?
    - Git (see lecture 2)
        - How does it work?
        - What is branching?
    - Gradle (see lecture 4)
        - What does it do?
        - How do you add a library to the build file?
    - Code structure (see lecture 4)
        - How will you use packages?
        - What will your code structure look like? (UML)
     - GUI
        - OpenFX
     - IDE
        - Eclipse
        - IntelliJ
    - Spring & Testing (see lecture 8)

# Action points for next week (Scrum board)
 - Get feedbacks from the CTA
 - Ask other groups to do the HCI reports for our prototype
 - Interview stakeholders and make the reports for Reponsible Computer Science
 - Make issues backlog on gitlab
 - Research necessary information for the project. 
 - Practice git on your own git repository 

# Any other business
- Quesions?
- Informs from members? sick, off for holidays, etc. 

# Questions for the TA

 - What strategy did the TA's group use last year?
 - How did the team member decide to distribute the tasks?
    + The separated feature will be implemented first, then to the feature which can be reused many times?
# Question round
*If there are any questions, now is the time to ask them.*

# Closing
*Now you can start working on the project. Good luck!*
