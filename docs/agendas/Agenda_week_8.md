# Agenda for Week 8

Date: Tuesday, 30 March 2021\
Main Focus: Wrapping up the project\
Chair: Michelle Chao Chen\
Note Taker: Juul Schnitzler

## General Remarks
* Enhancements have been implemented and bugs from last week have been fixed
* Design Document is due
* Tests have not been implemented 


## Question for Suzanne
* How does having one less member in the group affect this project?
* How are we assessed on whether a feature is actually implemented? (presentation or code)
* How is the design document assessed?
* How much test coverage do we need to have?

## Points of Action
* **Wrap up** the project: features (the complete application) should be merged into master.
    * Check whether we have the must-haves, should-haves, some could-haves
    * Check whether we have javadoc for methods
    * Check whether we commented our code
    * Check whether everything still works after merge into master :smile:
* **Testing**: We need a new strategy for testing
    * Make a measurable goal: e.g. Michelle will write tests for methods in the Answer Service. Issue: Write tests for all methods in Answer Service. Assignee = Michelle
* **Plan out** the presentation: slides, presentation format, video, etc.
    * We need to make an agreement on how we want to create the video and make a time to this
* **Meeting** to create the presentation.
    * We actually make the video

## Coming up...
* Finalizing the Design Document (31 March)
* BuddyCheck (1 April)
* Upload documents to BrightSpace (1 April)