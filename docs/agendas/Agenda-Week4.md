# Meeting Agenda
<span style="color:#ff79c6"> This agenda has been created based on the agenda template from gitlab </span>

Date: Tuesday, 01 March 2021
Main Focus: Receiving feedback on the new Code of Conduct + Starting Implementation
Chair: Mateusz Rębacz
Note Taker: Jan-Jaap

### General Thoughts and Comments
* Click [here](https://demo.hedgedoc.org/uCKCp7VwRT2mZEoF4BfsPw) to get to the document we were working on last week.

## 1. Questions for TA
1. Could you take a look at the changes that we made to the Code of Conduct, and give us feedback on them?
2. When splitting the work, do you think we should assign each person a specific feature to implement or should we make small groups?
3. Could you take a look at our issues board and check if the issues are detailed enough in your opinion?
4. We were thinking of splitting some big issues into smaller sub-issues. Is that a good idea or should we just assign multiple people/branches to one big issue?
5. Is there any rough estimate of the pace at which we need to be developing the implementation not to fall behind?
6. How many other groups do we need to get feedback from on our HCI document?
7. Could you take a look at the form that we designed for the other groups and give us some suggestions?
8. Is splitting the form into 2 sets a good or bad decision?

## 2. Points of Action
* **Scheduling of Issues** Now that we have all the issues/features laid out, we need to decide which ones we're gonna start with. Once we decide on that we will move on to: (~ 30 mins)
* **Role Assignment** We need to split the initial features among each of us. If an issue is big we could discuss splitting in in smaller parts. Since this is the first time we're doing implementation, we should let everyone choose a feature that they feel comfortable developing, there is no need to rush things right now. (~ 1 hour)
* **Code of Conduct:** Based on the feedback we get from the TA we can make some final changes to the Code of Conduct if needed (< 30 mins)
* **HCI surveys** We should finalize the surveys that we prepared for the other groups and send them to each of the members. (< 30 mins)
* **First Endpoint** We should add the first endpoint to our backend application as per deadline. (< 10 mins)

## 3. Coming up...
> This includes tasks we will need to tackle in the coming weeks 
* **BuddyChecks** Each of us needs to remember to do a BuddyCheck about the rest of the group. 
* **HCI Survey:** We need to collect and respond to the results of the HCI survey that we send out this week.
* **Design Document Draft** We need to have the design document draft ready by the 15th March. This means that we need to finalize the ethics section of the document, as well as collect the HCI survey results and apply them before then.

## Questions Round 
* Any questions? :)