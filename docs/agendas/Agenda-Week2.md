# Meeting Agenda
<span style="color:#ff79c6"> This agenda has been created based on the agenda template from gitlab </span>

Date: Tuesday, 16 February 2021
Main Focus: Requirements Document + Role Division
Chair: Michelle Chao Chen
Note Taker: Juul Schnitzler

### General Thoughts and Comments
* Any thoughts on the project or general remarks :)
* Requirements Engineering lecture 
* Click [here](https://demo.hedgedoc.org/uCKCp7VwRT2mZEoF4BfsPw) to get to the document we were working on last week. 
* I would like to share: Sebastian's suggestions yesterday: we need to add additional features to obtain a high grade. We should clarify commments made by our CTA. 

## 1. Questions for Suzanne
> Below are some questions I have :)
1. With who do we check the system requirements?
2. Should be check for user/system + (non)functional requirements with the CTA? Which requirements should we focus on during the interview?
3. What is meant by "application domain" as a system requirement?
4. How should we ask about hardware/software constraints to the CTA?
5. Could you please provide more insight into the use of a Docker? Do you recommend us to use it? 
6. What would be considered on track for this week, i.e. what should be have finished this week?
7. What "soft" deadlines would you recommend we set as a group? 


## 2. Points of Action
* **General discussion:** role division, when we should speed run, time for progress, etc. This is sort of scrum planning but macro. **Role division** will allow us to get an idea what we individually should learn/study/prepare for the coming weeks. 
* **Code of Conduct:** based on the questionnaire we all filled in, let's finalise the code of conduct and upload it to BrightSpace. (< 1 hour)
* **Requirement Elicitation:** after the meeting with CTA, let's write down all the requirements we gathered and observed. (30 mins)
* **Requirement Validation:** we organise + formalise all the requirements into 1 document that we can use to all the requirements with our CTA next week. (1 hour)
* **System Model:** let's create wireframes (based on all the requirements) that we can show to our CTA next week.  
* **Scrum Planning:** we should agree upon a general overview of when/how we are going to make progress in the coming weeks. 

## 3. Coming up...
> This includes tasks we will need to tackle in the coming weeks 
* **Backlog:** the complete backlog for all functionality. This can be finalised after checking the requirement document with our CTA next week. 
* **HCI Survey:** we need to design the survey according to the heuristics and send it out to another group.
* **HCI Report:** Let's aim for a draft, so we can get some feedback. The final design document is due on 15 March, but once we agreed upon things with our CTA and have the results of the survey, we can write it already. 

## Questions Round 
* Any questions? :)