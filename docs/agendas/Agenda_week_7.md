# Agenda week 7
Date:           Tuesday, 23 march 2021\
Main focus:     What to do for the application\
Chair:          Ivan Virovski\
Note taker:     Jan Jaap van Laar

### General remarks
* How does everybody think it is going so far?
    * Is the workload divided better between group members? (topic from last meeting)
    * Is it better not splitting frontends and backends of a feature to different group members?
    * Are we on schedule? 
    * Are there improvements possible?
    * Set goals for the implementation for this sprint.

## 1. Points of action

- **Application**
    - Are we finished with all the must-haves and at least half the should-haves?
    - Can we complete all the should-haves this sprint? 

- **Division**
    - Need to divide the should-have issues further?
    - Need to make sure to divide the backend and frontend in a proper way (people who have not worked on frontend should now work on backend and the other way around)

- **Documentation**
    -Complete the HCI and ethics report from TA's advice (Juul & Chao Chen)

- **CTA meeting**
    - Who will present the application and the features we have implemented until now?
    - Anything we want from the application

## 2. Coming up
- Third BuddyCheck deadline coming next week

## 3. Questions for the TA

* Any questions for the TA?

## 4. Question round

* Are there any questions? 
