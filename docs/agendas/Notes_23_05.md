## TA meeting 23/02/2021

\*GENERAL\*

No Urgent remarks, everyone has watched the required lectures and materials.

\*CTA MEATING\*

1. REMOVED FEATURES:

- Text resizing.

- Different color schemes.

- Merging questions.

- Adding tags/categories.

- Open Question polls.

- Color blindness mode.

- Deleting questions after being answered.

- Marking a question as spam.

- Student editing his own question.

- Cannot choose role - Moderator/Student.

2. NEW BONUS FEATURES:

- Ability to store and export all the questions from a lecture to a text file, as a moderator or a lecturer.

- File should only show the timestamp of each question, but no ips or usernames.

- Moderators and lecturers can view the questions from an old lecture by using the room code.

- Moderators can answer a student&#39;s question, but only by text.

- Moderators can claim a question so that there is no conflict between the moderators over who answers which question.

- Everyone in the chat can see the moderator&#39;s answer.

- The answer from the moderator is also exported to the text file.

- When a question is marked as answered by a lecturer or a moderator, the question is moved to an overview of all the answered questions in the lecture. Everyone can access this overview.

- Limit to the number of active students in a room, that is being set by the admin.

3. OTHER CHANGES:

- Moderators SHOULD BE able to ban people and remove questions.

- Polling and quizzes are now a bonus feature.

- Lecturer should be able to create two different links - one for moderators and one for students. The roles are assigned by joining through these two links.

- A poll or quiz may not have any correct answers.

- Each poll can have between 2 and 10 answers.

- Having and entering a nickname is a SHOULD-HAVE feature.

- Lecturer view - only shows the most important questions of the chat.

- Counter for amount of participants is not necessary.

- UPDATE FOR THE BACKLOG: The word user is too vague, instead use all the roles - &quot;Lecturer/Moderator/Student should have&quot;.

- Anyone who is the owner or maintains the system is the admin.

\*THIS WEEK TASKS\*

- Removing the unwanted features from the frontend design.

- Information literacy module on Brightspace.

- Create tags/labels in gitlab.

- Finalize the Backlog.

- HCI

- Choose next chair and note taker for next week&#39;s meeting.