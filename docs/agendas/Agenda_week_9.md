# Agenda for Week 9

Date: Tuesday, 6 April 2021\
Main Focus: Wrapping up the project & Preparing for the presentation\
Chair: Ivan Virovski & Juul Schnitzler\
Note Taker: Tan Nguyen

## General Remarks
* Should-Haves have been implemented and bugs from last week have been fixed
* HCI is due
* Tests coverage has improved
* Presentation


## Question for Suzanne
* Do we have enough bonus features implemented?
* How should we split the features in the presentation?
* How is the presentation graded?
* Is our test coverage sufficient?

## Points of Action
* **Wrap up** the project: Every feature/issue has been implemented and merged into development.
    * Check for inconsistencies and bugs in the implementation.
    * Check whether we have the must-haves, should-haves, most could-haves.
    * Have we improved our testing and java doc.
    * Merge the reamining tests into development/master.
    * Has everyone worked on every part of the project - Documentation, Frontend, Backend. 
    * Was the workload equally split among group members.
* **Plan out** the presentation: slides, presentation format, video, division etc.
    * We have chosen to do the presentation live, rather than with a recording.
    * What is the most efficient way to split the presentation and prepare for it.
* **Presentation** Juul's part

## Coming up...
* HCI the Design Document
* Presentation 