\documentclass{report}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\graphicspath{ {./images/} } 
\title{Design Document}
\author{Group 63}
\date{April 2021}

\begin{document}

\maketitle
\chapter{HCI Assignment}
\section{Introduction}
The primary objective of this heuristic evaluation is to identify potential usability problems before actual implementation so that they can be addressed before we launch the first version of our software application. 
We decided to focus on five out of ten of the revised usability heuristics (Nielsen, 1994):
\begin{itemize} 
\item Visibility of System Status: users are made aware of events in the application. The system provides feedback on user interactions with the screen. 
\item Match between the System and the Real World: users are familiar with the UI components on the application as the resemble objects of the real world. They can user pre-existing knowledge to navigate the application.
\item User Freedom and Control: users can navigate freely through the application. Their choices should not be hindered by the interface. 
\item Consistency and Standards: informational and navigational components that look the same should have the same functionality linked to them as well. 
\item Error Prevention: users are unlikely to run into errors by how the system has been build. In case of errors, users are given a clear warning or alert. 
\end{itemize}
In order to identify usability problems, we constructed one survey,  addressing the five heuristic principles, and send them out to multiple experts. As this evaluation is conducted prior to the implementation phase, we have presented our experts with prototypes, also known as visualisations, of the main screens of our application. With all the received feedback, we will consider changes to the screen and make all necessary adjustments. The prototypes of the current application are available in the Appendix. 

\section{Methods}
\subsection{Experts} 
In order to determine an appropriate number of evaluators, we decided to base our decision on the percentage of problems identified using the following equation 

\begin{displaymath}
f(i) = 1- (1 - \lambda)^i
\end{displaymath}

\( f(i) \) is the percentage of usability problems found. \( \lambda \) is the percentage of problems found by a single expert. \( i \) is the number of experts. (Nielsen, 1994) \\ 

For this evaluation, we recruited 5 experts from 3 different groups taking the OOP Project course. Considering that they are tasked with implementing an equivalent application and that they had training in performing heuristic evaluation within the same quarter, they have been categorised as specialists. Thus, we have chosen \( \lambda = 0.45 \) (Nielsen, 1994).
\begin{displaymath}
f(7) = 1- (1 - \lambda)^5 = 1- (1 - 0.45)^5 = 0.9497
\end{displaymath}
According to the equation above, we can expect to identify 95.0\% of all usability problems on our application. 
Under the limitations of current circumstances, the sample size is not large enough for us to derive statistically significant results that is representative of all potential users of our application.

\subsection{Procedure}

We sent out a Google Forms survey for our experts to conduct the heuristic evaluation. The survey consists of six sections in total. We guide the experts in completing the evaluation by breaking down the full evaluation into smaller sections and providing coherent instructions at the top of each section.  \par 
In the first section of the survey, evaluators were provided with prototypes of the entrance screen, main page for students, and main page for moderators, and presentation view for lecturers. In addition, there was a brief description of what the survey contains. This allowed the evaluators to get an overview of the interface before officially documenting usability problems.\par 
In the next section, the experts were prompted to evaluate our interface against the first heuristic - Visility of System Status. At the beginning of the section, they are provided with a simple explanation of this heuristic and a few questions to provide them with some cues of what they can address. Images of the prototypes from the first section followed. Below the images, there are 2 fields where evaluator input is required. The first field is an open question. In the second field, the user had to provide an impact index for the problem they reported in the first field. Both fields were mandatory. This format is repeated for the subsequent heuristics: match between system and real world, user control and freedom, consistency and standards, and erro prevention, respectively. \par
After completing the tasks in all 6 sections with a response in each field, the evaluator conclude the survey by submission. 

\subsection{Measures}

 We are measuring the severity of usability problems related to all 4 screens. The severity of a usability is determined by the frequency of the problem and its impact.\par
The results are divided by sections determined by a heuristic - one heuristic per section. We identify usability problems from the first field completed by our experts. In this field experts aggregated all their findings related to a particular heuristic from the three screens. Specific controls or components causing the problem are also included in their description. By creating a tally of all reported issues, we derive the frequency for each usability problem. For the second field of each section, users had to indicate the severity of the problems they found. They were asked to provide a severity index ranging from 1 to 5. An index of 1 meant that it was not important to the overall application and 5 if it needs to be changed immediately to make the application usable. Using this index, we obtain the impact of an issue. 


\section{Results}

In the raw results, the usability problems were divided by heuristics. As a result, usability problems for all the 4 screens were grouped together. In order to give priority and gain better insight into what we should focus on, we decided to further process the results and divided the data that was already grouped by heuristic to be divided by screen as well. 
\begin{table}[h!]
\centering
\begin{tabular}{ |p{3.5cm}||p{1.7cm}|p{1.7cm}|p{1.7cm}|p{1.7cm}|}
 \hline
 \multicolumn{5}{|c|}{Frequency of Usability problems} \\
 \hline
Heuristic Principle & Entrance Page & Main Student Page & Main Moderator Page &Presentation View\\
 \hline
Visibility of System Status & 3&6&4&3\\
Match between System \& Real-World&   1&2&0&0\\
User Control \& Freedom &3&7&6&2\\
Consistency \& Standards &1&1&1&1\\
Error Prevention &4&4&4&4\\
 \hline
\end{tabular}
\caption{Number of Unique Usability Problems by screen and heuristic}
\label{table:1}
\end{table}

\par

\begin{table}[h!]
\centering
\begin{tabular}{ |p{3.5cm}||p{2cm}|p{2cm}|p{2cm}|}
 \hline
 \multicolumn{4}{|c|}{Summary of Usability Problems over all Screens} \\
 \hline
Screen & \(\mu\) & \(\sigma\) & Total reported problems\\
 \hline
Entrance Screen &2.4&1.20&12\\
Main Student Page &4.0&2.28&20 \\
Main Moderator page &3.0&2.19&15\\
Presentation Mode &1.4&1.02&7\\
 \hline
\end{tabular}
\caption{Usability problems by screens}
\label{table:1}
\end{table}
\par

In the table above, we tallied reported usability problems by screen and by heuristic. This already gives us the idea that most problems are related to Visibility of System Status and User Freedom and Control. We decided to further process the results by checking which heuristic we needed to focus on more. In Table 1.2, \(\mu\) is the mean number of problems per screen. \(\sigma\) is the standard deviation of issues per screen related to the heuristic. Total reported problems is the total number of problems reported for that screen. \par In addition, we found that the mean number of problems per heuristic is 10.80 with a standard deviation of 6.177. The heuristic that had the most related problems reported was User Freedom and Control with a total of 18 problems with a mean of 4.50 problems per screen. This showed us which heuristic we failed to comply to. \par
In order to narrow down the issues we had to focus on, we selected 5 of the most frequently reported problems with highest impact index provided by the evaluators. 
\begin{enumerate}
\item The status (on/off) of presentation mode is unclear (\#Visibility of System Status)
\item It is unclear on the entrance page what type of application this is (\#Visibility of System status)
\item There are no undo options on the menu options on the screens (\#User Freedom \& Control, \# Error Prevention)
\item No assistance for mistakes on entrance screen (\#Error Prevention)
\item There is no 'Back' button on the entrance screens in case you entered the wrong lecture(\#Error Prevention, \#User Freedom \& Control)
\end{enumerate}

\begin{table}[h!]
\centering
\begin{tabular}{ |p{4.3cm}||p{2cm}|p{2cm}|}
 \hline
 \multicolumn{3}{|c|}{Frequently reported problems} \\
 \hline
Usability Problem & Impact (1-5) & Frequency\\
 \hline
Status of presentation mode &2.50&4\\
Application type/function &3.33&3\\
Undo menu options &3.57&7\\
Lack of assistance &2.40&5\\
Back button &3.75&4\\
 \hline
\end{tabular}
\caption{Usability problems by screens}
\label{table:1}
\end{table}
Since evaluators have provided different impact values for the same usability problems, we decided to take the average of their impact indices. Finally, we placed our results in a severity matrix to prioritise the identified problems. 

\begin{figure}[h]
\includegraphics[width=\textwidth]{images/SeverityMatrix.png}
\caption{Most frequently reported problems in a severity matrix}
    \label{fig:mesh1}
\end{figure}

\section{Conclusion \& Improvements}
\subsection{Conclusion from Results}
From the severity matrix above, the most severe problem is that users have no options to undo their actions in the application. Most of the evaluators reported that they are unable to undo upvote to a question, edit a question, banning of users, and deleting of their questions. This usability problem was categorized under User Freedom and Control, which was also where many less severe problems were categorized. Another huge concern was about the absence of a 'back' button on the entrance screen as it would not be possible to navigate back to the main application when joining the wrong lecture. However, in the context of the application, a student's only option is to close the application because the application is launched by clicking on a link, which gives them access to a lecture. 
\par Moreover, the results show that our application fails to comply to User Control and Freedom in particular. Two of the five most severe issues are under this category and it has the greatest number of reported problems. We suspect, in addition to the lack of 'undo' options, it is also due to the absence of help and documentation that allows novel users to use the application more effectively.  
\subsection{Improvements}
In order to improve usability of our application, we will prioritise the features that address compliance to User Freedom and Control. The first feature that will be integrated into our application would be the \emph{undo} options. 
\begin{figure}[h]
\includegraphics[width=\textwidth]{images/undo_button_fix.png}
\caption{Undo options on the question tiles}
\end{figure}
Adding these \emph{undo} options to the question tiles warrants change in their current layout as there was previously no space reserved for them. Secondly, a \emph{Back} button will be added to the entrance screen (\# User Freedom and Control) so that users can return to another page of the application when they unintentionally joined the wrong lecture. 
\begin{figure}[h]
\includegraphics[width=\textwidth]{images/back_button_fix.png}
\caption{Undo options on the question tiles}
\end{figure}
Furthermore, to address compliance to Visibility of System Status, we will be adding clear labels to the menu bar of the screen when a lecturer enters presentation mode (Figure 1.4). This will be produce a more perceptible change as lecturers transition from normal to presentation mode. 
\begin{figure}[h]
\includegraphics[width=\textwidth]{images/presentation_mode_fix.png}
\caption{Undo options on the question tiles}
\end{figure}
On Figure 1.4, the menu bar also reveals a bright button with a question mark. Placing the \emph{Help} on the menu bar, not only do we ensure that guidance is available, but also that users can find it effortlessly at all times. The final designs are available in the Appendix.

\chapter{Value Sensitive Design}
\section{Introduction}
In this chapter the following question will be answered regarding responsible computer science: “In what way would our design process and final product change if we also had to design our product for one additional value of a ‘non-obvious’ stakeholder?”.
\section{Stakeholders}
In order to come up with a ‘non-obvious’ stakeholder, it is necessary to first make an overview of all the stakeholders (direct as well as the indirect) involved in the product.

\begin{figure}[h]
\includegraphics[width=\textwidth]{images/DirectStakeholders.png}
\end{figure}

\begin{figure}[h]
\includegraphics[width=\textwidth]{images/IndirectStakeholders.png}
\end{figure}

\section{Non-obvious Stakeholder}
We will focus on one specific ‘non-obvious’ stakeholder, namely dyslexic students. They directly interact with the software, but they are not a majority at TU Delft. This could result in the fact that they will not always be taken into account, especially in the software design of the application. According to Amy Takabori’s (2019) research on dyslexia “Too many schools employ the “wait-to-fail” approach to dyslexia diagnosis, meaning they wait until children fail to learn how to read before providing them with targeted support. Sound familiar? This approach causes long-term damage to students’ education, mental health, and future economic success.”, which is why we think it is important to take this non-obvious stake holder into consideration. 
\section{Ethical Values}
The following three ethically relevant values are important to the dyslexic student in the context of the product:
\begin{enumerate}
\item \emph{Accessibility}: The application should be easy to use, for every end-user involved. Thus it is important that the application should be as accessible for dyslexic as for non-dyslexic students.
\item \emph{Separation between non-dyslexic students and dyslexic students}: While designing the application, the difference in cognitive skills of non-dyslexic students and dyslexic students should be taken into account without causing the students to feel discriminated against.
\item \emph{Usefulness of the asked questions}:It is of importance for the lecturer to know what kind of questions every student has, in order to find out what subjects are seen as more difficult in general. Thus dyslexic students should also be able to ask questions, so that their questions are also taken into account.
\end{enumerate}
\section{Separation between dyslexic and non-dyslexic students}
The separation between non-dyslexic students and dyslexic students is an important feature to take into consideration when designing the software of the application. “The word dyslexia is made up of two different parts: dys meaning not or difficult, and lexia meaning words, reading, or language.” according to Hudson, High and and Al Otaiba’s (2007, p. 506-515) research on dyslexia. Thus, a dyslexic student can be considered to be a student having trouble with words, reading and language. The separation between non-dyslexic students and dyslexic students mainly looks at the differences between the cognitive skills of a dyslexic student versus those of a non-dyslexic student, and how this could affect their use of the application.
\section{Finding Resources}
We could integrate some resources to learn more about the chosen value. We could discuss with doctors who have studied the phenomenon “dyslexia among students” to find out what the most common problems are that dyslexic students cope with, and how to prevent them from running into those problems while using the application. Besides, we could study scientific as well as philosophical literature on the differences between the cognitive skills of dyslexic students versus those of non- dyslexic students. We could also look into assistive technology that has already been developed for dyslexic students to aid reading/writing so that we know which software features are important to them. Finally we could interview dyslexic students to find out what kind of features they would include while designing an application for dyslexic students.

\section{Values Hierarchy}
See figure 2.1
\begin{figure}[h]
\includegraphics[width=\textwidth]{images/ValuesHierarchy.png}
\caption{Values Hierarchy for Dyslexic Students}
\label{fig:mesh1}
\end{figure}

\section{Design Changes}
The design changes required in order to design for our non-obvious stakeholder, can possibly create
issues. The value which will mostly be in tension with this newly introduced value will be the obvious
stakeholder “student”. In the original prototype design, there is only one homepage for the student,
and thus there is not much room left for adding all kinds of features that a dyslexic student might need. Fortunately, these tensions are easily solved by creating an additional “mode” specific for dyslexic students. This mode would look exactly the same as the homepage of a student, but with extra features:
\begin{itemize}
\item The fond is bigger by default, and can be adjusted by the student.
\item Less questions are shown by default, and this can be adjusted by the student.
\item The student can choose to show the questions in “keyword” format, where only a couple of keywords are shown per question.
\end{itemize}
In practice a student can click on a button "presentation mode" which causes the homepage to go into the presentation mode. This will allow students to focus (with larger text, highlighting tools, and less questions) better in the lecture.


\begin{thebibliography}{9}

\bibitem{hudson} 
Hudson, R. F., High, L., \& Al Otaiba, 
\textit{S. Dyslexia and the Brain: What Does Current Research Tell Us?}
\texttt{https://doi.org/10.1598/rt.60.6.1}
The Reading Teacher, 60(6), 506–515. 2007.

\bibitem{nielsen} 
Nielsen, J., \& Mack, R. L. 
\textit{Usability inspection methods}. 
John Wiley I \& Sons Inc. 1994.

\bibitem{takabori} 
Takabori, A.
\textit{2019 Update on Dyslexia Research. Scientific Learning.}
\texttt{https://www.scilearn.com/2019-update-dyslexia-research/}

\end{thebibliography}


\pagebreak
\chapter{Appendix}
\begin{figure}[h]
\includegraphics[width=\textwidth]{images/docs_frontend-prototype_join-page (1).png}
\caption{Entrance screen of the application}
\end{figure}

\begin{figure}[h]
\includegraphics[width=\textwidth]{images/docs_frontend-prototype_main-page (1).png}
\caption{Student main page}
\end{figure}

\begin{figure}[h]
\includegraphics[width=\textwidth]{images/docs_frontend-prototype_main-page-moderator (1).png}
\caption{Moderator main page}
\end{figure}

\begin{figure}[h]
\includegraphics[width=\textwidth]{images/docs_frontend-prototype_main-page-presentation-view.png}
\caption{Presentation view for the lecturer}
\end{figure}
\end{document}