## Code of Conduct

**Group Number:** Group 63\
**Student TA:** Suzanne Backer\
**Group Members:** Juul Schnitzler, Tan Nguyen, Jan Jaap van Laar, Ivan Virovski, Chao Chen, Mateusz Rębacz

1. We need to establish clear deadlines with specific dates (dd-mm-yyyy) for each feature that needs to be implemented. Two days before each deadline, we will check our progress as a group. 
2. We need to inform everyone in the group about our current progress once every week during the TA meeting. If a feature is complete, members will know through GitLab and else they should notify group members once per week on their progress. 
3. We need to meet every Tuesday to discuss progress and plan. We meet on Thursdays in case there's a lot of work and we need to discuss something ASAP.
4. We keep a list of all the features and their status in the gitlab repo. Each feature can be: <span style="color:red">**SCHEDULED**</span>, <span style="color:orange"> **DOING**</span> or <span style="color:green">**DONE**</span>. When you're working on a feature set the appropriate status on GitLab.
5. We keep a list of who is currently responsible for which of the features.
6. No racism.
7. The tasks will be distributed with appropriate levels of difficulty to each of the team members.
8. In the work environment, every problem, personal or general, must be handled and communicated professionally with appropriate language. 
9. The progress of the team will not be compared or used to defaming our team members with other group’s.
10. The flexibility to adapt your own approach to the task will be discussed in the meetings.
11. The guidelines as to what each member should do and the deadline will be discussed at the meetings every week. 
12. The good fringe benefits will be considered after the whole project is accomplished. 
13. In case you know the solution for the questions asked by the TAs, lecturers, or other members, please do not hesitate to answer. 
14. Good implemented features, contributions, or ideas will be praised during the meeting.
15. In case higher grades should be applied for you, please do not hesitate to discuss with the TAs or the people who are in charge. 
16. In case you find other members are doing more work than they should, you should inform them and tell the TA if necessary. 
17. We encourgage discussion of disagreements or conflicts in order to find an appropriate resolution. 
18. Protection of the environment should always be prioritised over the performance and completeness of the project. 
19. A decision affecting a group should be made on the basis of consensus. 
20. All BuddyChecks are acceptable. 
21. Professional and fair competition between teams is highly encouraged. 
22. Rules should be obeyed. In case of disagreement, please contact other members and discuss a better resolution. 
23. Freedom of expression is allowed in appropriate language.
24. Notify other members of late attendance in advance. 
25. As a group, our communication platform is WhatsApp and Mattermost. For meetings, we use Jitsi. Members are expected to check these platforms twice everyday in case there are any emergencies. 
26. If a group member does not inform the whole group in advance about late attendance, being more than fifteen minutes late for a meeting is considered late. For CTA and TA meetings, five minutes late is considered late. 
27. If someone makes a mistake, we will try to solve the issue together. 
28. If a group member is repeatedly late, we will discuss the issue as a group and try to resolve this. 
29. If a group member does not meet a deadline, he/she should inform the group in advance so that we can solve the issue together and prevent the situation next time. 
30. In order to keep this Code of Conduct up to date, we will keep this document on GitLab and everyone can check it anytime. Any changes will be discussed as a group.