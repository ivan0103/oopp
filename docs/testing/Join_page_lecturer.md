# Join page lecturer

**creatingLectureRoom**

1. Start the application
2. Enter lecture name
3. Enter course name
4. Enter date in the future
5. Enter current time
6. Click button &quot;Create Lecture room&quot;
7. Expect File Explorer will open, lecture can be saved with default name &quot;myLecture&quot;
8. Expect Fields will go back to default value once file is saved

**openingLecture**

1. Start the application
2. Click button &quot;Open saved lecture&quot;
3. Expect File Explorer will open
4. A valid file is selected, containing lecture taking place in the future
5. Expect new window to open
6. Fill in name and click &quot;Join as lecturer&quot;
7. Expect new window containing home page lecturer to open

**sharingLink**

1. Open saved file from local machine
2. Expect moderator code to be present
3. Expect student code to be present
4. Expect copying of lecture links is possible

**creatingLecture2**

1. Start the application
2. Enter lecture name
3. Enter course name
4. Enter today&#39;s date
5. Enter random time in future
6. Click button &quot;Create lecture room&quot;
7. Expect new window to open
8. Fill in name and click &quot;Join as lecturer&quot;
9. Expect new window containing home page lecturer to open

**archiveOption**

1. Start the application
2. Click &quot;Archives&quot; button
3. Expect pop-up new window to show
4. Enter valid lecture code
5. Click &quot;See questions&quot;
6. Expect to see list of questions asked during the lecture

**backOption**

1. Start the application
2. Click &quot;Archives&quot; button
3. Expect pop-up new window to show
4. Click &quot;back&quot; button
5. Expect current window to close
6. Expect window for creating a lecture room to show

**emptyList**

1. Start the application
2. Enter lecture information for today
3. Expect current window to close
4. Expect lecturer to join lecture
5. Expect lecture to start with an empty list of questions

**severalLectures**

1. One Lecturer opens a lecture
2. Another lecturer opens another lecture
3. Expect no errors, the lecturers can host their lectures at the same time
