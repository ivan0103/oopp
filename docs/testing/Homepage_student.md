# Homepage student

**askingQuestion**

1. Enter title question
2. Enter content question
3. Click submit button
4. Expect question to disappear with the other questions asked
5. Expect correct title/content/time to appear with question shown

**deletingOtherQuestion**

1. Click &quot;delete&quot; button of some other student&#39;s question
2. Expect a pop-up to show up
3. Expect no error, and the question to still display

**deletingOwnQuestion**

1. Click &quot;delete&quot; button of own question
2. Expect question to disseapear
3. Expect question to disseapear for other students / lecturers

**slowButton**

1. Click slow down button
2. Expect slow down bar lecturer to grow

**speedButton**

1. Click speed up button
2. Expect speed up bar lecturer to grow

**upvoteButton**

1. Click on the heart at the right of a question tile
2. Expect number under the heart to increase by 1 for all users
3. Expect question&#39;s position to go towards top of the list

**bannedFromLecture**

1. Expect lecturer to ban the student
2. Try to re-enter the lecture
3. Pop-up will show
4. No errors, the student will not be able to re-enter the lecture

**pollingOpens**

1. Lecturer opens polling question
2. Expect question and option to appear at left bar under &quot;Poll&quot;
3. Choose incorrect option
4. Lecturer closes polling question
5. Expect student to not be able to enter option
6. Expect student to see the result of the polling

**closedLecture**

1. Lecturer closes the lecture room
2. Expect student to not be able to ask any more questions

**participantsList**

1. Click on top left drop-down menu
2. Click &quot;Member list&quot; option
3. Expect a list with participants to show at the right bar of the window
4. Expect to see number of participants to show at the top of the list
5. Click &quot;close list&quot;
6. Expect list to disappear

**sortByUpvotes**

1. Click drop-down menu &quot;sort by&quot;
2. Click &quot;upvotes&quot; option
3. Expect question list to be sorted by upvotes

**answeredQuestions**

1. Click &quot;Answered&quot; button
2. Expect to only show questions marked as answered

**unAnsweredQuestions**

1. Click &quot;unanswered&quot; button
2. Expect to only see questions not marked as answered

**copyStudentCode**

1. Click &quot;invite&quot; drop-down menu at top bar
2. Click &quot;copy student code&quot; option
3. Expect student code to be copied on clipboard
4. Share code with other student
5. Expect student to enter lecture, no errors






