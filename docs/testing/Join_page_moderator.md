# Join page moderator

**joningLecture**

1. Enter invalid lecture code
2. Expect that a popup appears
3. Enter valid lecture code
4. Expect join page to automatically open
5. Expect all correct lecture information to show
6. Enter name
7. Click &quot;Join as moderator&quot; button
8. Expect no error, join the lecture

**joiningScheduledLecture**

1. Enter a valid lecture code (date lecture 1 day ahead)
2. Fill in name
3. Click &quot;Join as lecturer&quot; button
4. Expect no error, join the lecture