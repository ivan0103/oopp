# Homepage lecturer/moderator

**presentationMode**

1. Click on top left drop-down menu
2. Click &quot;Presentation mode&quot; option
3. Expect left bar to disappear
4. Expect question sorted by upvoted with maximum of 5 displayed

**normalMode**

1. Click on top left drop-down menu
2. Click &quot;normal mode&quot; option
3. Expect left bar to be visible
4. Expect all questions to show

**exportFile**

1. Click on top left drop-down menu
2. Click &quot;Export questions&quot; option
3. Expect File Explorer will open, questions asked can be saved with default name &quot;myQuestion&quot;
4. Save file
5. Expect questions to be saved in a human friendly manner

**participantsList**

1. Click on top left drop-down menu
2. Click &quot;Member list&quot; option
3. Expect a list with participants to show at the right bar of the window
4. Expect to see number of participants to show at the top of the list
5. Click &quot;close list&quot;
6. Expect list to disappear

**sortByUpvotes**

1. Click drop-down menu &quot;sort by&quot;
2. Click &quot;upvotes&quot; option
3. Expect question list to be sorted by upvotes

**answeredQuestions**

1. Click &quot;Answered&quot; button
2. Expect to only show questions marked as answered

**unAnsweredQuestions**

1. Click &quot;unanswered&quot; button
2. Expect to only see questions not marked as answered

**slowerStatistics**

1. Let student click slow down button
2. Expect slow down bar to grow
3. Expect bar to reset every 5 minutes

**fasterStatistics**

1. Let student click speed up button
2. Expect speed up bar to grow
3. Expect bar to reset every 5 minutes

**incomingQuestion**

1. Let student ask a questions
2. Expect lecture to see incoming question

**creatingPoll**

1. Click &quot;create poll&quot; button
2. Expect create poll window to open

**enterPollInformation**

1. Enter poll question
2. Click &quot;Add option&quot;
3. Expect text bar to appear
4. Fill in option
5. Mark option as answered
6. Expect &quot;marked as answered button&quot; to turn into &quot;unmark as answer&quot;

**openingPoll**

1. Enter poll question and options
2. Mark second option as correct answer
3. Click &quot;publish&quot; button
4. Expect window to close
5. Expect left bar to contain statistics about polling
6. Let student enter answer A
7. Expect statistics to update accordingly

**closingPoll**

1. Click &quot;close poll&quot;
2. Expect no additional statistics to come in
3. Click &quot;remove poll&quot;
4. Expect left bar statistics to disseapear
5. Expect statistics to disseapear for students

**copyModeratorCode**

1. Click &quot;invite&quot; drop-down menu at top bar
2. Click &quot;copy moderator code&quot; option
3. Expect moderator code to be copied on clipboard
4. Share code with moderator
5. Expect moderator to enter lecture, no errors

**copyStudentCode**

1. Click &quot;invite&quot; drop-down menu at top bar
2. Click &quot;copy student code&quot; option
3. Expect student code to be copied on clipboard
4. Share code with student
5. Expect student to enter lecture, no errors

**deletingQuestion**

1. Click drop-down menu top-right at question tile
2. Click &quot;delete&quot; option
3. Expect question to disappear from shown questions
4. Expect question to disappear from shown questions students

**editingQuestion**

1. Click drop-down menu top-right at question tile
2. Click &quot;edit&quot; option
3. Expect text-field to show up at the bottom of the window
4. Edit content of question
5. Click &quot;respond&quot; button
6. Expect question to be edited according to changes
7. Expect question to be edited accordingly on student side

**claimingQuestion**

1. Click drop-down menu top-right at question tile
2. Expect &quot;Answer&quot; option to be disabled
3. Click &quot;Claim&quot; option
4. Expect question tile to show &quot;claimed by: &quot; + user name who claimed to question
5. Expect question tile student to show &quot;claimed by: &quot; user name who claimed to question
6. Expect &quot;Answer&quot; option to be enabled

**answeringQuestion**

1. Click drop-down menu top-right at question tile
2. Click &quot;Claim&quot; option
3. Click &quot;Answer&quot; option
4. Expect text field to show up at the bottom of the window
5. Enter respond
6. Click &quot;respond&quot; button
7. Expect respond to be saved when exporting file

**markAsAnswered**

1. Click drop-down menu top-right at question tile
2. Click &quot;Mark as answered&quot; option
3. Expect question to not show anymore
4. Click &quot;Answered&quot; button at the left bar
5. Expect question to show

**banUser**

1. Click drop-down menu top-right at question tile
2. Click &quot;ban user&quot; option
3. Expect banned user&#39;s homepage window to close
4. Expect banned user to not show up in member list
