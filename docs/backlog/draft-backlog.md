# Draft Backlog
> This was the first draft of the backlog
**Group Number:** Group 63\
**Student TA:** Suzanne Backer\
**Group Members:** Juul Schnitzler, Tan Nguyen, Jan Jaap van Laar, Ivan Virovski, Chao Chen, Mateusz Rębacz

This backlog has been written by all group members using a **user-stories** format under their assigned **MoSCoW priorities**.

## Must Have

### User
* As a user, I want to be able to download the desktop application so that the application will run when you click the an invite link or run the application to make a lecture room.
* As a user, I want to be able to generate lecture room links that can be shared with others. 
* As a user, I want to be able to prioritise questions by the number of upvotes so that I can see which questions are most relevant according to the students.
* As a user, I want to set my own name and my own role when entering the lecture room so that I can be identified by other users. 
* As a user, I want the lecture room to be only accesible through the generated link so that I can only join a lecture room that I have the link of. 

### Teacher
* As a teacher, I want to set my own role as a teacher so that I can answer and delete questions.
* As a teacher, I want to able to schedule lecture rooms in advance so that students are well informed about when the session is. 
* As a lecturer, I want to be able to host a room simultaneously with other lecturers, so that multiple lectures can use the application at the same time.
* As a lecturer, I want to be able to mark questions as answered, spam or delete so that it is more clear which questions still need to be answered or which ones are spam. 

### Moderator
* As a moderator, I want to set my own role as a moderator so that I can edit, delete, and merge questions.
* As a moderator, I want to be able to recognise students by their IP address so that I can block them out of the lecture rooms in case that is necessary.
* As a moderator, I can add and delete tags to any questions asked by the students so that the tags are always correctly added. 

### Student
* As a student, I can add tags to my own questions so that I can clearly specify which topics my question is about. 
* As a student, I want to set my own role as a student so that I can ask questions.
* As a student, I want to be able to mark questions as spam and upvote questions so that the teacher can be notified of spam and the teacher can see what is important.
* As a student, I want to be able to delete my own comments so that I can undo a question I asked that is no longer relevant.


## Should Have

### User
* As a user, I want the priority of the questions to be sorted by time in addition to upvotes so that the more recent questions can get priority.
* As a user, I want to be able to change the colors and text size so that the application is accessible for me if I'm color blind or if I have trouble with reading.

### Teacher
* As a teacher, I can open polls for open questions and closed questions so I check students' progress. 
* As a teacher, I can see the speed polls so that I know whether I should speed up or slow down on my explanation during the lecture. 
* As a teacher, I can see the  responses to open questions and the answer distribution for closed questions so that I know what parts of the lecture needs more/less explanation. 

### Student
* As a student, I want to be able to react to the lecturer as "Too slow" or "Too fast" so that they can adapt their speed to the students.
* As a student, I can see the question being polled for and respond by clicking on a choice or writing a response to the question.

### Moderator
* As a moderator, I want to be able to open polls for open questions and closed questions so that I can check students' progress. 

## Could Have

### User
* As a user, I want to be able to see a list of participants currently in the lecture so that I know how many people and who are participating.

### Moderator
* As a moderator, I want to be able to choose the duration for which a student or their IP address is banned. 

### Student
* As a student, I can receive notifications on the status of my most recent questions as answered, merged, or removed. 


## Won't Have

### User
* As a user I won't be able to send a private message to a lecturer, moderators or other students so that every question is asked in public.
* As a user I won't be able to reply to other student's questions so that only the lecturer is able to answer it during the lecture.

### Teacher + Moderator
* As a lecturer or a moderator I won't be able to upvote a comment so that only the students decide whether they want the question to be prioritised or not.
* As a lecturer or a moderator I won't be able to create a message so that only students can ask questions.
* As a lecturer or a moderator I won't be able to react to the lecturer "Too slow" or "Too fast" so that only students can indicate the lecturers speed.
* As a lecturer, I won't be able to IP ban students so that only moderators can monitor the students behaviour and ban when necessary.

### Student
* As a student I won't be able to see or ban the IP addreses of other users so that their IP addresses stay secured.
* As a student I won't be able to delete other students messages so that you cannot delete someone else their question by mistake.