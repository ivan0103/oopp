# Backlog

**Group Number:** Group 63\
**Student TA:** Suzanne Backer\
**Group Members:** Juul Schnitzler, Tan Nguyen, Jan Jaap van Laar, Ivan Virovski, Chao Chen, Mateusz Rębacz

This backlog has been written by all group members using a **user-stories** format under their assigned **MoSCoW priorities**.

### Table of Contents
[TOC]


## Must Have - Basic

### Student/Lecturer
* As a student/lecturer, I want to be able to download the desktop application so that the application will run when you click the an invite link or run the application to make a lecture room. 
* As a student/lecturer, I want to see the questions ordered by the number of upvotes so that I can see which questions are most relevant according to the students.

### Lecturer
* As a lecturer, I want to host the lecture room as a lecturer such that I have access to features to manage the lecture room.
* As a lecturer, I want to be able to mark questions as spam or answered so that it is clear which questions still need to be answered or which ones are spam. 
* As a lecturer, I can generate different links for the moderator and the students so that I can manage the roles that people get when they enter the lecture room. 
* As a lecturer, I want the lecture room to be only accesible through the generated link so that only the people that I want can join the room.
* *As a lecturer, I want to start every new lecture with an empty list of questions.*
* *As a lecturer, I can go into presentation view where only the most important details of the lecture room are shown to me.*
* *As a lecturer, I want to close a question room after the lecture ends, so I can avoid further questions that will be left unanswered.*
 
### Student
* As a student, I want to be able to upvote questions so that the lecturer can be notified of what is important.
* As a student, I want to be able to delete my own questions so that I can undo a question I asked that is no longer relevant. 
* *As a student, I want the lecture room to be only accesible through the generated link so that I can only join a lecture room that I have the link of.*


## Should Have - Complete 

### Student / Lecturer / Moderator
* As a student/lecturer/moderator, I want to set my own name. 
* As a student/lecturer/moderator, I can later access exported files containing questions and written responses during a lecture through a code. 

### Lecturer / Moderator
* As a lecturer/moderator, I can see the speed polls so that I know whether I should speed up or slow down on my explanation during the lecture. 
* As a lecturer/moderator, I want to be able to claim questions and write a response to it. 
* As a lecturer/moderator, I see the questions that have been claimed by other moderators/lecturers so that I know which questions are being answered. 
* As a lecturer/moderator, I want to be able to recognise students by their IP address so that I can block them out of the lecture rooms in case that is necessary.
* *As a student/lecturer, I want to see all answered questions under one category with the tag answered.*

### Lecturer
* As a lecturer, I want to able to schedule lecture rooms on a specific date and time in advance so that students are well informed about when the session is. *I can also share the roomcode upfront.*
* *As a lecturer, I want to be able to host a room simultaneously with other lecturers, so that multiple lectures can use the application at the same time. *

### Student
* As a student, I want to be able to react to the lecturer as "Too slow" or "Too fast" so that they can adapt their speed to the students.

### Moderator 
* *As a moderator, I want to delete questions, so that I can control the quality of the questions being asked.*
* *As a moderator, I want to rephrase questions.* 

### Admin 
* *As an admin I want to restart the whole system without losing data.*
* *As an admin I want to recover the system after an outage without losing data.*
* *As an admin I want to see all activities in an event log, so I can trace back who did what when. The log should contain the names and IP addresses.*



## Could Have

### Student / Lecturer / Moderator
* As a student/lecturer/moderator, I want to be able to see a list of participants currently in the lecture so that I know how many people and who are participating.
* As a student/lecturer/moderator, I want the priority of the questions to be sorted by time in addition to upvotes so that the more recent questions can get priority.


### Lecturer / Moderator 
* *As a lecturer/moderator, I can open (and close) polls for closed questions that have 2 to 10 options so I check students' progress.*
* *As a lecturer/moderator, I can see the answer distribution for closed questions so that I know what parts of the lecture needs more/less explanation.* 

### Moderator
* As a moderator, I want to be able to choose the duration for which a student or their IP address is banned. 

### Student
* As a student, I have a limit on the number of questions I can ask during one lecture to prevent overloading of the server. 
* *As a student, I can see the question being polled for and respond by clicking on a choice.*
* *As a student, I see all the answers to questions that have been given in a written response by all lecturers.*



## Won't Have

### Lecturer/Moderator
* As a lecturer/moderator I won't be able to upvote a comment so that only the students decide whether they want the question to be prioritised or not.
* As a lecturer/moderator I won't be able to create a question so that only students can ask questions.
* As a lecturer/moderator I won't be able to react to the lecturer "Too slow" or "Too fast" so that only students can indicate the lecturers speed.


### Student
* As a student, I won't be able to send a private message to a lecturer, moderators or other students so that every question is asked in public.
* As a student, I won't be able to see or ban the IP addreses of other users so that their IP addresses stay secured.
* As a student I won't be able to delete other students messages so that you cannot delete someone else their question by mistake.
* As a student, I won't be able to enter a lecture room in advance. 
