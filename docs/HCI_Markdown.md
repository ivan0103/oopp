# HCI Assignment


## Introduction
The primary objective of this heuristic evaluation is to identify potential usability problems before actual implementation so that they can be addressed before we launch the first version of our software application. 
We decided to focus on five out of ten of the revised usability heuristics (Nielsen, 1994):

* Visibility of System Status: users are made aware of events in the application. The system provides feedback on user interactions with the screen. 
* Match between the System and the Real World: users are familiar with the UI components on the application as the resemble objects of the real world. They can use pre-existing knowledge to navigate the application.
* User Freedom and Control: users can navigate freely through the application. Their choices should not be hindered by the interface. 
* Consistency and Standards: informational and navigational components that look the same should have the same functionality linked to them as well. 
* Error Prevention: users are unlikely to run into errors by how the system has been build. In case of errors, users are given a clear warning or alert. 

In order to identify usability problems, we constructed one survey,  addressing the five heuristic principles, and send them out to multiple experts. As this evaluation is conducted prior to the implementation phase, we have presented our experts with prototypes, also known as visualisations, of the main screens of our application. With all the received feedback, we will consider changes to the screen and make all necessary adjustments. 

## Methods
### Experts
In order to determine an appropriate number of evaluators, we decided to base our decision on the percentage of problems identified using the following equation 

> Below I am going to keep the latex (i don't know how to fix it on markdown hehe)
```latex 
\begin{displaymath}
f(i) = 1- (1 - \lambda)^i
\end{displaymath}

\( f(i) \) is the percentage of usability problems found. \( \lambda \) is the percentage of problems found by a single expert. \( i \) is the number of experts. (Nielsen, 1994) \\ 

For this evaluation, we recruited 5 experts from 3 different groups taking the OOP Project course. Considering that they are tasked with implementing an equivalent application and that they had training in performing heuristic evaluation within the same quarter, they have been categorised as specialists. Thus, we have chosen \( \lambda = 0.45 \) (Nielsen, 1994).
\begin{displaymath}
f(7) = 1- (1 - \lambda)^5 = 1- (1 - 0.45)^5 = 0.9497
\end{displaymath}
According to the equation above, we can expect to identify 95.0\% of all usability problems on our application. 
Under the limitations of current circumstances, the sample size is not large enough for us to derive statistically significant results that is representative of all potential users of our application.
````

## Procedure

We sent out a Google Forms survey for our experts to conduct the heuristic evaluation. The survey consists of six sections in total. We guide the experts in completing the evaluation by breaking down the full evaluation into smaller sections and providing coherent instructions at the top of each section.
In the first section of the survey, evaluators were provided with prototypes of the entrance screen, student page, and moderator page, and presentation view for the lecturer. In addition, there was a brief description of what the survey contains. This allowed the evaluators to get an overview of the interface before officially documenting usability problems.
In the next section, the experts were prompted to evaluate our interface against the first heuristic - Visibility of System Status. At the beginning of the section, they are provided with a simple explanation of this heuristic and a few questions to provide them with some cues of what they can address. Images of the prototypes from the first section followed. Below the images, there are 2 fields where evaluator input is required. The first field is an open question. In the second field, the user had to provide an impact index for the problem they reported in the first field. Both fields were mandatory. This format is repeated for the subsequent heuristics: match between system and real world, user control and freedom, consistency and standards, and error prevention, respectively.\
After completing the tasks in all 6 sections with a response in each field, the evaluator conclude the survey by submission. 

### Measures

 We are measuring the severity of usability problems related to all 3 screens. The severity of a usability is determined by the frequency of the problem and its impact.\
The results are divided by sections determined by a heuristic - one heuristic per section. We identify usability problems from the first field completed by our experts. In this field experts aggregated all their findings related to a particular heuristic from the three screens. Specific controls or components causing the problem are also included in their description. By creating a tally of all reported issues, we derive the frequency for each usability problem. For the second field of each section, users had to indicate the severity of the problems they found. They were asked to provide a severity index ranging from 1 to 5. An index of 1 meant that it was not important to the overall application and 5 if it needs to be changed immediately to make the application usable. Using this index, we obtain the impact of an issue. 


## Results
> This is a plan of how to process the results

Since in each section they were provided with all 4 screens, we decided to further process the raw results and group them per screen.  All usability issues were already grouped by heuristic but we decided to further subdivide the data by screens. 

* Plan: Find the mean of the frequency of problems about a heuristic (frequency distribution for us to see which heuristic principle we are violating the most - 5 graphs). This one is for us to see Provide the graphs for the general application (all of them aggregrated). 
* Plan: Provide the table when you divide the problem per screen (the average number of problems per screen and with their standard deviation)
* Plan: We are going to focus on the most frequently reported + the most impactful usability problems per heuristic. This rank is going to be determined for each usability problem by adding the frequecy and impact. 

