package nl.tudelft.oopp.application.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.extern.java.Log;
import nl.tudelft.oopp.application.controller.dtos.AnswerCreateRequest;
import nl.tudelft.oopp.application.controller.dtos.LectureCreateRequest;
import nl.tudelft.oopp.application.controller.dtos.PollAnswerRequest;
import nl.tudelft.oopp.application.controller.dtos.PollCreateRequest;
import nl.tudelft.oopp.application.controller.dtos.QuestionCreateRequest;
import nl.tudelft.oopp.application.controller.dtos.QuestionEditRequest;
import nl.tudelft.oopp.application.controller.dtos.QuestionUpvoteRequest;
import nl.tudelft.oopp.application.entity.Answer;
import nl.tudelft.oopp.application.entity.Lecture;
import nl.tudelft.oopp.application.entity.Poll;
import nl.tudelft.oopp.application.entity.Question;
import nl.tudelft.oopp.application.entity.Reaction;
import nl.tudelft.oopp.application.entity.User;
import nl.tudelft.oopp.application.exception.InvalidArgumentsException;
import nl.tudelft.oopp.application.exception.LectureNotFoundException;
import nl.tudelft.oopp.application.exception.PollNotClosedException;
import nl.tudelft.oopp.application.exception.PollNotFoundException;
import nl.tudelft.oopp.application.exception.QuestionNotClaimedException;
import nl.tudelft.oopp.application.exception.QuestionNotFoundException;
import nl.tudelft.oopp.application.exception.QuestionNotOwnedByUserException;
import nl.tudelft.oopp.application.exception.ReactionNotFoundException;
import nl.tudelft.oopp.application.exception.UnauthorizedException;
import nl.tudelft.oopp.application.exception.UserBannedException;
import nl.tudelft.oopp.application.exception.UserNotFoundException;
import nl.tudelft.oopp.application.repository.UserRepository;
import nl.tudelft.oopp.application.service.AnswerService;
import nl.tudelft.oopp.application.service.LectureService;
import nl.tudelft.oopp.application.service.QuestionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(LectureController.class)
@Log
public class LectureControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private QuestionService questionService;
    @MockBean
    private LectureService lectureService;
    @MockBean
    private AnswerService answerService;
    @MockBean
    private UserRepository userRepository;

    private Lecture lecture;
    private User user1;
    private User user2;
    private Set users;
    private Question q1;
    private Question q2;
    private Set questions;
    private Poll poll;
    private Date date = new Date(2021, 4, 3);
    private LectureCreateRequest lectureCreateRequest;
    private Answer answer;

    private static ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
        this.questions = new HashSet();
        q1 = new Question();
        q2 = new Question();
        q1.setTitle("title1");
        q1.setContent("content1");
        q2.setTitle("title2");
        questions.add(q1);
        questions.add(q2);
        this.users = new HashSet();
        user1 = new User();
        user2 = new User();
        users.add(user1);
        users.add(user2);
        this.lecture = new Lecture();
        lecture.setCourse("courseName");
        lecture.setUsers(users);
        lecture.setQuestions(questions);
        this.lectureCreateRequest = new LectureCreateRequest();
        lectureCreateRequest.setName("name");
        lectureCreateRequest.setCourse("courseName");
        lectureCreateRequest.setTime(date);
        this.answer = new Answer();
        this.answer.setAnswer("answerContent");
        this.answer.setAnswerer(user1);
    }

    /**
     * Generic JSON parser.
     * @param o Object of any class
     * @return JSON String used for requests/response
     */
    public static String toJsonString(Object o) {
        try {
            return objectMapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            log.warning("Error parsing to json");
            e.printStackTrace();
            return null;
        }
    }


    @Test
    public void getListFutureLecturesTest() throws Exception {
        when(lectureService.listLectures("future"))
                .thenReturn(List.of(lecture));
        mockMvc.perform(get("/lecture")
                .param("filter", "future")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("[" + toJsonString(lecture) + "]"));
    }

    @Test
    public void getListPastLecturesTest() throws Exception {
        when(lectureService.listLectures("past"))
                .thenReturn(List.of(lecture));
        mockMvc.perform(get("/lecture")
                .param("filter", "past")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("[" + toJsonString(lecture) + "]"));
    }

    @Test
    public void postCreateLectureTest() throws Exception {
        when(lectureService.createLecture("name", "courseName", date))
                .thenReturn(lecture);
        mockMvc.perform(post("/lecture")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(lectureCreateRequest)))
                .andExpect(status().isOk())
                .andExpect(content().json(toJsonString(lecture)));
    }

    @Test
    public void postCreateLectureExceptionTest() throws Exception {
        when(lectureService.createLecture("name", "courseName", date))
                .thenThrow(new InvalidArgumentsException());
        mockMvc.perform(post("/lecture")
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(lectureCreateRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getLectureTest() throws Exception {
        when(lectureService.getLecture(1L))
                .thenReturn(lecture);
        mockMvc.perform(get("/lecture/{lectureId}", 1L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(toJsonString(lecture)));
    }

    @Test
    public void getLectureTestException() throws Exception {
        when(lectureService.getLecture(1L))
                .thenThrow(LectureNotFoundException.class);
        mockMvc.perform(get("/lecture/{lectureId}", 1L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteLectureTest() throws Exception {
        mockMvc.perform(delete("/lecture/{lectureId}", 1L)
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getListQuestionsTest() throws Exception {
        when(questionService.listQuestions(1L, "unanswered", "upvotes", "127.0.0.1"))
                .thenReturn(List.of(q1,q2));
        mockMvc.perform(get("/lecture/{lectureId}/questions", 1L)
                .param("filter", "unanswered")
                .param("sort", "upvotes")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("["
                        + toJsonString(q1)
                        + "," + toJsonString(q2)
                        + "]"));
    }

    @Test
    public void getListQuestionsTestNotFoundException() throws Exception {
        when(questionService.listQuestions(1L, "unanswered", "upvotes", "127.0.0.1"))
                .thenThrow(LectureNotFoundException.class);
        mockMvc.perform(get("/lecture/{lectureId}/questions", 1L)
                .param("filter", "unanswered")
                .param("sort", "upvotes")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getListQuestionsTestUserBannedException() throws Exception {
        when(questionService.listQuestions(1L, "unanswered", "upvotes", "127.0.0.1"))
                .thenThrow(UserBannedException.class);
        mockMvc.perform(get("/lecture/{lectureId}/questions", 1L)
                .param("filter", "unanswered")
                .param("sort", "upvotes")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void getListUsersTest() throws Exception {
        when(lectureService.listUsers(1L))
                .thenReturn(List.of(user1));
        mockMvc.perform(get("/lecture/{lectureId}/users", 1L)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("["
                        + toJsonString(user1)
                        + "]"));
    }

    @Test
    public void getListUsersTestNotFoundException() throws Exception {
        when(lectureService.listUsers(1L))
                .thenThrow(LectureNotFoundException.class);
        mockMvc.perform(get("/lecture/{lectureId}/users", 1L)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void postCreateQuestionTest() throws Exception {
        when(questionService.createQuestion("title1", "content1", 1L, 2L,
                "127.0.0.1"))
                .thenReturn(q1);
        QuestionCreateRequest request = new QuestionCreateRequest();
        request.setContent("content1");
        request.setTitle("title1");
        request.setUserID(2L);
        mockMvc.perform(post("/lecture/{lectureId}/questions", 1L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)))
                .andExpect(status().isOk())
                .andExpect(content().json(toJsonString(q1)));
    }

    @Test
    public void postCreateQuestionTestLectureNotFoundException() throws Exception {
        when(questionService.createQuestion("title1", "content1", 1L, 2L,
                "127.0.0.1"))
                .thenThrow(LectureNotFoundException.class);
        QuestionCreateRequest request = new QuestionCreateRequest();
        request.setContent("content1");
        request.setTitle("title1");
        request.setUserID(2L);
        mockMvc.perform(post("/lecture/{lectureId}/questions", 1L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void postCreateQuestionTestUserNotFoundException() throws Exception {
        when(questionService.createQuestion("title1", "content1", 1L, 2L,
                "127.0.0.1"))
                .thenThrow(UserNotFoundException.class);
        QuestionCreateRequest request = new QuestionCreateRequest();
        request.setContent("content1");
        request.setTitle("title1");
        request.setUserID(2L);
        mockMvc.perform(post("/lecture/{lectureId}/questions", 1L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void postCreateQuestionTestInvalidArgumentException() throws Exception {
        when(questionService.createQuestion("title1", "content1", 1L, 2L,
                "127.0.0.1"))
                .thenThrow(InvalidArgumentsException.class);
        QuestionCreateRequest request = new QuestionCreateRequest();
        request.setContent("content1");
        request.setTitle("title1");
        request.setUserID(2L);
        mockMvc.perform(post("/lecture/{lectureId}/questions", 1L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void postCreateQuestionTestUserBannedException() throws Exception {
        when(questionService.createQuestion("title1", "content1", 1L, 2L,
                "127.0.0.1"))
                .thenThrow(UserBannedException.class);
        QuestionCreateRequest request = new QuestionCreateRequest();
        request.setContent("content1");
        request.setTitle("title1");
        request.setUserID(2L);
        mockMvc.perform(post("/lecture/{lectureId}/questions", 1L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)))
                .andExpect(status().isForbidden());
    }

    @Test
    public void putEditQuestionTest() throws Exception {
        when(questionService.editQuestion(
                "content1", 1L, 2L, 3L, "127.0.0.1"))
            .thenReturn(q1);
        QuestionEditRequest request = new QuestionEditRequest();
        request.setContent("content1");
        request.setUserID(2L);
        mockMvc.perform(put("/lecture/{lectureId}/questions/{id}", 1L, 3L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)))
                .andExpect(status().isOk())
                .andExpect(content().json(toJsonString(q1)));
    }

    @Test
    public void putEditQuestionTestInvalidArgumentException() throws Exception {
        when(questionService.editQuestion(
                "content1", 1L, 2L, 3L, "127.0.0.1"))
                .thenThrow(InvalidArgumentsException.class);
        QuestionEditRequest request = new QuestionEditRequest();
        request.setContent("content1");
        request.setUserID(2L);
        mockMvc.perform(put("/lecture/{lectureId}/questions/{id}", 1L, 3L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void putEditQuestionTestLectureNotFoundException() throws Exception {
        when(questionService.editQuestion(
                "content1", 1L, 2L, 3L, "127.0.0.1"))
                .thenThrow(LectureNotFoundException.class);
        QuestionEditRequest request = new QuestionEditRequest();
        request.setContent("content1");
        request.setUserID(2L);
        mockMvc.perform(put("/lecture/{lectureId}/questions/{id}", 1L, 3L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void putEditQuestionTestQuestionNotFoundException() throws Exception {
        when(questionService.editQuestion(
                "content1", 1L, 2L, 3L, "127.0.0.1"))
                .thenThrow(QuestionNotFoundException.class);
        QuestionEditRequest request = new QuestionEditRequest();
        request.setContent("content1");
        request.setUserID(2L);
        mockMvc.perform(put("/lecture/{lectureId}/questions/{id}", 1L, 3L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void putEditQuestionTestUserBannedException() throws Exception {
        when(questionService.editQuestion(
                "content1", 1L, 2L, 3L, "127.0.0.1"))
                .thenThrow(UserBannedException.class);
        QuestionEditRequest request = new QuestionEditRequest();
        request.setContent("content1");
        request.setUserID(2L);
        mockMvc.perform(put("/lecture/{lectureId}/questions/{id}", 1L, 3L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)))
                .andExpect(status().isForbidden());
    }

    @Test
    public void putEditQuestionTestQuestionNotOwnedByUserException() throws Exception {
        when(questionService.editQuestion(
                "content1", 1L, 2L, 3L, "127.0.0.1"))
                .thenThrow(QuestionNotOwnedByUserException.class);
        QuestionEditRequest request = new QuestionEditRequest();
        request.setContent("content1");
        request.setUserID(2L);
        mockMvc.perform(put("/lecture/{lectureId}/questions/{id}", 1L, 3L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)))
                .andExpect(status().isForbidden());
    }

    @Test
    public void getQuestionTest() throws Exception {
        when(questionService.getQuestion(2L, 1L, "127.0.0.1"))
            .thenReturn(q1);
        mockMvc.perform(get("/lecture/{lectureId}/questions/{id}", 1L, 2L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(toJsonString(q1)));
    }

    @Test
    public void getQuestionTestQuestionNotFoundException() throws Exception {
        when(questionService.getQuestion(2L, 1L, "127.0.0.1"))
                .thenThrow(QuestionNotFoundException.class);
        mockMvc.perform(get("/lecture/{lectureId}/questions/{id}", 1L, 2L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getQuestionTestLectureNotFoundException() throws Exception {
        when(questionService.getQuestion(2L, 1L, "127.0.0.1"))
                .thenThrow(LectureNotFoundException.class);
        mockMvc.perform(get("/lecture/{lectureId}/questions/{id}", 1L, 2L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getQuestionTestUserBannedException() throws Exception {
        when(questionService.getQuestion(2L, 1L, "127.0.0.1"))
                .thenThrow(UserBannedException.class);
        mockMvc.perform(get("/lecture/{lectureId}/questions/{id}", 1L, 2L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void postAnswerQuestion() throws Exception {
        when(answerService.createAnswer("answerContent", 3L, 2L, 1L))
                .thenReturn(answer);
        AnswerCreateRequest request = new AnswerCreateRequest();
        request.setContent("answerContent");
        request.setUserID(3L);
        mockMvc.perform(post("/lecture/{lectureId}/questions/{id}/answer", 1L, 2L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)))
                .andExpect(status().isOk())
                .andExpect(content().json(toJsonString(answer)));
    }

    @Test
    public void postAnswerQuestionLectureNotFoundException() throws Exception {
        when(answerService.createAnswer("answerContent", 3L, 2L, 1L))
                .thenThrow(LectureNotFoundException.class);
        AnswerCreateRequest request = new AnswerCreateRequest();
        request.setContent("answerContent");
        request.setUserID(3L);
        mockMvc.perform(post("/lecture/{lectureId}/questions/{id}/answer", 1L, 2L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void postAnswerQuestionQuestionNotFoundException() throws Exception {
        when(answerService.createAnswer("answerContent", 3L, 2L, 1L))
                .thenThrow(QuestionNotFoundException.class);
        AnswerCreateRequest request = new AnswerCreateRequest();
        request.setContent("answerContent");
        request.setUserID(3L);
        mockMvc.perform(post("/lecture/{lectureId}/questions/{id}/answer", 1L, 2L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void postAnswerQuestionUserNotFoundException() throws Exception {
        when(answerService.createAnswer("answerContent", 3L, 2L, 1L))
                .thenThrow(UserNotFoundException.class);
        AnswerCreateRequest request = new AnswerCreateRequest();
        request.setContent("answerContent");
        request.setUserID(3L);
        mockMvc.perform(post("/lecture/{lectureId}/questions/{id}/answer", 1L, 2L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void postAnswerQuestionQuestionNotClaimedException() throws Exception {
        when(answerService.createAnswer("answerContent", 3L, 2L, 1L))
                .thenThrow(QuestionNotClaimedException.class);
        AnswerCreateRequest request = new AnswerCreateRequest();
        request.setContent("answerContent");
        request.setUserID(3L);
        mockMvc.perform(post("/lecture/{lectureId}/questions/{id}/answer", 1L, 2L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void postAnswerQuestionInvalidArgumentsException() throws Exception {
        when(answerService.createAnswer("answerContent", 3L, 2L, 1L))
                .thenThrow(InvalidArgumentsException.class);
        AnswerCreateRequest request = new AnswerCreateRequest();
        request.setContent("answerContent");
        request.setUserID(3L);
        mockMvc.perform(post("/lecture/{lectureId}/questions/{id}/answer", 1L, 2L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getQuestionUpvotesTest() throws Exception {
        when(questionService.getQuestionUpvotes(2L, 1L))
                .thenReturn(Set.of(user1));
        mockMvc.perform(get("/lecture/{lectureId}/questions/{id}/upvote", 1L, 2L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("[" + toJsonString(user1) + "]"));
    }

    @Test
    public void getQuestionUpvotesTestLectureNotFoundException() throws Exception {
        when(questionService.getQuestionUpvotes(2L, 1L))
                .thenThrow(LectureNotFoundException.class);
        mockMvc.perform(get("/lecture/{lectureId}/questions/{id}/upvote", 1L, 2L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getQuestionUpvotesTestQuestionNotFoundException() throws Exception {
        when(questionService.getQuestionUpvotes(2L, 1L))
                .thenThrow(QuestionNotFoundException.class);
        mockMvc.perform(get("/lecture/{lectureId}/questions/{id}/upvote", 1L, 2L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void postQuestionUpvoteTest() throws Exception {
        QuestionUpvoteRequest request = new QuestionUpvoteRequest();
        request.setUserId(3L);
        mockMvc.perform(post("/lecture/{lectureId}/questions/{id}/upvote", 1L, 2L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteQuestionTest() throws Exception {
        mockMvc.perform(delete("/lecture/{lectureId}/questions/{id}", 1L, 2L))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteReactionTest() throws Exception {
        mockMvc.perform(delete("/lecture/{lectureId}/reactions/{id}", 1L, 2L))
                .andExpect(status().isOk());
    }

    @Test
    public void postEndLectureTest() throws Exception {
        mockMvc.perform(post("/lecture/end")
            .content(toJsonString(lecture))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    public void postBanUserTest() throws Exception {
        mockMvc.perform(post("/lecture/{id}/ban", 1L)
                .param("user", "3"))
                .andExpect(status().isOk());
    }

    @Test
    public void postBanUserTestUserNoUserId() throws Exception {
        mockMvc.perform(post("/lecture/{id}/ban", 1L))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void postUnBanUserTest() throws Exception {
        mockMvc.perform(post("/lecture/{id}/unban", 1L)
                .param("ip", "127.0.0.1"))
                .andExpect(status().isOk());
    }

    @Test
    public void postUnBanUserTestNoIpAddress() throws Exception {
        mockMvc.perform(post("/lecture/{id}/unban", 1L))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void postCreatePollTest() throws Exception {
        PollCreateRequest request = new PollCreateRequest("quiz",
                List.of("o1", "o2"), 0, "modCode");
        when(lectureService.createPoll(1L, request))
                .thenReturn(lecture);

        mockMvc.perform(post("/lecture/{lectureId}/poll", 1L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(toJsonString(request)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(toJsonString(lecture)));
    }

    @Test
    public void postPollLectureNotFoundTest() throws Exception {
        PollCreateRequest request = new PollCreateRequest("quiz",
                List.of("o1", "o2"), 0, "modCode");
        when(lectureService.createPoll(1L, request))
                .thenThrow(new LectureNotFoundException());
        mockMvc.perform(post("/lecture/{lectureId}/poll", 1L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(toJsonString(request)))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void postPollUnauthorizedTest() throws Exception {
        PollCreateRequest request = new PollCreateRequest("quiz",
                List.of("o1", "o2"), 0, "modCode");
        when(lectureService.createPoll(1L, request))
                .thenThrow(new UnauthorizedException());
        mockMvc.perform(post("/lecture/{lectureId}/poll", 1L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(toJsonString(request)))
                .andDo(print())
                .andExpect(status().isNotFound());
    }


    @Test
    public void postAnswerPollTest() throws Exception {
        boolean thrown = false;

        PollAnswerRequest request = new PollAnswerRequest(1, 2L);
        when(lectureService.answerPoll(lecture.getId(), request))
                .thenReturn(lecture);

        mockMvc.perform(post("/lecture/{lectureId}/poll/answer", lecture.getId())
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(toJsonString(request)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void postAnswerPollLectureNotFoundTest() throws Exception {
        PollAnswerRequest request = new PollAnswerRequest(1, 2L);
        when(lectureService.answerPoll(lecture.getId(), request))
                .thenThrow(LectureNotFoundException.class);

        mockMvc.perform(post("/lecture/{lectureId}/poll/answer", lecture.getId())
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(toJsonString(request)))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void postAnswerPollUserNotFoundTest() throws Exception {
        PollAnswerRequest request = new PollAnswerRequest(1, 2L);
        when(lectureService.answerPoll(lecture.getId(), request))
                .thenThrow(UserNotFoundException.class);

        mockMvc.perform(post("/lecture/{lectureId}/poll/answer", lecture.getId())
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(toJsonString(request)))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void postAnswerPollUnauthorizedTest() throws Exception {
        PollAnswerRequest request = new PollAnswerRequest(1, 2L);
        when(lectureService.answerPoll(lecture.getId(), request))
                .thenThrow(UnauthorizedException.class);

        mockMvc.perform(post("/lecture/{lectureId}/poll/answer", lecture.getId())
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(toJsonString(request)))
                .andDo(print())
                .andExpect(status().isForbidden());
    }


    @Test
    public void postClosePollTest() throws Exception {
        when(lectureService.closePoll(1L, "modCode"))
                .thenReturn(lecture);

        mockMvc.perform(post("/lecture/{lectureId}/closePoll", 1L)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .param("code", "modCode"))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void postClosePollLectureNotFoundTest() throws Exception {
        when(lectureService.closePoll(1L, "modCode"))
                .thenThrow(LectureNotFoundException.class);

        mockMvc.perform(post("/lecture/{lectureId}/closePoll", 1L)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .param("code", "modCode"))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    @Test
    public void postClosePollUnauthorizedTest() throws Exception {
        when(lectureService.closePoll(1L, "modCode"))
                .thenThrow(UnauthorizedException.class);

        mockMvc.perform(post("/lecture/{lectureId}/closePoll", 1L)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .param("code", "modCode"))
                .andDo(print())
                .andExpect(status().isForbidden());

    }

    @Test
    public void postClosePollNotFoundExceptionTest() throws Exception {
        when(lectureService.closePoll(1L, "modCode"))
                .thenThrow(PollNotFoundException.class);

        mockMvc.perform(post("/lecture/{lectureId}/closePoll", 1L)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .param("code", "modCode"))
                .andDo(print())
                .andExpect(status().isNotFound());

    }


    @Test
    public void postRemovePollTest() throws Exception {
        when(lectureService.removePoll(1L, "modCode"))
                .thenReturn(lecture);

        mockMvc.perform(post("/lecture/{lectureId}/removePoll", 1L)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .param("code", "modCode"))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void postRemovePollLectureNotFoundTest() throws Exception {
        when(lectureService.removePoll(1L, "modCode"))
                .thenThrow(LectureNotFoundException.class);

        mockMvc.perform(post("/lecture/{lectureId}/removePoll", 1L)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .param("code", "modCode"))
                .andDo(print())
                .andExpect(status().isNotFound());

    }


    @Test
    public void postRemovePollUnauthorizedTest() throws Exception {
        when(lectureService.removePoll(1L, "modCode"))
                .thenThrow(UnauthorizedException.class);

        mockMvc.perform(post("/lecture/{lectureId}/removePoll", 1L)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .param("code", "modCode"))
                .andDo(print())
                .andExpect(status().isForbidden());

    }

    @Test
    public void postRemovePollNotFoundTest() throws Exception {
        when(lectureService.removePoll(1L, "modCode"))
                .thenThrow(PollNotFoundException.class);

        mockMvc.perform(post("/lecture/{lectureId}/removePoll", 1L)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .param("code", "modCode"))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    @Test
    public void postRemovePollNotClosedTest() throws Exception {
        when(lectureService.removePoll(1L, "modCode"))
                .thenThrow(PollNotClosedException.class);

        mockMvc.perform(post("/lecture/{lectureId}/removePoll", 1L)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .param("code", "modCode"))
                .andDo(print())
                .andExpect(status().isBadRequest());

    }

    @Test
    public void postMarkAsAnsweredTest() throws Exception {
        when(answerService.markAsAnswered(1L,2L))
                .thenReturn(q1);

        mockMvc.perform(post("/lecture/{lectureId}/questions/{id}/markAnswered", 1L, 2L)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void postMarkAsAnsweredLectureNotFoundTest() throws Exception {
        when(answerService.markAsAnswered(1L,2L))
                .thenThrow(LectureNotFoundException.class);

        mockMvc.perform(post("/lecture/{lectureId}/questions/{id}/markAnswered", 1L, 2L)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andDo(print())
                .andExpect(status().isNotFound());

    }


    @Test
    public void postMarkAsAnsweredQuestionNotFoundTest() throws Exception {
        when(answerService.markAsAnswered(1L,2L))
                .thenThrow(QuestionNotFoundException.class);

        mockMvc.perform(post("/lecture/{lectureId}/questions/{id}/markAnswered", 1L, 2L)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    @Test
    public void postClaimQuestionTest() throws Exception {
        when(questionService.claimQuestion(1L,2L))
                .thenReturn(q1);

        mockMvc.perform(post("/lecture/questions/{id}/claim/{userId}", 1L, 2L)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void postClaimQuestionNotFoundTest() throws Exception {
        when(questionService.claimQuestion(1L,2L))
                .thenThrow(QuestionNotFoundException.class);

        mockMvc.perform(post("/lecture/questions/{id}/claim/{userId}", 1L, 2L)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    @Test
    public void postClaimQuestionUserNotFoundTest() throws Exception {
        when(questionService.claimQuestion(1L,2L))
                .thenThrow(UserNotFoundException.class);

        mockMvc.perform(post("/lecture/questions/{id}/claim/{userId}", 1L, 2L)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    @Test
    public void postSpeedReactionTest() throws Exception {
        Reaction reaction = new Reaction();
        reaction.setType(false);
        reaction.setLecture(lecture);
        when(lectureService.speedReaction(1L, false))
                .thenReturn(reaction);

        mockMvc.perform(post("/lecture/{lectureId}/reaction/{type}", 1L, false)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andDo(print())
                .andExpect(status().isOk());

    }


    @Test
     public void postSpeedReactionLectureNotFoundTest() throws Exception {
        when(lectureService.speedReaction(1L, false))
                .thenThrow(LectureNotFoundException.class);

        mockMvc.perform(post("/lecture/{lectureId}/reaction/{type}", 1L, false)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    /* @Test
    public void postSpeedReactionReactionNotFoundTest() throws Exception {
        when(lectureService.speedReaction(1L, false))
                .thenThrow(ReactionNotFoundException.class);

        mockMvc.perform(post("/lecture/{lectureId}/reaction/{type}", 1L, false)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andDo(print())
                .andExpect(status().isNotFound());

    }*/


    @Test
    public void getListSpeedReactionsTest() throws Exception {
        Reaction reaction = new Reaction();
        reaction.setType(false);
        reaction.setLecture(lecture);
        List<Reaction> reactions = new ArrayList<>();
        reactions.add(reaction);
        when(lectureService.listReactions(1L))
                .thenReturn(reactions);

        mockMvc.perform(get("/lecture/{lectureId}/reactions", 1L)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void getListSpeedReactionsLectureNotFoundTest() throws Exception {
        Reaction reaction = new Reaction();
        reaction.setType(false);
        reaction.setLecture(lecture);
        List<Reaction> reactions = new ArrayList<>();
        reactions.add(reaction);
        when(lectureService.listReactions(1L))
                .thenThrow(LectureNotFoundException.class);

        mockMvc.perform(get("/lecture/{lectureId}/reactions", 1L)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andDo(print())
                .andExpect(status().isNotFound());

    }



}
