package nl.tudelft.oopp.application;

import static org.junit.jupiter.api.Assertions.assertEquals;

import nl.tudelft.oopp.application.entity.Answer;
import nl.tudelft.oopp.application.entity.User;
import nl.tudelft.oopp.application.entity.UserRole;
import nl.tudelft.oopp.application.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@AutoConfigureTestDatabase
public class UserTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    @Transactional
    public void saveAndRetrieveUser() {
        User user = new User();
        user.setName("John Doe");
        user.setIpAddress("192.168.1.1");
        user.setRole(UserRole.STUDENT);

        // save user
        user = userRepository.save(user);

        // retrieve user
        User user2 = userRepository.getOne(user.getId());
        assertEquals(user, user2);
    }
}
