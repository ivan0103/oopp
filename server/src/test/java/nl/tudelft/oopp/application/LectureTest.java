package nl.tudelft.oopp.application;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.transaction.Transactional;
import nl.tudelft.oopp.application.controller.dtos.PollAnswerRequest;
import nl.tudelft.oopp.application.controller.dtos.PollCreateRequest;
import nl.tudelft.oopp.application.entity.Lecture;
import nl.tudelft.oopp.application.entity.Poll;
import nl.tudelft.oopp.application.entity.Question;
import nl.tudelft.oopp.application.entity.Reaction;
import nl.tudelft.oopp.application.entity.User;
import nl.tudelft.oopp.application.exception.InvalidArgumentsException;
import nl.tudelft.oopp.application.exception.LectureNotFoundException;
import nl.tudelft.oopp.application.exception.LectureNotStartedYetException;
import nl.tudelft.oopp.application.exception.PollNotClosedException;
import nl.tudelft.oopp.application.exception.PollNotFoundException;
import nl.tudelft.oopp.application.exception.QuestionNotFoundException;
import nl.tudelft.oopp.application.exception.QuestionNotOwnedByUserException;
import nl.tudelft.oopp.application.exception.QuestionSelfUpvoteException;
import nl.tudelft.oopp.application.exception.ReactionNotFoundException;
import nl.tudelft.oopp.application.exception.UnauthorizedException;
import nl.tudelft.oopp.application.exception.UserBannedException;
import nl.tudelft.oopp.application.exception.UserNotFoundException;
import nl.tudelft.oopp.application.repository.BanRepository;
import nl.tudelft.oopp.application.repository.LectureRepository;
import nl.tudelft.oopp.application.repository.PollRepository;
import nl.tudelft.oopp.application.repository.ReactionRepository;
import nl.tudelft.oopp.application.repository.UserRepository;
import nl.tudelft.oopp.application.service.LectureService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@AutoConfigureTestDatabase
public class LectureTest {
    @Autowired
    private LectureRepository lectureRepository;
    @Autowired
    private ReactionRepository reactionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BanRepository banRepository;
    @Autowired
    private PollRepository pollRepository;
    @Autowired
    private LectureService lectureService;

    /**
     * Test constructor.
     */
    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testConstructor() {
        assertNotNull(new LectureService(
                lectureRepository,
                userRepository,
                reactionRepository,
                banRepository,
                pollRepository
        ));
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    public void scheduledLectureJoinTest() throws InvalidArgumentsException {
        // create a scheduled lecture
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.set(2001, Calendar.APRIL, 6, 4, 20);
        Lecture lecture = lectureService.createLecture("Test", "Test",
                new Date(Calendar.getInstance().getTime().getTime()
                        + calendar.getTime().getTime()));

        // attempt to verify lecture code
        assertThrows(LectureNotStartedYetException.class,
            () -> lectureService.verifyJoinCode(lecture.getStudentCode(),
                        "localhost"));

        // attempt to join lecture
        assertThrows(LectureNotStartedYetException.class,
            () -> lectureService.joinLecture(lecture.getStudentCode(),
                        "User", "localhost"));

        // attempt to verify moderator lecture code
        Assertions.assertDoesNotThrow(() -> lectureService.verifyJoinCode(
                lecture.getModeratorCode(),
                "localhost"));

        // attempt to join as moderator
        Assertions.assertDoesNotThrow(() -> lectureService.joinLecture(
                lecture.getModeratorCode(),
                "User", "localhost"));
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    public void generateLinkTest() {
        assertNotEquals(lectureService.generateRandomJoinCode(),
                lectureService.generateRandomJoinCode());
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    public void createPollLectureNotFoundExceptionTest() {

        assertThrows(LectureNotFoundException.class, () -> {
            List<String> options = new ArrayList<>();
            PollCreateRequest pollCreateRequest = new PollCreateRequest(
                    "Quiz", options, 0,
                    "code"
            );
            lectureService.createPoll(1, pollCreateRequest);
        });
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    public void createPollTest() {
        try {
            Lecture lecture = lectureService.createLecture("lecture name",
                    "Course Name", new Date());

            List<String> options = new ArrayList<>();
            PollCreateRequest pollCreateRequest = new PollCreateRequest(
                    "Quiz", options, 0,
                    lecture.getModeratorCode()
            );
            assertNotNull(lectureService.createPoll(lecture.getId(), pollCreateRequest));
        } catch (Exception e) {
            assertTrue("This action should not raise any exceptions",
                    false);
        }
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    public void answerPollExceptionTest() {
        try {
            Lecture lecture = lectureService.createLecture("lecture name",
                    "Course Name", new Date());

            User user = new User();
            user = lectureService.joinLecture(lecture.getStudentCode(),
                    user.getName(), "IPADDRESS").getUser();

            User finalUser = user;
            assertThrows(LectureNotFoundException.class, () -> {
                PollAnswerRequest pollAnswerRequest =
                        new PollAnswerRequest(0, finalUser.getId());
                lectureService.answerPoll(-1, pollAnswerRequest);
            });

            assertThrows(UserNotFoundException.class, () -> {
                PollAnswerRequest pollAnswerRequest =
                        new PollAnswerRequest(0, -1);
                lectureService.answerPoll(lecture.getId(), pollAnswerRequest);
            });

        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    public void answerPollTest() {
        try {
            Lecture lecture = lectureService.createLecture("lecture name",
                    "Course Name", new Date());

            User user = new User();
            user = lectureService.joinLecture(lecture.getStudentCode(),
                    user.getName(), "IPADDRESS").getUser();

            List<String> options = new ArrayList<>();
            options.add(new String("OPTION 0"));

            PollCreateRequest pollCreateRequest = new PollCreateRequest(
                    "Quiz", options, 0,
                    lecture.getModeratorCode()
            );
            Poll poll = lectureService.createPoll(lecture.getId(), pollCreateRequest)
                        .getPoll();

            assertNotNull("Poll should not be null", poll);
            assertNotNull("Stats should not be null", poll.getStatistics());
            assertTrue("Stats size should be > 0",
                    poll.getStatistics().size() > 0);
            PollAnswerRequest pollAnswerRequest =
                    new PollAnswerRequest(0, user.getId());
            lectureService.answerPoll(lecture.getId(), pollAnswerRequest);

        } catch (LectureNotFoundException e) {
            assertTrue("This action should not raise lecture not found "
                            + "exceptions",
                    false);
        } catch (UserNotFoundException e) {
            assertTrue("This action should not raise user not found "
                            + "exceptions",
                    false);
        } catch (UnauthorizedException e) {
            assertTrue("This action should not raise unauthorized "
                            + "exceptions",
                    false);
        } catch (Exception e) {
            assertTrue("This action should not raise any "
                            + "exceptions",
                    false);
        }
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    public void closePollTest() {
        try {
            Lecture lecture = lectureService.createLecture("lecture name",
                    "Course Name", new Date());

            assertThrows(PollNotFoundException.class, () -> {
                lectureService.closePoll(lecture.getId(), lecture.getModeratorCode());
            });
            List<String> options = new ArrayList<>();
            options.add(new String("OPTION 0"));

            PollCreateRequest pollCreateRequest = new PollCreateRequest(
                    "Quiz", options, 0,
                    lecture.getModeratorCode()
            );
            Poll poll = lectureService.createPoll(lecture.getId(), pollCreateRequest)
                    .getPoll();

            assertThrows(UnauthorizedException.class, () -> {
                lectureService.closePoll(lecture.getId(),
                        lecture.getStudentCode());
            });
            assertThrows(LectureNotFoundException.class, () -> {
                lectureService.closePoll(-1, lecture.getModeratorCode());
            });
            assertNotNull(lectureService.closePoll(lecture.getId(),
                    lecture.getModeratorCode()));

        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    public void removePollTest() {
        try {
            Lecture lecture = lectureService.createLecture("lecture name",
                    "Course Name", new Date());

            assertThrows(PollNotFoundException.class, () -> {
                lectureService.removePoll(lecture.getId(), lecture.getModeratorCode());
            });
            List<String> options = new ArrayList<>();
            options.add(new String("OPTION 0"));

            PollCreateRequest pollCreateRequest = new PollCreateRequest(
                    "Quiz", options, 0,
                    lecture.getModeratorCode()
            );
            Poll poll = lectureService.createPoll(lecture.getId(), pollCreateRequest)
                    .getPoll();

            assertThrows(UnauthorizedException.class, () -> {
                lectureService.removePoll(lecture.getId(),
                        lecture.getStudentCode());
            });
            assertThrows(LectureNotFoundException.class, () -> {
                lectureService.removePoll(-1, lecture.getModeratorCode());
            });
            assertThrows(PollNotClosedException.class, () -> {
                lectureService.removePoll(lecture.getId(),
                        lecture.getModeratorCode());
            });
            lectureService.closePoll(lecture.getId(), lecture.getModeratorCode());
            assertNotNull(lectureService.removePoll(lecture.getId(),
                    lecture.getModeratorCode()));

        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    public void endLectureTest() {
        try {
            Lecture lecture = lectureService.createLecture("lecture name",
                    "Course Name", new Date());
            assertThrows(LectureNotFoundException.class, () -> {
                lectureService.endLecture(-1, lecture.getModeratorCode());
            });

            assertThrows(UnauthorizedException.class, () -> {
                lectureService.endLecture(lecture.getId(), lecture.getStudentCode());
            });

            lectureService.endLecture(lecture.getId(), lecture.getModeratorCode());
            lectureService.endLecture(lecture.getId(), lecture.getModeratorCode());

        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    /**
     * Test creating a Faster reaction.
     */
    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testCreateSpeedReactionSlower() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        try {
            Reaction reaction = lectureService.speedReaction(lecture.getId(), true);
            assertNotNull(reaction);
        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    /**
     * Test creating a Slower reaction.
     */
    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testCreateSpeedReactionFaster() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        try {
            Reaction reaction = lectureService.speedReaction(lecture.getId(), false);
            assertNotNull(reaction);
        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    /**
     * Test creating Faster reaction with incorrect lecture id.
     */
    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testCreateReactionFasterLectureNotExistException() {
        assertThrows(LectureNotFoundException.class, () -> {
            lectureService.speedReaction(-1, false);
        });
    }

    /**
     * Test creating Slower reaction with incorrect lecture id.
     */
    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testCreateReactionSlowerLectureNotExistException() {
        assertThrows(LectureNotFoundException.class, () -> {
            lectureService.speedReaction(-1, true);
        });
    }

    /**
     * Test listing if the lecture ID is false.
     */
    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testListReactionsLectureIdFalse() {
        assertThrows(LectureNotFoundException.class, () -> {
            lectureService.listReactions(-1);
        });
    }

    /**
     * Test if there is nothing in the reaction database for a particular lecture.
     */
    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testListReactionNothing() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        List<Reaction> reactions;
        try {
            reactions = lectureService.listReactions(lecture.getId());
            assertTrue(reactions.size() == 0);

        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    /**
     * Test deleting a Reaction (every 5 minutes).
     */
    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testDeleteReaction() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        Reaction reaction = new Reaction();
        reaction.setLecture(lecture);
        reactionRepository.save(reaction);

        try {
            assertTrue("Before deletion, the size should be 1",
                    reactionRepository.findAllByLectureEquals(lecture).size() == 1);
            lectureService.deleteReaction(reaction.getId());
            assertTrue("After deletion, the size must be 0",
                    reactionRepository.findAllByLectureEquals(lecture).size() == 0);
        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    /**
     * Test deleting a Reaction that is not found.
     */
    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testDeleteReactionReactionNotFound() {
        assertThrows(ReactionNotFoundException.class, () -> {
            lectureService.deleteReaction(-1);
        });
    }

    /**
     * Test listing if the lecture ID is false.
     */
    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testListUsersLectureIdFalse() {
        assertThrows(LectureNotFoundException.class, () -> {
            lectureService.listUsers(-1);
        });
    }

    /**
     * Test if there is nothing in the user database for a particular lecture.
     */
    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testListUsersNothing() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        List<User> users;
        try {
            users = lectureService.listUsers(lecture.getId());
            assertTrue(users.size() == 0);

        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

}
