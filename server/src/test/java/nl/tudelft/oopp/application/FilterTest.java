package nl.tudelft.oopp.application;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.util.Date;
import java.util.Random;
import javax.transaction.Transactional;
import nl.tudelft.oopp.application.entity.Lecture;
import nl.tudelft.oopp.application.entity.Question;
import nl.tudelft.oopp.application.entity.User;
import nl.tudelft.oopp.application.exception.InvalidArgumentsException;
import nl.tudelft.oopp.application.exception.LectureNotFoundException;
import nl.tudelft.oopp.application.exception.LectureNotStartedYetException;
import nl.tudelft.oopp.application.exception.QuestionNotFoundException;
import nl.tudelft.oopp.application.exception.UserBannedException;
import nl.tudelft.oopp.application.exception.UserNotFoundException;
import nl.tudelft.oopp.application.repository.QuestionRepository;
import nl.tudelft.oopp.application.service.AnswerService;
import nl.tudelft.oopp.application.service.LectureService;
import nl.tudelft.oopp.application.service.QuestionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@AutoConfigureTestDatabase
public class FilterTest {

    @Autowired
    QuestionService questionService;

    @Autowired
    LectureService lectureService;

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    AnswerService answerService;

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testFilterNoQuestions() throws InvalidArgumentsException {
        // create test lecture
        Lecture lecture = lectureService.createLecture(
                "Test Lecture", "Test Course", new Date());

        assertDoesNotThrow(() -> questionService.listQuestions(
                lecture.getId(), "answered", "upvote", "localhost"));
        assertDoesNotThrow(() -> questionService.listQuestions(
                lecture.getId(), "unanswered", "upvote", "localhost"));
        assertDoesNotThrow(() -> questionService.listQuestions(
                lecture.getId(), null, "upvote", "localhost"));
    }

    @Test
    @Transactional
    void testFilterNQuestion() throws QuestionNotFoundException, LectureNotFoundException,
            InvalidArgumentsException, UserBannedException,
            LectureNotStartedYetException, UserNotFoundException {
        // create test lecture
        Lecture lecture = lectureService.createLecture(
                "Test Lecture", "Test Course", new Date());

        // join lecture
        User user = lectureService.joinLecture(lecture.getStudentCode(),
                "User", "localhost").getUser();

        // n questions
        int n = 100;
        int cnt = 0;

        Random random = new Random();
        for (int i = 0; i < n; i++) {
            // create question
            Question question =  questionService.createQuestion(
                    "Title " + i, "Content " + i,
                    lecture.getId(), user.getId(), "localhost");

            // randomly decide whether to answer the question
            if (random.nextBoolean()) {
                answerService.markAsAnswered(lecture.getId(), question.getId());
                cnt++;
            }
        }

        // check sizes
        assertEquals(n, questionRepository.findAllByLectureEquals(lecture).size());
        /* assertEquals(n - cnt, questionRepository
                .findAllByLectureEqualsAndAnswerNull(lecture).size());*/
        /*assertEquals(cnt, questionRepository
                .findAllByLectureEqualsAndAnswerNotNull(lecture).size());*/
    }
}
