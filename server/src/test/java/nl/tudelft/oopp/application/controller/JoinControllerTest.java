package nl.tudelft.oopp.application.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import lombok.extern.java.Log;
import nl.tudelft.oopp.application.controller.dtos.CheckCodeResponse;
import nl.tudelft.oopp.application.controller.dtos.LectureJoinResponse;
import nl.tudelft.oopp.application.entity.Lecture;
import nl.tudelft.oopp.application.entity.User;
import nl.tudelft.oopp.application.entity.UserRole;
import nl.tudelft.oopp.application.exception.LectureNotFoundException;
import nl.tudelft.oopp.application.exception.LectureNotStartedYetException;
import nl.tudelft.oopp.application.exception.UserBannedException;
import nl.tudelft.oopp.application.repository.UserRepository;
import nl.tudelft.oopp.application.service.AnswerService;
import nl.tudelft.oopp.application.service.LectureService;
import nl.tudelft.oopp.application.service.QuestionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(JoinController.class)
@Log
public class JoinControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private QuestionService questionService;
    @MockBean
    private LectureService lectureService;
    @MockBean
    private AnswerService answerService;
    @MockBean
    private UserRepository userRepository;

    User user;
    Lecture lecture;
    LectureJoinResponse joinResponse;
    CheckCodeResponse checkCodeSuccessResponse;
    CheckCodeResponse checkCodeLectureNotFoundResponse;
    CheckCodeResponse checkCodeUserBannedResponse;
    CheckCodeResponse checkCodeLectureNotYetResponse;

    private static ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void setup() {
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
        objectMapper.setTimeZone(TimeZone.getTimeZone("UTC"));

        this.lecture = new Lecture();
        lecture.setName("lectureName");
        lecture.setCourse("courseName");
        lecture.setStudentCode("studentCode");
        lecture.setModeratorCode("moderatorCode");

        this.user = new User();
        user.setLecture(lecture);
        user.setName("userName");
        user.setIpAddress("127.0.0.1");
        user.setRole(UserRole.STUDENT);

        this.joinResponse = new LectureJoinResponse(lecture, user);

        this.checkCodeSuccessResponse = new CheckCodeResponse(
                true, UserRole.STUDENT, false, null);
        this.checkCodeLectureNotFoundResponse = new CheckCodeResponse(
                false, null, false, null);
        this.checkCodeUserBannedResponse = new CheckCodeResponse(
                true, null, true, null);
        this.checkCodeLectureNotYetResponse = new CheckCodeResponse(
                true, null, false, new Date());
    }

    /**
     * Generic JSON parser.
     * @param o Object of any class
     * @return JSON String used for requests/response
     */
    public static String toJsonString(Object o) {
        try {
            return objectMapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            log.warning("Error parsing to json");
            e.printStackTrace();
            return null;
        }
    }

    @Test
    public void postJoinLectureTest()
            throws Exception {
        when(lectureService.joinLecture(lecture.getStudentCode(), user.getName(), "127.0.0.1"))
                .thenReturn(new LectureJoinResponse(lecture, user));
        mockMvc.perform(post("/join")
            .param("code", lecture.getStudentCode())
            .content(toJsonString(user))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(toJsonString(joinResponse)));
    }

    @Test
    public void postJoinLectureTestLectureNotFoundException()
            throws Exception {
        when(lectureService.joinLecture(lecture.getStudentCode(), user.getName(), "127.0.0.1"))
                .thenThrow(new LectureNotFoundException());
        mockMvc.perform(post("/join")
                .param("code", lecture.getStudentCode())
                .content(toJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void postJoinLectureTestUserBannedException()
            throws Exception {
        when(lectureService.joinLecture(lecture.getStudentCode(), user.getName(), "127.0.0.1"))
                .thenThrow(new UserBannedException());
        mockMvc.perform(post("/join")
                .param("code", lecture.getStudentCode())
                .content(toJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void postJoinLectureTestLectureNotStartedYetException()
            throws Exception {
        when(lectureService.joinLecture(lecture.getStudentCode(), user.getName(), "127.0.0.1"))
                .thenThrow(new LectureNotStartedYetException(new Date()));
        mockMvc.perform(post("/join")
                .param("code", lecture.getStudentCode())
                .content(toJsonString(user))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void getCheckCodeTest() throws Exception {
        when(lectureService.verifyJoinCode(lecture.getStudentCode(), "127.0.0.1"))
                .thenReturn(UserRole.STUDENT);
        mockMvc.perform(get("/checkcode")
                .param("code", lecture.getStudentCode())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(toJsonString(checkCodeSuccessResponse)));
    }

    @Test
    public void getCheckCodeTestLectureNotFoundException() throws Exception {
        when(lectureService.verifyJoinCode(lecture.getStudentCode(), "127.0.0.1"))
                .thenThrow(LectureNotFoundException.class);
        mockMvc.perform(get("/checkcode")
                .param("code", lecture.getStudentCode())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(toJsonString(checkCodeLectureNotFoundResponse)));
    }

    @Test
    public void getCheckCodeTestUserBannedException() throws Exception {
        when(lectureService.verifyJoinCode(lecture.getStudentCode(), "127.0.0.1"))
                .thenThrow(UserBannedException.class);
        mockMvc.perform(get("/checkcode")
                .param("code", lecture.getStudentCode())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(toJsonString(checkCodeUserBannedResponse)));
    }

    @Test
    public void getCheckCodeTestLectureNotStartedYetException() throws Exception {
        when(lectureService.verifyJoinCode(lecture.getStudentCode(), "127.0.0.1"))
                .thenThrow(new LectureNotStartedYetException(
                        checkCodeLectureNotYetResponse.getScheduledDate()));
        mockMvc.perform(get("/checkcode")
                .param("code", lecture.getStudentCode())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(toJsonString(checkCodeLectureNotYetResponse)));
    }

    @Test
    public void getLectureInfoTest() throws Exception {
        when(lectureService.getLectureByJoinCode(lecture.getStudentCode()))
                .thenReturn(lecture);
        mockMvc.perform(get("/join/info")
                .param("code", lecture.getStudentCode())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(toJsonString(lecture)));
    }

    @Test
    public void getLectureInfoTestLectureNotFoundException() throws Exception {
        when(lectureService.getLectureByJoinCode(lecture.getStudentCode()))
                .thenThrow(LectureNotFoundException.class);
        mockMvc.perform(get("/join/info")
                .param("code", lecture.getStudentCode())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
