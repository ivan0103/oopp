package nl.tudelft.oopp.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import nl.tudelft.oopp.application.entity.Question;
import nl.tudelft.oopp.application.repository.QuestionRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@AutoConfigureTestDatabase
public class SortingTest {
    @Autowired
    private QuestionRepository questionRepository;

    @Test
    void testOneQuestionInList() {
        Question question = new Question();
        question.setTitle("Title");
        question.setContent("Content");
        questionRepository.save(question);
        assertNotEquals(questionRepository.findAll(), Collections.emptyList());
    }
}
