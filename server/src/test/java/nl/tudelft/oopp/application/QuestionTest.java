package nl.tudelft.oopp.application;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import javax.transaction.Transactional;
import nl.tudelft.oopp.application.entity.Answer;
import nl.tudelft.oopp.application.entity.Ban;
import nl.tudelft.oopp.application.entity.Lecture;
import nl.tudelft.oopp.application.entity.Question;
import nl.tudelft.oopp.application.entity.User;
import nl.tudelft.oopp.application.exception.InvalidArgumentsException;
import nl.tudelft.oopp.application.exception.LectureNotFoundException;
import nl.tudelft.oopp.application.exception.QuestionNotFoundException;
import nl.tudelft.oopp.application.exception.QuestionNotOwnedByUserException;
import nl.tudelft.oopp.application.exception.QuestionSelfUpvoteException;
import nl.tudelft.oopp.application.exception.UserBannedException;
import nl.tudelft.oopp.application.exception.UserNotFoundException;
import nl.tudelft.oopp.application.repository.BanRepository;
import nl.tudelft.oopp.application.repository.LectureRepository;
import nl.tudelft.oopp.application.repository.QuestionRepository;
import nl.tudelft.oopp.application.repository.QuestionUpvoteRepository;
import nl.tudelft.oopp.application.repository.UserRepository;
import nl.tudelft.oopp.application.service.QuestionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;



@SpringBootTest
@AutoConfigureTestDatabase
public class QuestionTest {

    @Autowired
    private QuestionService questionService;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private LectureRepository lectureRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BanRepository banRepository;
    @Autowired
    private QuestionUpvoteRepository questionUpvoteRepository;

    /**
     * Test constructor.
     */
    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testConstructor() {
        assertNotNull(new QuestionService(
                questionRepository,
                lectureRepository,
                userRepository,
                questionUpvoteRepository,
                banRepository
        ));
    }

    /**
     * Test the sorting which based on relevancy.
     */
    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testRelevancySortSmall() {


        Question a = new Question();
        a.setTitle("Title A");
        a.setContent("Content A");
        a.setDate(1617376152405L);
        a.setUpvotes(5);

        Question b = new Question();
        b.setTitle("Title B");
        b.setContent("Content B");
        b.setDate(1617375912464L);
        b.setUpvotes(2);

        Question c = new Question();
        c.setTitle("Title C");
        c.setContent("Content C");
        c.setDate(1617376692465L);
        c.setUpvotes(1);

        Question d = new Question();
        d.setTitle("Title D");
        d.setContent("Content D");
        d.setDate(1617376032465L);
        d.setUpvotes(4);

        Question e = new Question();
        e.setTitle("Title E");
        e.setContent("Content E");
        e.setDate(1617376572465L);
        e.setUpvotes(9);

        List<Question> questionList = new ArrayList<>();
        questionList.add(a);
        questionList.add(b);
        questionList.add(c);
        questionList.add(d);
        questionList.add(e);

        Collections.sort(questionList, new Comparator<Question>() {
            @Override
            public int compare(Question o1, Question o2) {
                return (int) (o2.getDate() - o1.getDate());
            }
        });

        List<Question> expected = new ArrayList<>();
        expected.add(e);
        expected.add(c);
        expected.add(a);
        expected.add(d);
        expected.add(b);

        questionList = questionService.sortByRelevancy(questionList);
        assertTrue("Size must be equal", questionList.size() == expected.size());
        for (int i = 0; i < expected.size();i++) {
            assertTrue("Date must be prioritized first",
                    expected.get(i).getDate() == questionList.get(i).getDate());

            assertTrue("Upvotes must be prioritized second",
                    expected.get(i).getUpvotes() == questionList.get(i).getUpvotes());
        }
    }

    /**
     * Test listing if the lecture ID is false.
     */
    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testListQuestionLectureIdFalse() {
        assertThrows(LectureNotFoundException.class, () -> {
            questionService.listQuestions(-1, "answered", "time", null);
        });
    }

    /**
     * Test listing if the user is banned.
     */
    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testListQuestionUserBanned() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        Question question = new Question();
        question.setLecture(lecture);
        questionRepository.save(question);

        Ban ban = new Ban();
        ban.setBanIp("IPADDRESS");
        ban.setLecture(lecture);
        ban.setTimestamp(System.currentTimeMillis());
        banRepository.save(ban);

        assertThrows(UserBannedException.class, () -> {
            questionService.listQuestions(lecture.getId(), "answered", "time", ban.getBanIp());
        });
    }

    /**
     * Test if there is nothing in the question database for a particular lecture.
     */
    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testListQuestionNothing() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        List<Question> questions;
        try {
            questions = questionService.listQuestions(lecture.getId(),
                    "answered","upvotes", null);
            assertTrue(questions.size() == 0);

            questions = questionService.listQuestions(lecture.getId(),
                    "answered","time", null);
            assertTrue(questions.size() == 0);

            questions = questionService.listQuestions(lecture.getId(),
                    "answered","relevancy", null);
            assertTrue(questions.size() == 0);

            questions = questionService.listQuestions(lecture.getId(),
                    "unanswered","upvotes", null);
            assertTrue(questions.size() == 0);

            questions = questionService.listQuestions(lecture.getId(),
                    "unanswered","time", null);
            assertTrue(questions.size() == 0);

            questions = questionService.listQuestions(lecture.getId(),
                    "unanswered","relevancy", null);
            assertTrue(questions.size() == 0);


        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testQuestionListFilter() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        // Question without answer
        Question question1 = new Question();
        question1.setLecture(lecture);
        question1.setAnswer(null);
        questionRepository.save(question1);

        User user = new User();
        userRepository.save(user);

        // Question with answer
        Answer answer1 = new Answer();
        answer1.setAnswer("Answer content");
        answer1.setAnswerer(user);

        Question question2 = new Question();
        question2.setLecture(lecture);
        question2.setAnswer(answer1);
        questionRepository.save(question2);

        // Question with answer
        Answer answer2 = new Answer();
        answer2.setAnswer("Answer content");
        answer2.setAnswerer(user);

        Question question3 = new Question();
        question3.setLecture(lecture);
        question3.setAnswer(answer2);
        questionRepository.save(question3);

        List<Question> questions;
        try {
            questions = questionService.listQuestions(lecture.getId(),
                    "unanswered","upvotes", null);
            assertTrue("The size of unanswered question should be equal to 1 ",
                    questions.size() == 1);

            questions = questionService.listQuestions(lecture.getId(),
                    "unanswered","time", null);
            assertTrue("The size of unanswered question should be equal to 1 ",
                    questions.size() == 1);

            questions = questionService.listQuestions(lecture.getId(),"unanswered",
                    "relevancy", null);
            assertTrue("The size of unanswered question should be equal to 1 ",
                    questions.size() == 1);

            questions = questionService.listQuestions(lecture.getId(),"unanswered",
                    "else", null);
            assertTrue("The size of unanswered question should be equal to 1 ",
                    questions.size() == 1);

            questions = questionService.listQuestions(lecture.getId(),"answered",
                    "upvotes", null);
            assertTrue("The size of answered question should not be equal to 1",
                    questions.size() != 1);

            questions = questionService.listQuestions(lecture.getId(),"answered",
                    "time", null);
            assertTrue("The size of answered question should not be equal to 1",
                    questions.size() != 1);

            questions = questionService.listQuestions(lecture.getId(),"answered",
                    "relevancy", null);
            assertTrue("The size of answered question should not be equal to 1",
                    questions.size() != 1);

            questions = questionService.listQuestions(lecture.getId(),"answered",
                    "else", null);
            assertTrue("The size of answered question should not be equal to 1",
                    questions.size() != 1);

        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testQuestionListSortOptionsUpvote() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        Random random = new Random();

        int n = 100;
        for (int i = 0; i < n; i++) {
            Question question = new Question();
            question.setLecture(lecture);
            question.setUpvotes(random.nextInt(10000));
            questionRepository.save(question);
        }

        try {
            List<Question> questions = questionService.listQuestions(lecture.getId(),
                    "unanswered", "upvotes", null);
            assertTrue("The size should be equal to n",
                    questions.size() == n);

            for (int i = 0; i < n - 1; i++) {
                assertTrue("The higher upvotes should appear before the lower upvotes",
                        questions.get(i).getUpvotes() >= questions.get(i + 1)
                                .getUpvotes());
            }

        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testQuestionListSortOptionsTime() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        Random random = new Random();

        int n = 100;
        for (int i = 0; i < n; i++) {
            Question question = new Question();
            question.setLecture(lecture);
            question.setDate(random.nextInt((int) 1e6));
            questionRepository.save(question);
        }

        try {
            List<Question> questions = questionService.listQuestions(lecture.getId(),
                    "unanswered", "time", null);
            assertTrue("The size should be equal to n",
                    questions.size() == n);

            for (int i = 0; i < n - 1; i++) {
                assertTrue("The largest time should appear before the smaller time",
                        questions.get(i).getDate() >= questions.get(i + 1)
                                .getDate());
            }

        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testQuestionListFilterNotMatch() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        Question question1 = new Question();
        question1.setLecture(lecture);
        question1.setAnswer(null);
        questionRepository.save(question1);

        try {
            assertTrue(questionService.listQuestions(lecture.getId(),
                    "else", "time", null).size() == 1);

        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }

    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testCreateQuestionTitleAndContentException() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User user = new User();
        userRepository.save(user);

        assertThrows(InvalidArgumentsException.class, () -> {
            questionService.createQuestion("", "abcd",
                    lecture.getId(), user.getId(), null);
        });

        assertThrows(InvalidArgumentsException.class, () -> {
            questionService.createQuestion("abcd", "",
                    lecture.getId(), user.getId(), null);
        });
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testCreateQuestionLectureNotExistException() {
        User user = new User();
        userRepository.save(user);

        assertThrows(LectureNotFoundException.class, () -> {
            questionService.createQuestion("abcd", "abcd",
                    -1, user.getId(), null);
        });

    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testCreateQuestionUserBannedException() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User user = new User();
        userRepository.save(user);

        Ban ban = new Ban();
        ban.setBanIp("IPADDRESS");
        ban.setLecture(lecture);
        ban.setTimestamp(System.currentTimeMillis());
        banRepository.save(ban);

        assertThrows(UserBannedException.class, () -> {
            questionService.createQuestion("abcd", "abcd",
                    lecture.getId(), user.getId(), "IPADDRESS");
        });
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testCreateQuestionUserNotFoundException() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        assertThrows(UserNotFoundException.class, () -> {
            questionService.createQuestion("abcd", "abcd",
                    lecture.getId(), -1, null);
        });
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testCreateQuestion() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User user = new User();
        userRepository.save(user);

        try {
            Question question = questionService.createQuestion("abcd",
                    "abcd", lecture.getId(), user.getId(), null);
            assertNotNull(question);
        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testEditQuestionTitleOrContentEmptyException() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User user = new User();
        userRepository.save(user);

        Question question = new Question();
        question.setLecture(lecture);
        question.setAuthor(user);
        questionRepository.save(question);

        assertThrows(InvalidArgumentsException.class, () -> {
            questionService.editQuestion("",  lecture.getId(),
                    user.getId(), question.getId(), null);
        });
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testEditQuestionLectureNotFoundException() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User user = new User();
        userRepository.save(user);

        Question question = new Question();
        question.setLecture(lecture);
        question.setAuthor(user);
        questionRepository.save(question);

        assertThrows(LectureNotFoundException.class, () -> {
            questionService.editQuestion("abcd", -1,
                    user.getId(), question.getId(), null);
        });
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testEditQuestionUserBannedException() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User user = new User();
        userRepository.save(user);

        Ban ban = new Ban();
        ban.setBanIp("IPADDRESS");
        ban.setLecture(lecture);
        ban.setTimestamp(System.currentTimeMillis());
        banRepository.save(ban);

        Question question = new Question();
        question.setLecture(lecture);
        question.setAuthor(user);
        questionRepository.save(question);

        assertThrows(UserBannedException.class, () -> {
            questionService.editQuestion("abcd",  lecture.getId(),
                    user.getId(), question.getId(), "IPADDRESS");
        });
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testEditQuestionQuestionNotFoundException() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User user = new User();
        userRepository.save(user);

        Question question = new Question();
        question.setLecture(lecture);
        question.setAuthor(user);
        questionRepository.save(question);

        assertThrows(QuestionNotFoundException.class, () -> {
            questionService.editQuestion("abcd", lecture.getId(),
                    user.getId(), -1, null);
        });
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testEditQuestionUserNotOwnException() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User user = new User();
        userRepository.save(user);

        User stranger = new User();
        userRepository.save(stranger);

        Question question = new Question();
        question.setLecture(lecture);
        question.setAuthor(user);
        questionRepository.save(question);

        assertThrows(QuestionNotOwnedByUserException.class, () -> {
            questionService.editQuestion("abcd", lecture.getId(),
                    stranger.getId(), question.getId(), null);
        });
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testEditQuestion() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User user = new User();
        userRepository.save(user);

        Question question = new Question();
        question.setLecture(lecture);
        question.setAuthor(user);
        questionRepository.save(question);

        try {
            assertNotNull(questionService.editQuestion("abcd",
                    lecture.getId(),
                    user.getId(), question.getId(), null));
        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testGetQuestionLectureNotFoundException() {
        Question question = new Question();
        questionRepository.save(question);

        assertThrows(LectureNotFoundException.class, () -> {
            questionService.getQuestion(question.getId(), -1, null);
        });
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testGetQuestionUserBannedException() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User user = new User();
        userRepository.save(user);

        Ban ban = new Ban();
        ban.setBanIp("IPADDRESS");
        ban.setLecture(lecture);
        ban.setTimestamp(System.currentTimeMillis());
        banRepository.save(ban);

        Question question = new Question();
        question.setLecture(lecture);
        question.setAuthor(user);
        questionRepository.save(question);


        assertThrows(UserBannedException.class, () -> {
            questionService.getQuestion(question.getId(), lecture.getId(),
                    "IPADDRESS");
        });
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testGetQuestionQuestionNotFoundException() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);
        
        assertThrows(QuestionNotFoundException.class, () -> {
            questionService.getQuestion(-1, lecture.getId(), null);
        });
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testGetQuestion() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User user = new User();
        userRepository.save(user);

        Question question = new Question();
        question.setLecture(lecture);
        question.setAuthor(user);
        questionRepository.save(question);

        try {
            assertNotNull(questionService.getQuestion(question.getId(), lecture.getId(),
                    null));

        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testDeleteQuestionQuestionNotFound() {
        assertThrows(QuestionNotFoundException.class, () -> {
            questionService.deleteQuestion(-1);
        });
    }


    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testDeleteQuestion() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        Question question = new Question();
        question.setLecture(lecture);
        questionRepository.save(question);

        try {
            assertTrue("Before deletion, the size should be 1",
                    questionRepository.findAllByLectureEquals(lecture).size() == 1);
            questionService.deleteQuestion(question.getId());
            assertTrue("After deletion, the size must be 0",
                    questionRepository.findAllByLectureEquals(lecture).size() == 0);
        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testVoteOnQuestionLectureNotFoundException() {
        User user = new User();
        userRepository.save(user);

        Question question = new Question();
        question.setAuthor(user);
        questionRepository.save(question);

        assertThrows(LectureNotFoundException.class, () -> {
            questionService.voteOnQuestion(question.getId(), -1, user.getId(),
                    null);
        });
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testVoteOnQuestionUserBannedException() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User user = new User();
        userRepository.save(user);

        Ban ban = new Ban();
        ban.setBanIp("IPADDRESS");
        ban.setLecture(lecture);
        ban.setTimestamp(System.currentTimeMillis());
        banRepository.save(ban);

        Question question = new Question();
        question.setLecture(lecture);
        question.setAuthor(user);
        questionRepository.save(question);

        assertThrows(UserBannedException.class, () -> {
            questionService.voteOnQuestion(question.getId(), lecture.getId(),
                    user.getId(), "IPADDRESS");
        });
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testVoteOnQuestionQuestionNotFound() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User user = new User();
        userRepository.save(user);

        User stranger = new User();
        userRepository.save(stranger);

        Question question = new Question();
        question.setLecture(lecture);
        question.setAuthor(user);
        questionRepository.save(question);

        assertThrows(QuestionNotFoundException.class, () -> {
            questionService.voteOnQuestion(-1, lecture.getId(),
                    stranger.getId(), null);
        });
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testVoteOnQuestionUserNotFound() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User user = new User();
        userRepository.save(user);

        Question question = new Question();
        question.setLecture(lecture);
        question.setAuthor(user);
        questionRepository.save(question);

        assertThrows(QuestionSelfUpvoteException.class, () -> {
            questionService.voteOnQuestion(question.getId(), lecture.getId(),
                    user.getId(), null);
        });
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testVoteOnQuestionNewUpvote() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User user = new User();
        userRepository.save(user);

        User stranger = new User();
        userRepository.save(stranger);

        Question question = new Question();
        question.setLecture(lecture);
        question.setAuthor(user);
        questionRepository.save(question);

        try {
            Question onlyQuestion = questionRepository.findById(question.getId()).get();
            assertTrue("Before upvote should be 0",
                    onlyQuestion.getUpvotes() == 0);

            questionService.voteOnQuestion(question.getId(), lecture.getId(),
                    stranger.getId(), null);
            onlyQuestion = questionRepository.findById(question.getId()).get();

            assertTrue("After upvote should be 1",
                    onlyQuestion.getUpvotes() == 1);
        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }

    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testVoteOnQuestionUnupvote() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User user = new User();
        userRepository.save(user);

        User stranger = new User();
        userRepository.save(stranger);

        Question question = new Question();
        question.setLecture(lecture);
        question.setAuthor(user);
        questionRepository.save(question);

        Question onlyQuestion;
        try {
            questionService.voteOnQuestion(question.getId(), lecture.getId(),
                    stranger.getId(), null);
            onlyQuestion = questionRepository.findById(question.getId()).get();
            assertTrue("After upvote should be 1",
                    onlyQuestion.getUpvotes() == 1);

            questionService.voteOnQuestion(question.getId(), lecture.getId(),
                    stranger.getId(), null);
            onlyQuestion = questionRepository.findById(question.getId()).get();
            assertTrue("After UnUpvote should be 0",
                    onlyQuestion.getUpvotes() == 0);
        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testGetQuestionUpvoteLectureNotFoundException() {
        Question question = new Question();
        questionRepository.save(question);

        assertThrows(LectureNotFoundException.class, () -> {
            questionService.getQuestionUpvotes(question.getId(), -1);
        });

    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testGetQuestionUpvoteQuestionNotFoundException() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        Question question = new Question();
        question.setLecture(lecture);
        questionRepository.save(question);

        assertThrows(QuestionNotFoundException.class, () -> {
            questionService.getQuestionUpvotes(-1, lecture.getId());
        });

    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testGetQuestionUpvote() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User author = new User();
        userRepository.save(author);

        Question question = new Question();
        question.setLecture(lecture);
        question.setAuthor(author);
        questionRepository.save(question);

        User user1 = new User();
        userRepository.save(user1);

        User user2 = new User();
        userRepository.save(user2);

        try {
            questionService.voteOnQuestion(question.getId(), lecture.getId(),
                    user1.getId(), null);
            questionService.voteOnQuestion(question.getId(), lecture.getId(),
                    user2.getId(), null);
        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }

        try {
            Set<User> users = questionService.getQuestionUpvotes(question.getId(),
                    lecture.getId());
            assertTrue("2 upvotes must be received", users.size() == 2);
        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testClaimQuestionQuestionNotFoundException() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User user = new User();
        userRepository.save(user);

        Question question = new Question();
        question.setLecture(lecture);
        question.setAuthor(user);
        questionRepository.save(question);

        assertThrows(QuestionNotFoundException.class, () -> {
            questionService.claimQuestion(-1, user.getId());
        });
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testClaimQuestion() {
        Lecture lecture = new Lecture();
        lectureRepository.save(lecture);

        User user = new User();
        userRepository.save(user);

        Question question = new Question();
        question.setLecture(lecture);
        question.setAuthor(user);
        questionRepository.save(question);

        Question onlyQuestion;
        try {
            questionService.claimQuestion(question.getId(), user.getId());
            onlyQuestion = questionRepository.findById(question.getId()).get();

            assertEquals(onlyQuestion.getClaimedBy().getId(), user.getId());
        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testGetUserUserNotFoundException() {
        assertThrows(UserNotFoundException.class, () -> {
            questionService.getUser(-1);
        });
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testGetUser() {
        User user = new User();
        userRepository.save(user);
        try {
            assertEquals(user.getId(), questionService.getUser(user.getId()).getId());
        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }
}