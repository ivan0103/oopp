package nl.tudelft.oopp.application;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Date;
import javax.transaction.Transactional;
import nl.tudelft.oopp.application.entity.Lecture;
import nl.tudelft.oopp.application.entity.Question;
import nl.tudelft.oopp.application.entity.User;
import nl.tudelft.oopp.application.exception.InvalidArgumentsException;
import nl.tudelft.oopp.application.exception.LectureNotFoundException;
import nl.tudelft.oopp.application.exception.QuestionNotClaimedException;
import nl.tudelft.oopp.application.exception.QuestionNotFoundException;
import nl.tudelft.oopp.application.exception.UserNotFoundException;
import nl.tudelft.oopp.application.repository.LectureRepository;
import nl.tudelft.oopp.application.repository.QuestionRepository;
import nl.tudelft.oopp.application.repository.UserRepository;
import nl.tudelft.oopp.application.service.AnswerService;
import nl.tudelft.oopp.application.service.LectureService;
import nl.tudelft.oopp.application.service.QuestionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@AutoConfigureTestDatabase
public class AnswerTest {

    @Autowired
    private LectureService lectureService;
    @Autowired
    private QuestionService questionService;
    @Autowired
    private AnswerService answerService;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private LectureRepository lectureRepository;
    @Autowired
    private UserRepository userRepository;

    /**
     * Test constructor.
     */
    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void testConstructor() {
        assertNotNull(new AnswerService(
                questionRepository,
                lectureRepository,
                userRepository
        ));
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void createAnswerTest() {
        try {
            Lecture lecture = lectureService.createLecture("lecture Name",
                    "Course Name", new Date());

            User user = lectureService.joinLecture(lecture.getModeratorCode(),
                    "name","IPADDRESS").getUser();


            Question question = questionService.createQuestion("title",
                    "content",lecture.getId(),user.getId(), "IPADDRESS");

            assertThrows(InvalidArgumentsException.class, () -> {
                answerService.createAnswer("", user.getId(),
                        question.getId(), lecture.getId());
            });

            assertThrows(UserNotFoundException.class, () -> {
                answerService.createAnswer("content",
                        -1, question.getId(), lecture.getId());
            });

            assertThrows(LectureNotFoundException.class, () -> {
                answerService.createAnswer("content",
                        user.getId(), question.getId(), -1);
            });

            assertThrows(QuestionNotFoundException.class, () -> {
                answerService.createAnswer("content",
                        user.getId(), -1, lecture.getId());
            });

            assertThrows(QuestionNotClaimedException.class, () -> {
                answerService.createAnswer("content",
                        user.getId(), question.getId(), lecture.getId());
            });

            questionService.claimQuestion(question.getId(), user.getId());

            answerService.createAnswer("content",
                    user.getId(), question.getId(), lecture.getId());
        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }

    @Test
    @Transactional(Transactional.TxType.SUPPORTS)
    void markAnswerTest() {
        try {
            Lecture lecture = lectureService.createLecture("lecture Name",
                    "Course Name", new Date());

            User user = lectureService.joinLecture(lecture.getModeratorCode(),
                    "name","IPADDRESS").getUser();


            Question question = questionService.createQuestion("title",
                    "content",lecture.getId(),user.getId(), "IPADDRESS");


            assertThrows(LectureNotFoundException.class, () -> {
                answerService.markAsAnswered(-1, question.getId());
            });

            assertThrows(QuestionNotFoundException.class, () -> {
                answerService.markAsAnswered(lecture.getId(), -1);
            });
            answerService.markAsAnswered(lecture.getId(), question.getId());
        } catch (Exception e) {
            assertTrue("This should not raise any exceptions", false);
        }
    }
}
