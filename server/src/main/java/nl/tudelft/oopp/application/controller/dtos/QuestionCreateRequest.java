package nl.tudelft.oopp.application.controller.dtos;

import lombok.Data;

@Data
public class QuestionCreateRequest {
    private long userID;
    private String title;
    private String content;
}
