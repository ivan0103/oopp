package nl.tudelft.oopp.application.controller;

import javax.servlet.http.HttpServletRequest;
import lombok.extern.java.Log;
import nl.tudelft.oopp.application.controller.dtos.CheckCodeResponse;
import nl.tudelft.oopp.application.controller.dtos.LectureJoinResponse;
import nl.tudelft.oopp.application.entity.Lecture;
import nl.tudelft.oopp.application.entity.User;
import nl.tudelft.oopp.application.exception.LectureNotFoundException;
import nl.tudelft.oopp.application.exception.LectureNotStartedYetException;
import nl.tudelft.oopp.application.exception.UserBannedException;
import nl.tudelft.oopp.application.service.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Log
@RestController
public class JoinController {

    private final LectureService lectureService;

    @Autowired
    public JoinController(LectureService lectureService) {
        this.lectureService = lectureService;
    }


    /**
     * Join a user into a lecture.
     * @param user user who is joining.
     * @param code the join code.
     * @return lecture and user info.
     */
    @PostMapping("join")
    public ResponseEntity<LectureJoinResponse> joinLecture(HttpServletRequest httpBody,
                                                            @RequestBody User user,
                                                            @RequestParam String code) {
        log.info("REQUEST TO JOIN LECTURE WITH PATH VARIABLE ACCESS-CODE = "
                + code
                + ", POST INFORMATION = " + user
                + " - BY USER WITH IP ADDRESS = " + httpBody.getRemoteAddr());
        try {
            return ResponseEntity.ok(lectureService.joinLecture(code,
                    user.getName(), httpBody.getRemoteAddr()));
        } catch (LectureNotFoundException exception) {
            log.warning("LECTURE NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (UserBannedException e) {
            log.warning("USER BANNED");
            return ResponseEntity.status(403).build();
        } catch (LectureNotStartedYetException e) {
            log.warning("LECTURE NOT STARTED");
            return ResponseEntity.status(403).build();
        }
    }


    /**
     * Checks whether a join code is correct.
     * @param code the join code.
     * @return Info on whether the code is correct.
     */
    @GetMapping("checkcode")
    public CheckCodeResponse checkCode(HttpServletRequest httpBody, @RequestParam String code) {
        log.info("REQUEST TO VERIFY ACCESS CODE WITH PATH VARIABLE ACCESS-CODE = "
                + code
                + " - BY USER WITH IP ADDRESS = " + httpBody.getRemoteAddr());
        try {
            return new CheckCodeResponse(true,
                    lectureService.verifyJoinCode(code, httpBody.getRemoteAddr()), false, null);
        } catch (LectureNotFoundException e) {
            log.warning("LECTURE NOT FOUND");
            return new CheckCodeResponse(false, null, false, null);
        } catch (UserBannedException e) {
            log.warning("USER BANNED");
            return new CheckCodeResponse(true, null, true, null);
        } catch (LectureNotStartedYetException e) {
            log.warning("LECTURE NOT STARTED");
            return new CheckCodeResponse(true, null, false, e.getScheduledDate());
        }
    }

    /**
     * Gets information about a lecture based on a join code.
     * @param code The join code.
     * @return Lecture info.
     */
    @GetMapping("join/info")
    public ResponseEntity<Lecture> getLectureInfo(HttpServletRequest httpBody,
                                                  @RequestParam String code) {
        log.info("REQUEST TO GET LECTURE INFOMATION WITH PATH VARIABLE ACCESS-CODE = "
                + code
                + " - BY USER WITH IP ADDRESS = " + httpBody.getRemoteAddr());
        try {
            return ResponseEntity.ok(lectureService.getLectureByJoinCode(code));
        } catch (LectureNotFoundException e) {
            log.warning("LECTURE NOT FOUND");
            return ResponseEntity.notFound().build();
        }
    }
}
