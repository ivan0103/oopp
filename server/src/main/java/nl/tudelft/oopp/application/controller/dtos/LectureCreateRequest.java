package nl.tudelft.oopp.application.controller.dtos;

import java.util.Date;
import lombok.Data;

@Data
public class LectureCreateRequest {
    private String name;
    private String course;
    private Date time;
}
