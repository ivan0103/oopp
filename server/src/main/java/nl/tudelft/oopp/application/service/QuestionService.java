package nl.tudelft.oopp.application.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.java.Log;
import nl.tudelft.oopp.application.entity.Ban;
import nl.tudelft.oopp.application.entity.Lecture;
import nl.tudelft.oopp.application.entity.Question;
import nl.tudelft.oopp.application.entity.QuestionUpvote;
import nl.tudelft.oopp.application.entity.User;
import nl.tudelft.oopp.application.entity.UserRole;
import nl.tudelft.oopp.application.exception.InvalidArgumentsException;
import nl.tudelft.oopp.application.exception.LectureNotFoundException;
import nl.tudelft.oopp.application.exception.QuestionNotFoundException;
import nl.tudelft.oopp.application.exception.QuestionNotOwnedByUserException;
import nl.tudelft.oopp.application.exception.QuestionSelfUpvoteException;
import nl.tudelft.oopp.application.exception.UserBannedException;
import nl.tudelft.oopp.application.exception.UserNotFoundException;
import nl.tudelft.oopp.application.repository.BanRepository;
import nl.tudelft.oopp.application.repository.LectureRepository;
import nl.tudelft.oopp.application.repository.QuestionRepository;
import nl.tudelft.oopp.application.repository.QuestionUpvoteRepository;
import nl.tudelft.oopp.application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Log
@Service
public class QuestionService {
    private final QuestionRepository questionRepository;
    private final LectureRepository lectureRepository;
    private final UserRepository userRepository;
    private final QuestionUpvoteRepository questionUpvoteRepository;
    private final BanRepository banRepository;

    /**
     * Sets up a question service.
     */
    @Autowired
    public QuestionService(QuestionRepository questionRepository,
                           LectureRepository lectureRepository,
                           UserRepository userRepository,
                           QuestionUpvoteRepository questionUpvoteRepository,
                           BanRepository banRepository) {
        this.questionRepository = questionRepository;
        this.lectureRepository = lectureRepository;
        this.userRepository = userRepository;
        this.questionUpvoteRepository = questionUpvoteRepository;
        this.banRepository = banRepository;
    }

    /**
     * Sorting a list of given question from index_1 to index_2.
     * Distribution sort. Time complexity O(max).
     * In general cases, max <= n as the number of upvote will rarely exceed the number of
     * total questions.
     * This algorithm will most of the time work better than any traditional O(nLogN) sorting
     * algorithm.
     *
     * @param questions question that need to be sorted
     * @param st sort from this index
     * @param nd to this index
     */
    private void distributionSort(List<Question> questions, int st, int nd) {
        int max = 0;
        for (int i = st; i <= nd; i++) {
            max = Math.max(max, questions.get(i).getUpvotes());
        }
        LinkedList<Question>[] distributionArray = new LinkedList[max + 1];
        for (int i = 0; i <= max; i++) {
            distributionArray[i] = new LinkedList<>();
        }

        // Distributing question based on the number of upvotes.
        for (int i = st; i <= nd; i++) {
            Question question = questions.get(i);
            distributionArray[question.getUpvotes()].add(question);
        }
        // Since the jpa repository return arraylist type
        // (check this by using List.getClass().getName())
        //
        // The set method can be accomplished in O(1).
        for (int i = max; i >= 0; i--) {
            while (!distributionArray[i].isEmpty()) {
                questions.set(st, distributionArray[i].pollFirst());
                st++;
            }
        }
    }

    /**
     * Perform sort by relevancy.
     * The list of question will be group by n minutes, which mean all the questions which are
     * asked within n minutes time frame will be considered as relevant question.
     * Eventually, after grouped, the sorting algorithm will be performed to sort all the questions
     * in each group by the number of upvotes.
     * @param questions the list of questions that need to be sorted.
     * @return a desired list of questions.
     */
    public List<Question> sortByRelevancy(List<Question> questions) {
        double n = 5.0; // group by n minutes
        long millisecond = (long) (n * 60000);

        // Two pointers technique. Time complexity O(n)
        int idx = 0;
        while (idx < questions.size()) {
            int nxt = idx;

            while (nxt + 1 < questions.size()
                    && questions.get(idx).getDate()
                    - questions.get(nxt + 1).getDate() <= millisecond) {
                nxt++;
            }
            distributionSort(questions, idx, nxt);
            idx = nxt + 1;
        }
        return questions;
    }

    /**
     * Lists questions in a given lecture.
     * @param lectureId lecture to list the questions of.
     * @param sort sort by: upvotes, relevancy or time
     * @param filter filter by: answered or unanswered
     * @return list of questions
     * @throws LectureNotFoundException thrown if lecture cannot be found.
     * @throws UserBannedException thrown if the user is banned.
     */
    public List<Question> listQuestions(long lectureId, String filter,
                                        String sort, String userAddress)
            throws LectureNotFoundException, UserBannedException {
        // get lecture
        Optional<Lecture> lectureOptional = lectureRepository.findById(lectureId);
        if (lectureOptional.isEmpty()) {
            throw new LectureNotFoundException();
        }
        Lecture lecture = lectureOptional.get();

        // check if banned
        Optional<Ban> optionalBan = banRepository.findByBanIpAndLecture(userAddress, lecture);
        if (optionalBan.isPresent()) {
            throw new UserBannedException();
        }

        if (filter != null && filter.equals("answered")) {
            // filter by answered
            switch (sort) {
                case "upvotes":
                    return questionRepository
                            .findAllByLectureEqualsAndAnswerNotNullOrderByUpvotesDesc(
                                    lectureOptional.get());
                case "relevancy":
                    return sortByRelevancy(questionRepository
                            .findAllByLectureEqualsAndAnswerNotNullOrderByDateDesc(
                                    lectureOptional.get()));
                case "time":
                    return questionRepository.findAllByLectureEqualsAndAnswerNotNullOrderByDateDesc(
                            lectureOptional.get());
                default:
                    return questionRepository.findAllByLectureEqualsAndAnswerNotNull(
                            lectureOptional.get());
            }
        } else if (filter != null && filter.equals("unanswered")) {
            // filter by unanswered
            switch (sort) {
                case "upvotes":
                    return questionRepository
                            .findAllByLectureEqualsAndAnswerNullOrderByUpvotesDesc(
                                    lectureOptional.get());
                case "relevancy":
                    return sortByRelevancy(questionRepository
                            .findAllByLectureEqualsAndAnswerNullOrderByDateDesc(
                                    lectureOptional.get()));
                case "time":
                    return questionRepository.findAllByLectureEqualsAndAnswerNullOrderByDateDesc(
                            lectureOptional.get());
                default:
                    return questionRepository.findAllByLectureEqualsAndAnswerNull(
                            lectureOptional.get());
            }
        }
        return questionRepository.findAllByLectureEquals(lectureOptional.get());
    }


    /**
     * Creates a new question.
     * @param title title of the question.
     * @param content content of the question.
     * @param lectureId lecture id of the lecture where the question should be posted.
     * @param userId user id of the posting user.
     * @throws LectureNotFoundException thrown when the lecture is not found.
     * @throws UserNotFoundException thrown when the user is not found.
     * @throws InvalidArgumentsException thrown when the arguments are incorrect.
     * @throws UserBannedException thrown if the user is banned.
     */
    public Question createQuestion(String title, String content, long lectureId,
                                   long userId, String userAddress)
            throws LectureNotFoundException, UserNotFoundException,
            InvalidArgumentsException, UserBannedException {
        // check if null
        if (StringUtils.isEmpty(title) || StringUtils.isEmpty(content)) {
            throw new InvalidArgumentsException();
        }

        // get lecture
        Optional<Lecture> lectureOptional = lectureRepository.findById(lectureId);
        if (lectureOptional.isEmpty()) {
            throw new LectureNotFoundException();
        }
        Lecture lecture = lectureOptional.get();

        // check if user is banned
        Optional<Ban> optionalBan = banRepository.findByBanIpAndLecture(userAddress, lecture);
        if (optionalBan.isPresent()) {
            throw new UserBannedException();
        }

        // get author
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isEmpty()) {
            throw new UserNotFoundException();
        }

        // create question
        Question question = new Question();
        question.setTitle(title);
        question.setContent(content);
        question.setLecture(lectureOptional.get());
        question.setAuthor(userOptional.get());
        question.setDate(System.currentTimeMillis());

        // save question
        return questionRepository.save(question);
    }

    /**
     * Edits a question.
     * @param content The new content of the question.
     * @param lectureId The id of the lecture.
     * @param userId The id of the user.
     * @param questionId The id of the question.
     * @param userAddress The address of the user.
     * @return The edited question.
     * @throws InvalidArgumentsException thrown if the arguments are wrong.
     * @throws LectureNotFoundException thrown if the lecture doesn't exist.
     * @throws UserBannedException thrown if the user doesn't exist.
     * @throws QuestionNotFoundException thrown if the question doesn't exist.
     * @throws QuestionNotOwnedByUserException thrown if the question is not owned by the user.
     * @throws UserNotFoundException thrown if the editer id is not found.
     */
    public Question editQuestion(String content, long lectureId,
                                 long userId, long questionId, String userAddress)
            throws InvalidArgumentsException, LectureNotFoundException, UserBannedException,
            QuestionNotFoundException, QuestionNotOwnedByUserException,
            UserNotFoundException {
        // check if null
        if (StringUtils.isEmpty(content)) {
            throw new InvalidArgumentsException();
        }

        // get lecture
        Optional<Lecture> lectureOptional = lectureRepository.findById(lectureId);
        if (lectureOptional.isEmpty()) {
            throw new LectureNotFoundException();
        }
        Lecture lecture = lectureOptional.get();

        // check if user is banned
        Optional<Ban> optionalBan = banRepository.findByBanIpAndLecture(userAddress, lecture);
        if (optionalBan.isPresent()) {
            throw new UserBannedException();
        }

        // get question
        Optional<Question> questionOptional = questionRepository.findById(questionId);
        if (questionOptional.isEmpty()) {
            throw new QuestionNotFoundException();
        }
        Question question = questionOptional.get();

        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isEmpty()) {
            throw new UserNotFoundException();
        }

        User user = userOptional.get();
        // check if user owns this question
        if (!(user.getRole() == UserRole.LECTURER
                || user.getRole() == UserRole.MODERATOR)
                && question.getAuthor().getId() != userId) {
            throw new QuestionNotOwnedByUserException();
        }

        // edit question
        question.setContent(content);

        // save question
        return questionRepository.save(question);
    }

    /**
     * Gets a question by id and lecture id.
     * @param questionId the id of the question.
     * @param lectureId the id of the question.
     * @return Found question
     * @throws LectureNotFoundException exception
     * @throws QuestionNotFoundException exception
     * @throws UserBannedException thrown if the user is banned.
     */
    public Question getQuestion(long questionId, long lectureId, String userAddress)
            throws LectureNotFoundException, QuestionNotFoundException, UserBannedException {
        // get lecture
        Optional<Lecture> lectureOptional = lectureRepository.findById(lectureId);
        if (lectureOptional.isEmpty()) {
            throw new LectureNotFoundException();
        }
        Lecture lecture = lectureOptional.get();

        // check if user is banned
        Optional<Ban> optionalBan = banRepository.findByBanIpAndLecture(userAddress, lecture);
        if (optionalBan.isPresent()) {
            throw new UserBannedException();
        }

        // get question
        Optional<Question> questionOptional =
                questionRepository.findByIdAndLectureEquals(questionId, lecture);
        if (questionOptional.isEmpty()) {
            throw new QuestionNotFoundException();
        }
        return questionOptional.get();
    }

    /**
     * Deletes a question by id.
     * @param questionId id of question to be deleted
     * @throws QuestionNotFoundException when question is not found.
     */
    public void deleteQuestion(long questionId)
            throws QuestionNotFoundException {
        // get question
        Optional<Question> questionOptional = questionRepository.findById(questionId);
        if (questionOptional.isEmpty()) {
            throw new QuestionNotFoundException();
        }
        Question question = questionOptional.get();

        // delete upvotes
        questionUpvoteRepository.deleteAllByQuestion(question);

        // delete question
        questionRepository.delete(question);
    }

    /**
     * Upvotes a question with a given id and lecture id
     * or downvotes the question if it was previously
     * upvoted by the user.
     * @param questionId the id of the question.
     * @param lectureId the id of the lecture.
     * @param userId the id of the user voting.
     * @throws LectureNotFoundException thrown if the lecture doesn't exist.
     * @throws QuestionNotFoundException thrown if the question doesn't exist.
     * @throws UserNotFoundException thrown if the user doesn't exist.
     * @throws QuestionSelfUpvoteException thrown if the user tries to upvote their own question.
     * @throws UserBannedException thrown if the user is banned.
     */
    public void voteOnQuestion(long questionId, long lectureId,
                               long userId, String userAddress)
            throws LectureNotFoundException, QuestionNotFoundException, UserNotFoundException,
            QuestionSelfUpvoteException, UserBannedException {
        // get lecture
        Optional<Lecture> lectureOptional = lectureRepository.findById(lectureId);
        if (lectureOptional.isEmpty()) {
            throw new LectureNotFoundException();
        }
        Lecture lecture = lectureOptional.get();

        // check if user is banned
        Optional<Ban> optionalBan = banRepository.findByBanIpAndLecture(userAddress, lecture);
        if (optionalBan.isPresent()) {
            throw new UserBannedException();
        }

        // get question
        Optional<Question> questionOptional = questionRepository
                .findByIdAndLectureEquals(questionId, lecture);
        if (questionOptional.isEmpty()) {
            throw new QuestionNotFoundException();
        }
        Question question = questionOptional.get();

        // get user
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isEmpty()) {
            throw new UserNotFoundException();
        }
        User user = userOptional.get();

        // check if own question
        if (question.getAuthor().getId() == user.getId()) {
            throw new QuestionSelfUpvoteException();
        }

        // get votes
        List<QuestionUpvote> upvotes = questionUpvoteRepository.findAllByQuestion(question);

        // check if voted
        Optional<QuestionUpvote> upvoteOptional = upvotes.stream()
                .filter((v) -> v.getUser().getId() == user.getId()).findAny();
        if (upvoteOptional.isPresent()) {
            // downvote
            questionUpvoteRepository.delete(upvoteOptional.get());

            // update question
            question.setUpvotes(question.getUpvotes() - 1);
            questionRepository.save(question);
        } else {
            // upvote
            QuestionUpvote upvote = new QuestionUpvote();
            upvote.setQuestion(question);
            upvote.setUser(user);
            questionUpvoteRepository.save(upvote);

            // update question
            question.setUpvotes(question.getUpvotes() + 1);
            questionRepository.save(question);
        }
    }

    /**
     * Gets upvotes of a question.
     * @param questionId The id of the question.
     * @param lectureId The id of the lecture where the question is.
     * @return Set of users that upvoted the question.
     * @throws LectureNotFoundException thrown if the lecture was not found.
     * @throws QuestionNotFoundException thrown if the question was not found.
     */
    public Set<User> getQuestionUpvotes(long questionId, long lectureId)
            throws LectureNotFoundException, QuestionNotFoundException {
        // get lecture
        Optional<Lecture> lectureOptional = lectureRepository.findById(lectureId);
        if (lectureOptional.isEmpty()) {
            throw new LectureNotFoundException();
        }

        // get question
        Optional<Question> questionOptional = questionRepository
                .findByIdAndLectureEquals(questionId, lectureOptional.get());
        if (questionOptional.isEmpty()) {
            throw new QuestionNotFoundException();
        }
        Question question = questionOptional.get();

        return questionUpvoteRepository.findAllByQuestion(question).stream()
                .map(QuestionUpvote::getUser)
                .collect(Collectors.toSet());
    }

    /**
     * Claim the  question that belongs to the question id by the user that belongs to the user id.
     * @param questionId The id of the question.
     * @param userId the id of the user.
     * @throws QuestionNotFoundException When the question cannot be found.
     * @throws UserNotFoundException When the user cannot be found.
     */
    public Question claimQuestion(long questionId, long userId)
            throws QuestionNotFoundException, UserNotFoundException {
        Optional<Question> questionOptional =
                questionRepository.findById(questionId);
        if (questionOptional.isEmpty()) {
            throw new QuestionNotFoundException();
        }
        Question questionToClaim = questionOptional.get();
        questionToClaim.setClaimedBy(getUser(userId));
        return questionRepository.save(questionToClaim);
    }

    /**
     * Get the user of the user id.
     * @param userId The id of the user.
     * @return The user.
     * @throws UserNotFoundException When the user is not found.
     */
    public User getUser(long userId)
            throws  UserNotFoundException {
        // get author
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isEmpty()) {
            throw new UserNotFoundException();
        }
        return userOptional.get();
    }
}
