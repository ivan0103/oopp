package nl.tudelft.oopp.application.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import nl.tudelft.oopp.application.entity.Lecture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LectureRepository extends JpaRepository<Lecture, Long> {
    Optional<Lecture> findFirstByStudentCodeOrModeratorCode(
            String studentCode, String moderatorCode);

    List<Lecture> findAllByTimeAfter(Date time);

    List<Lecture> findAllByTimeBefore(Date time);

    Optional<Lecture> findFirstById(long id);

    Optional<Lecture> findFirstByModeratorCode(String moderatorCode);
}
