package nl.tudelft.oopp.application.controller.dtos;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonMerge;
import lombok.Data;
import nl.tudelft.oopp.application.entity.Lecture;
import nl.tudelft.oopp.application.entity.User;

@Data
public class LectureJoinResponse {
    private final Lecture lecture;
    private final User user;
}
