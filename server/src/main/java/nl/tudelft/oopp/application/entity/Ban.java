package nl.tudelft.oopp.application.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.Data;

@Entity
@Data
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Ban {
    @Id
    @GeneratedValue
    private long id;
    /**
     * The IP of the user that was banned.
     */
    private String banIp;
    /**
     * Lecture in which the user was banned.
     */
    @ManyToOne
    private Lecture lecture;
    /**
     * When was this ban created.
     */
    private long timestamp;
}
