package nl.tudelft.oopp.application.entity;

public enum UserRole { STUDENT, MODERATOR, LECTURER }
