package nl.tudelft.oopp.application.repository;

import java.util.List;
import java.util.Optional;
import nl.tudelft.oopp.application.entity.Lecture;
import nl.tudelft.oopp.application.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findAllByLectureEquals(Lecture lecture);

    Optional<User> findFirstByIpAddressEquals(String ipAddress);
}
