package nl.tudelft.oopp.application.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import lombok.extern.java.Log;
import nl.tudelft.oopp.application.controller.dtos.LectureJoinResponse;
import nl.tudelft.oopp.application.controller.dtos.PollAnswerRequest;
import nl.tudelft.oopp.application.controller.dtos.PollCreateRequest;
import nl.tudelft.oopp.application.entity.Ban;
import nl.tudelft.oopp.application.entity.Lecture;
import nl.tudelft.oopp.application.entity.Poll;
import nl.tudelft.oopp.application.entity.Reaction;
import nl.tudelft.oopp.application.entity.User;
import nl.tudelft.oopp.application.entity.UserRole;
import nl.tudelft.oopp.application.exception.BanNotFoundException;
import nl.tudelft.oopp.application.exception.InvalidArgumentsException;
import nl.tudelft.oopp.application.exception.LectureNotFoundException;
import nl.tudelft.oopp.application.exception.LectureNotStartedYetException;
import nl.tudelft.oopp.application.exception.PollNotClosedException;
import nl.tudelft.oopp.application.exception.PollNotFoundException;
import nl.tudelft.oopp.application.exception.ReactionNotFoundException;
import nl.tudelft.oopp.application.exception.UnauthorizedException;
import nl.tudelft.oopp.application.exception.UserBannedException;
import nl.tudelft.oopp.application.exception.UserNotFoundException;
import nl.tudelft.oopp.application.repository.BanRepository;
import nl.tudelft.oopp.application.repository.LectureRepository;
import nl.tudelft.oopp.application.repository.PollRepository;
import nl.tudelft.oopp.application.repository.ReactionRepository;
import nl.tudelft.oopp.application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@Log
public class LectureService {

    private final LectureRepository lectureRepository;
    private final UserRepository userRepository;
    private final ReactionRepository reactionRepository;
    private final BanRepository banRepository;
    private final PollRepository pollRepository;

    /**
     * Controller.
     * @param lectureRepository repository.
     * @param userRepository repository.
     * @param reactionRepository repository.
     */
    @Autowired
    public LectureService(LectureRepository lectureRepository, UserRepository userRepository,
                          ReactionRepository reactionRepository, BanRepository banRepository,
                          PollRepository pollRepository) {
        this.lectureRepository = lectureRepository;
        this.userRepository = userRepository;
        this.reactionRepository = reactionRepository;
        this.banRepository = banRepository;
        this.pollRepository = pollRepository;
    }

    /**
     * Creates a new lecture with a given name.
     *
     * @param lectureName name of the lecture.
     * @param courseName  name of the course.
     * @param time time of the lecture.
     * @return created lecture object.
     * @throws InvalidArgumentsException thrown when argument is invalid
     */
    public Lecture createLecture(String lectureName, String courseName, Date time)
            throws InvalidArgumentsException {
        // make sure fields arent null
        if (StringUtils.isEmpty(lectureName) || StringUtils.isEmpty(courseName)) {
            throw new InvalidArgumentsException();
        }

        // use default time if no time specified
        if (time == null) {
            time = new Date();
        }

        log.info("Creating lecture: " + lectureName + "(" + courseName + ")...");

        // create lecture object
        Lecture lecture = new Lecture();
        lecture.setName(lectureName);
        lecture.setCourse(courseName);
        lecture.setTime(time);

        // set joincodes
        lecture.setStudentCode(generateRandomJoinCode());
        lecture.setModeratorCode(generateRandomJoinCode());

        // save lecture to db
        return lectureRepository.save(lecture);
    }

    /**
     * Gets a lecture with a given lecture id.
     * @param lectureId id of the lecture.
     * @return lecture info.
     * @throws LectureNotFoundException thrown if the lecture doesn't exist.
     */
    public Lecture getLecture(long lectureId)
            throws LectureNotFoundException {
        // get lecture
        Optional<Lecture> optionalLecture = lectureRepository.findFirstById(lectureId);
        if (optionalLecture.isEmpty()) {
            throw new LectureNotFoundException();
        }
        return optionalLecture.get();
    }

    /**
     * Gets a lecture with a given lecture id.
     * @param joinCode join code of the lecture.
     * @return lecture info.
     * @throws LectureNotFoundException exception
     */
    public Lecture getLectureByJoinCode(String joinCode) throws LectureNotFoundException {
        Optional<Lecture> optionalLecture = lectureRepository
                .findFirstByStudentCodeOrModeratorCode(joinCode, joinCode);
        if (optionalLecture.isEmpty()) {
            throw new LectureNotFoundException();
        }
        Lecture lecture = optionalLecture.get();

        // set answer to 0 for students if poll exists
        if (lecture.getPoll() != null) {
            if (!lecture.getModeratorCode().equals(joinCode)
                    && !lecture.getPoll().isClosed()) {
                lecture.getPoll().setAnswer(0); // student shouldn't get answer back
                lecture.getPoll().setStatistics(null);
            }
        }
        return lecture;
    }

    /**
     * Deletes a lecture with a given lecture id.
     * @param lectureID id of the lecture.
     */
    public void deleteLecture(long lectureID) {
        lectureRepository.deleteById(lectureID);
    }

    /**
     * Join a lecture given a lecture code
     * and a user name.
     *
     * @param code        lecture join code
     * @param userName    name of the joining user
     * @param userAddress IP address of the joining user
     * @return user info with role assigned
     * @throws LectureNotFoundException exception
     * @throws UserBannedException thrown if the user is banned from this lecture.
     * @throws LectureNotStartedYetException thrown if the lecture is not started yet.
     */
    public LectureJoinResponse joinLecture(String code, String userName, String userAddress)
            throws LectureNotFoundException, UserBannedException, LectureNotStartedYetException {
        // find lecture by code
        Optional<Lecture> lectureOptional = lectureRepository
                .findFirstByStudentCodeOrModeratorCode(code, code);
        if (lectureOptional.isEmpty()) {
            throw new LectureNotFoundException();
        }
        Lecture lecture = lectureOptional.get();

        // check if user is banned
        Optional<Ban> optionalBan = banRepository.findByBanIpAndLecture(userAddress, lecture);
        if (optionalBan.isPresent()) {
            log.info("User is banned");
            throw new UserBannedException();
        }

        // get role
        UserRole role = getUserRole(code, lecture);

        // check if the lecture is scheduled
        // or happening right now
        if (role == UserRole.STUDENT) {
            Calendar calendar = Calendar.getInstance();
            if (calendar.getTime().before(lecture.getTime())) {
                // lecture is scheduled
                throw new LectureNotStartedYetException(lecture.getTime());
            }
        }

        // create user
        User user = new User();
        user.setRole(role);
        user.setName(userName);
        user.setIpAddress(userAddress);
        user.setLecture(lecture);
        user = userRepository.save(user);

        return new LectureJoinResponse(lecture, user);
    }

    /**
     * Lists users in a given lecture.
     * @param lectureId lecture to list the users of.
     * @return list of users.
     * @throws LectureNotFoundException thrown if lecture cannot be found.
     */
    public List<User> listUsers(long lectureId)
            throws LectureNotFoundException {
        // get lecture
        Optional<Lecture> lectureOptional = lectureRepository.findById(lectureId);
        if (lectureOptional.isEmpty()) {
            throw new LectureNotFoundException();
        }
        return userRepository.findAllByLectureEquals(lectureOptional.get());
    }

    /**
     * Verifies a join code.
     * @param code the code to verify.
     * @param userIp the ip of the user.
     * @return the role associated with the join code.
     * @throws LectureNotStartedYetException exception
     * @throws LectureNotFoundException exception
     * @throws UserBannedException thrown if the user is banned.
     */
    public UserRole verifyJoinCode(String code, String userIp)
            throws LectureNotStartedYetException, LectureNotFoundException, UserBannedException {
        // get lecture
        Optional<Lecture> optionalLecture = lectureRepository
                .findFirstByStudentCodeOrModeratorCode(code, code);
        if (optionalLecture.isEmpty()) {
            throw new LectureNotFoundException();
        }
        Lecture lecture = optionalLecture.get();

        // check if the user is banned
        Optional<Ban> optionalBan = banRepository.findByBanIpAndLecture(userIp, lecture);
        if (optionalBan.isPresent()) {
            throw new UserBannedException();
        }

        // get user role
        UserRole userRole = getUserRole(code, lecture);

        // check if the lecture is scheduled
        // or happening right now
        if (userRole == UserRole.STUDENT) {
            Calendar calendar = Calendar.getInstance();
            if (calendar.getTime().before(lecture.getTime())) {
                // lecture is scheduled
                throw new LectureNotStartedYetException(lecture.getTime());
            } else {
                // lecture can be joined
                return userRole;
            }
        } else {
            return userRole;
        }
    }


    /**
     * Determines the user role based on
     * the join code used and lecture info.
     * @param joinCode join code used by the user.
     * @param lecture lecture info
     * @return determined user role.
     */
    private UserRole getUserRole(String joinCode, Lecture lecture) {
        // get role
        if (lecture.getModeratorCode().equals(joinCode)) {
            // if no users join as lecturer
            if (lecture.getUsers().size() == 0) {
                // join as lecturer
                return UserRole.LECTURER;
            } else {
                // join as moderator
                return UserRole.MODERATOR;
            }
        } else {
            // join as student
            return UserRole.STUDENT;
        }
    }

    /**
     * Bans a user from joining a lecture.
     * @param lectureId The id of the lecture.
     * @param userId The id of the user.
     * @throws LectureNotFoundException exception
     * @throws UserNotFoundException exception
     */
    public void banUser(long lectureId, long userId)
            throws LectureNotFoundException, UserNotFoundException {
        // get lecture
        Optional<Lecture> lectureOptional = lectureRepository.findById(lectureId);
        if (lectureOptional.isEmpty()) {
            throw new LectureNotFoundException();
        }
        Lecture lecture = lectureOptional.get();

        // get user
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isEmpty()) {
            throw new UserNotFoundException();
        }
        User user = userOptional.get();

        // create ban
        Ban ban = new Ban();
        ban.setBanIp(user.getIpAddress());
        ban.setLecture(lecture);
        ban.setTimestamp(System.currentTimeMillis());
        banRepository.save(ban);
    }

    /**
     * Unbans a user by their ip address.
     * @param lectureId The id of the lecture.
     * @param ipAddress The ip address of the user.
     * @throws BanNotFoundException exception
     * @throws LectureNotFoundException exception
     */
    public void unbanUser(long lectureId, String ipAddress)
            throws BanNotFoundException, LectureNotFoundException {
        // get lecture
        Optional<Lecture> lectureOptional = lectureRepository.findById(lectureId);
        if (lectureOptional.isEmpty()) {
            throw new LectureNotFoundException();
        }
        Lecture lecture = lectureOptional.get();

        // get ban
        Optional<Ban> banOptional = banRepository.findByBanIpAndLecture(ipAddress, lecture);
        if (banOptional.isEmpty()) {
            throw new BanNotFoundException();
        }

        // remove ban
        banRepository.delete(banOptional.get());
    }


    /**
     * Generating random access code,
     * This version will generate access code with lower chance of getting collision.
     * As the access code will larger over time and there are even more
     * probabilities can be generated per second, if not millisecond.
     * @return a random access code.
     */
    public String generateRandomJoinCode() {
        StringBuilder result = new StringBuilder("group63://");
        Random random = new Random();
        long time = new Date().getTime();

        int n = 15; // number of extra characters.
        while (time != 0 || n != 0) {
            if ((random.nextBoolean() && time > 0) || n == 0) {
                result.append(time % 10);
                time /= 10;
            } else {
                result.append((char) (97 + random.nextInt(26)));
                n--;
            }
        }

        return result.toString();
    }


    /**
     * End lecture using lectureId and ModeratorCode.
     *  @param lectureId Id of the lecture to be ended.
     *  @param moderatorCode Used to confirm the permission to end the lecture.
     * @throws LectureNotFoundException throw when lecture is not found.
     * @throws UnauthorizedException throw when user unauthorized to end this lecture.
     */
    public void endLecture(long lectureId, String moderatorCode)
            throws LectureNotFoundException, UnauthorizedException {
        Optional<Lecture> optionalLecture = lectureRepository.findFirstById(lectureId);
        if (optionalLecture.isEmpty()) {
            throw new LectureNotFoundException();
        }
        Lecture lecture = optionalLecture.get();
        if (!lecture.getModeratorCode().equals(moderatorCode)) {
            throw new UnauthorizedException();
        }

        if (lecture.isEnded()) {
            lecture.setEnded(false);
        } else {
            lecture.setEnded(true);
        }
        lectureRepository.save(lecture);
    }


    /**
     * Creates a poll for a lecture.
     * @param lectureID id of lecture it belongs to
     * @param poll the poll to be created
     * @return the updated lecture
     * @throws UnauthorizedException thrown if not moderator creates the poll
     * @throws LectureNotFoundException thrown if lecture does not exist
     */
    public Lecture createPoll(long lectureID, PollCreateRequest poll)
            throws UnauthorizedException, LectureNotFoundException {
        Optional<Lecture> lectureOptional = lectureRepository
                .findFirstByModeratorCode(poll.getModeratorCode());
        if (lectureOptional.isEmpty()) {
            throw new LectureNotFoundException();
        }

        // check if authorized
        if (!lectureOptional.get().getModeratorCode().equals(poll.getModeratorCode())) {
            throw new UnauthorizedException();
        }

        // set poll info
        Poll resultPoll = new Poll();
        resultPoll.setQuiz(poll.getQuiz());
        resultPoll.setOptions(poll.getOptions());
        resultPoll.setAnswer(poll.getAnswer()); // actual answer is saved here
        int n = poll.getOptions().size();
        List<Integer> stats = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            stats.add(0);
        }
        resultPoll.setStatistics(stats);

        pollRepository.save(resultPoll);
        Lecture lecture = lectureOptional.get();
        lecture.setPoll(resultPoll);

        lectureRepository.save(lecture);
        return lecture;
    }

    /**
     * Updates the answers to a poll.
     * @param lectureID id of lecture it belongs to
     * @param answer answer of that should be saved
     * @return the updated lecture
     * @throws LectureNotFoundException exception
     * @throws UserNotFoundException exception
     * @throws UnauthorizedException exception
     */
    public Lecture answerPoll(long lectureID, PollAnswerRequest answer)
            throws LectureNotFoundException, UserNotFoundException, UnauthorizedException {
        Optional<Lecture> lectureOptional = lectureRepository.findFirstById(lectureID);
        if (lectureOptional.isEmpty()) {
            throw new LectureNotFoundException();
        }

        Optional<User> userOptional = userRepository.findById(answer.getUserId());
        if (userOptional.isEmpty()) {
            throw new UserNotFoundException();
        }

        if (!userOptional.get().getLecture().getStudentCode().equals(
                lectureOptional.get().getStudentCode())) {
            throw new UnauthorizedException();
        }

        Lecture lecture = lectureOptional.get();
        Poll poll = lecture.getPoll();

        List<Integer> statistic = poll.getStatistics();
        int pos = Math.abs(answer.getAnswerOption());
        int stats = statistic.get(pos);

        statistic.set(pos, stats + 1);

        poll.setStatistics(statistic);
        pollRepository.save(poll);

        lecture.setPoll(poll);
        lectureRepository.save(lecture);
        return lecture;
    }


    /**
     * Closes the poll for the lecture.
     * @param lectureID id of lecture it belongs to
     * @param code code of user for authorization
     * @return updated lecture
     * @throws LectureNotFoundException exception
     * @throws UnauthorizedException exception
     * @throws PollNotFoundException exception
     */
    public Lecture closePoll(long lectureID, String code) throws LectureNotFoundException,
            UnauthorizedException, PollNotFoundException {
        Optional<Lecture> lectureOptional = lectureRepository.findFirstById(lectureID);
        if (lectureOptional.isEmpty()) {
            throw new LectureNotFoundException();
        }

        // check if authorized
        Lecture lecture = lectureOptional.get();
        if (!lecture.getModeratorCode().equals(code)) {
            throw new UnauthorizedException();
        }

        if (lecture.getPoll() == null) {
            throw new PollNotFoundException();
        }

        Poll poll = lecture.getPoll();
        poll.setClosed(true);
        pollRepository.save(poll);
        lecture.setPoll(poll);

        log.info("Poll close received");

        return lectureRepository.save(lecture);
    }


    /**
     * Removes the poll from a lecture.
     * @param lectureID id of lecture
     * @param code code of moderator
     * @return updated lecture
     * @throws LectureNotFoundException thrown if lecture not found.
     * @throws UnauthorizedException thrown if not authorized.
     * @throws PollNotFoundException thrown if poll not found.
     * @throws PollNotClosedException thrown if poll not closed.
     */
    public Lecture removePoll(long lectureID, String code) throws LectureNotFoundException,
            UnauthorizedException, PollNotFoundException, PollNotClosedException {
        Optional<Lecture> lectureOptional = lectureRepository.findFirstById(lectureID);
        if (lectureOptional.isEmpty()) {
            throw new LectureNotFoundException();
        }

        // check if authorized
        Lecture lecture = lectureOptional.get();
        if (!lecture.getModeratorCode().equals(code)) {
            throw new UnauthorizedException();
        }

        if (lecture.getPoll() == null) {
            throw new PollNotFoundException();
        }

        Poll poll = lecture.getPoll();
        if (!poll.isClosed()) {
            throw new PollNotClosedException();
        }

        lecture.setPoll(null);

        return lectureRepository.save(lecture);
    }

    /**
     * Creates a Speed reaction and stores it in the database.
     * @param lectureId the id of the lecture.
     * @param type boolean of the reaction.
     * @throws LectureNotFoundException thrown when lecture is not found
     */
    public Reaction speedReaction(long lectureId, boolean type)
            throws LectureNotFoundException {
        // get lecture
        Optional<Lecture> lectureOptional = lectureRepository.findById(lectureId);
        if (lectureOptional.isEmpty()) {
            throw new LectureNotFoundException();
        }
        // create reaction
        Reaction reaction = new Reaction();
        reaction.setType(type);
        reaction.setLecture(lectureOptional.get());

        // save reaction
        return reactionRepository.save(reaction);
    }

    /**
     * Lists reactions in a given lecture.
     * @param lectureId lecture to list the reactions of.
     * @return list of reactions
     * @throws LectureNotFoundException thrown if lecture cannot be found.
     */
    public List<Reaction> listReactions(long lectureId)
            throws LectureNotFoundException {
        // get lecture
        Optional<Lecture> lectureOptional = lectureRepository.findById(lectureId);
        if (lectureOptional.isEmpty()) {
            throw new LectureNotFoundException();
        }
        return reactionRepository.findAllByLectureEquals(lectureOptional.get());
    }

    /**
     * Deletes a reaction by id.
     * @throws ReactionNotFoundException when reaction is not found.
     */
    public void deleteReaction(long reactionId)
            throws ReactionNotFoundException {
        // get question
        Optional<Reaction> questionOptional = reactionRepository.findById(reactionId);
        if (questionOptional.isEmpty()) {
            throw new ReactionNotFoundException();
        }
        Reaction reaction = questionOptional.get();

        // delete question
        reactionRepository.delete(reaction);

    }

    /**
     * Lists lectures scheduled in the future.
     * @param filter "future" - lectures scheduled in the future
     *               "past" - lectures scheduled in the past
     *               null - all lectures
     * @return Lectures scheduled in the future.
     */
    public List<Lecture> listLectures(String filter) {
        switch (filter) {
            case "future":
                return lectureRepository.findAllByTimeAfter(Calendar.getInstance().getTime());
            case "past":
                return lectureRepository.findAllByTimeBefore(Calendar.getInstance().getTime());
            default:
                return lectureRepository.findAll();
        }
    }
}
