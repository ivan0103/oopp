package nl.tudelft.oopp.application.exception;

import lombok.Data;

public class LectureNotFoundException extends Exception {
}
