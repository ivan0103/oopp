package nl.tudelft.oopp.application.repository;

import java.util.List;
import java.util.Optional;
import nl.tudelft.oopp.application.entity.Lecture;
import nl.tudelft.oopp.application.entity.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
    List<Question> findAllByLectureEquals(Lecture lecture);

    List<Question> findAllByLectureEqualsAndAnswerNull(Lecture lecture);

    List<Question> findAllByLectureEqualsAndAnswerNotNull(Lecture lecture);

    List<Question> findAllByLectureEqualsAndAnswerNullOrderByUpvotesDesc(Lecture lecture);

    List<Question> findAllByLectureEqualsAndAnswerNotNullOrderByUpvotesDesc(Lecture lecture);

    Optional<Question> findByIdAndLectureEquals(long id, Lecture lecture);

    List<Question> findAllByLectureEqualsAndAnswerNullOrderByDateDesc(Lecture lecture);

    List<Question> findAllByLectureEqualsAndAnswerNotNullOrderByDateDesc(Lecture lecture);
}
