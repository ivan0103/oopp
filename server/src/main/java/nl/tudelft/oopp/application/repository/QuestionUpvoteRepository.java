package nl.tudelft.oopp.application.repository;

import java.util.List;
import nl.tudelft.oopp.application.entity.Question;
import nl.tudelft.oopp.application.entity.QuestionUpvote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionUpvoteRepository extends JpaRepository<QuestionUpvote, Long> {
    List<QuestionUpvote> findAllByQuestion(Question question);

    void deleteAllByQuestion(Question question);
}
