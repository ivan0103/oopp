package nl.tudelft.oopp.application.entity;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;
import lombok.Data;


@Data
@Embeddable
public class Answer {
    @Size(max = 1000)
    private String answer;
    @ManyToOne
    private User answerer;
}


