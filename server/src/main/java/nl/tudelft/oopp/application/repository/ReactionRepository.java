package nl.tudelft.oopp.application.repository;

import java.util.List;
import java.util.Optional;
import nl.tudelft.oopp.application.entity.Lecture;
import nl.tudelft.oopp.application.entity.Question;
import nl.tudelft.oopp.application.entity.Reaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReactionRepository extends JpaRepository<Reaction, Boolean> {
    List<Reaction> findAllByLectureEquals(Lecture lecture);

    Optional<Reaction> findById(long reactionId);
}
