package nl.tudelft.oopp.application.controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import lombok.extern.java.Log;
import nl.tudelft.oopp.application.controller.dtos.AnswerCreateRequest;
import nl.tudelft.oopp.application.controller.dtos.LectureCreateRequest;
import nl.tudelft.oopp.application.controller.dtos.PollAnswerRequest;
import nl.tudelft.oopp.application.controller.dtos.PollCreateRequest;
import nl.tudelft.oopp.application.controller.dtos.QuestionCreateRequest;
import nl.tudelft.oopp.application.controller.dtos.QuestionEditRequest;
import nl.tudelft.oopp.application.controller.dtos.QuestionUpvoteRequest;
import nl.tudelft.oopp.application.entity.Answer;
import nl.tudelft.oopp.application.entity.Lecture;
import nl.tudelft.oopp.application.entity.Question;
import nl.tudelft.oopp.application.entity.Reaction;
import nl.tudelft.oopp.application.entity.User;
import nl.tudelft.oopp.application.exception.BanNotFoundException;
import nl.tudelft.oopp.application.exception.InvalidArgumentsException;
import nl.tudelft.oopp.application.exception.LectureNotFoundException;
import nl.tudelft.oopp.application.exception.PollNotClosedException;
import nl.tudelft.oopp.application.exception.PollNotFoundException;
import nl.tudelft.oopp.application.exception.QuestionNotClaimedException;
import nl.tudelft.oopp.application.exception.QuestionNotFoundException;
import nl.tudelft.oopp.application.exception.QuestionNotOwnedByUserException;
import nl.tudelft.oopp.application.exception.QuestionSelfUpvoteException;
import nl.tudelft.oopp.application.exception.ReactionNotFoundException;
import nl.tudelft.oopp.application.exception.UnauthorizedException;
import nl.tudelft.oopp.application.exception.UserBannedException;
import nl.tudelft.oopp.application.exception.UserNotFoundException;
import nl.tudelft.oopp.application.repository.UserRepository;
import nl.tudelft.oopp.application.service.AnswerService;
import nl.tudelft.oopp.application.service.LectureService;
import nl.tudelft.oopp.application.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Log
@RestController
@RequestMapping("lecture")
public class LectureController {

    private final LectureService lectureService;
    private final QuestionService questionService;
    private final AnswerService answerService;
    private final UserRepository userRepository;

    /**
     * Sets up lecture controller.
     */
    @Autowired
    public LectureController(LectureService lectureService, QuestionService questionService,
                             AnswerService answerService, UserRepository userRepository) {
        this.lectureService = lectureService;
        this.questionService = questionService;
        this.answerService = answerService;
        this.userRepository = userRepository;
    }

    /**
     * Lists lectures.
     * @param filter "future" - lectures scheduled in the future
     *               "past" - lectures scheduled in the past
     *               null - all lectures
     * @return the list of lectures.
     */
    @GetMapping
    public List<Lecture> listLectures(HttpServletRequest httpBody, @RequestParam String filter) {
        log.info("REQUEST TO LIST LECTURE USING FILTER = " + filter
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        return lectureService.listLectures(filter);
    }

    /**
     * Creates a new lecture.
     * @param body lecture information.
     * @return created lecture info.
     */
    @PostMapping
    public ResponseEntity<Lecture> createLecture(HttpServletRequest httpBody,
                                                 @RequestBody LectureCreateRequest body) {
        log.info("REQUEST TO CREATE NEW LECTURE WITH POST INFORMATION = " + body
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            return ResponseEntity.ok(lectureService.createLecture(
                    body.getName(), body.getCourse(), body.getTime()));
        } catch (InvalidArgumentsException e) {
            log.warning("INVALID ARGUMENT");
            return ResponseEntity.badRequest().build();
        }
    }

    /**
     * Gets a lecture by id.
     * @param lectureId id of the lecture.
     * @return created lecture info.
     */
    @GetMapping("{lectureId}")
    public ResponseEntity<Lecture> getLecture(HttpServletRequest httpBody,
                                              @PathVariable long lectureId) {
        log.info("REQUEST TO GET LECTURE WITH PATH VARIABLE LECTURE-ID = " + lectureId
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            return ResponseEntity.ok(lectureService.getLecture(lectureId));
        } catch (LectureNotFoundException e) {
            log.warning("LECTURE NOT FOUND");
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Deletes a lecture with a given id.
     * @param lectureId id of the lecture.
     */
    @DeleteMapping("{lectureId}")
    public void deleteLecture(HttpServletRequest httpBody, @PathVariable long lectureId) {
        log.info("REQUEST TO DELETE LECTURE WITH PATH VARIABLE LECTURE-ID = " + lectureId
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        lectureService.deleteLecture(lectureId);
    }

    /**
     * Lists questions from a lecture.
     * @param lectureId Lecture from which to list questions.
     * @param sort Sorting that should be applied to questions.
     * @param filter Filtering that should be applied to questions.
     * @return List of questions.
     */
    @GetMapping("{lectureId}/questions")
    public ResponseEntity<List<Question>> listQuestions(HttpServletRequest httpBody,
                                                        @PathVariable("lectureId") long lectureId,
                                                        @PathParam("sort") String sort,
                                                        @PathParam("filter") String filter) {
        log.info("REQUEST TO LIST ALL QUESTIONS WITH PATH VARIABLE LECTURE-ID = " + lectureId + ", "
                + "SORT = " + sort + ", FILTER = " + filter + " - "
                + " BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            return ResponseEntity.ok(questionService.listQuestions(
                    lectureId, filter, sort, httpBody.getRemoteAddr()));
        } catch (LectureNotFoundException e) {
            log.warning("LECTURE NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (UserBannedException userBannedException) {
            log.warning("USER BANNED");
            return ResponseEntity.status(403).build();
        }
    }

    /**
     * Lists users from a lecture.
     * @param lectureId Lecture from which to list users.
     * @return List of users.
     */
    @GetMapping("{lectureId}/users")
    public ResponseEntity<List<User>> listUsers(@PathVariable("lectureId") long lectureId) {
        try {
            return ResponseEntity.ok(lectureService.listUsers(lectureId));
        } catch (LectureNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Creates a new question in a lecture.
     * @param lectureId ID of the lecture.
     * @param request Question details.
     */
    @PostMapping("{lectureId}/questions")
    public ResponseEntity<Question> createQuestion(HttpServletRequest httpBody,
            @PathVariable("lectureId") long lectureId,
            @RequestBody QuestionCreateRequest request) {

        log.info("REQUEST TO CREATE QUESTION WITH PATH VARIABLE LECTURE-ID = "
                + lectureId + ", POST INFORMATION = " + request + " - "
                + " BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            return ResponseEntity.ok(questionService.createQuestion(request.getTitle(),
                    request.getContent(), lectureId, request.getUserID(),
                    httpBody.getRemoteAddr()));
        } catch (LectureNotFoundException e) {
            log.warning("LECTURE NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (UserNotFoundException e) {
            log.warning("USER NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (InvalidArgumentsException e) {
            log.warning("INVALID ARGUMENT");
            return ResponseEntity.badRequest().build();
        } catch (UserBannedException userBannedException) {
            log.warning("USER BANNED");
            return ResponseEntity.status(403).build();
        }
    }

    /**
     * Edits a question.
     * @param lectureId The id of the lecture.
     * @param id The id of the question.
     * @param request The details of the edit.
     * @return The new question.
     */
    @PutMapping("{lectureId}/questions/{id}")
    public ResponseEntity<Question> editQuestion(HttpServletRequest httpBody,
                                                 @PathVariable long lectureId,
                                                 @PathVariable long id,
                                                 @RequestBody QuestionEditRequest request) {
        log.info("REQUEST TO EDIT QUESTION WITH PATH VARIABLE LECTURE-ID = "
                + lectureId + ", QUESTION-ID = " + id + ", REQUEST BODY = " + request
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            return ResponseEntity.ok(questionService.editQuestion(
                    request.getContent(), lectureId,
                    request.getUserID(), id, httpBody.getRemoteAddr()));
        } catch (InvalidArgumentsException e) {
            log.warning("INVALID ARGUMENT");
            return ResponseEntity.badRequest().build();
        } catch (LectureNotFoundException e) {
            log.warning("LECTURE NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (QuestionNotFoundException e) {
            log.warning("QUESTION NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (UserBannedException userBannedException) {
            log.warning("USER BANNED");
            return ResponseEntity.status(403).build();
        } catch (QuestionNotOwnedByUserException e) {
            log.warning("USER NOT OWN THIS QUESTION");
            return ResponseEntity.status(403).build();
        } catch (UserNotFoundException e) {
            log.warning("USER NOT FOUND");
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Gets a question based on the question id and lecture id.
     * @param lectureId the id of the lecture.
     * @param id the id of the question.
     * @return Question info.
     */
    @GetMapping("{lectureId}/questions/{id}")
    public ResponseEntity<Question> getQuestion(HttpServletRequest httpBody,
                                                @PathVariable long lectureId,
                                                @PathVariable long id) {
        log.info("REQUEST TO GET QUESTION WITH PATH VARIABLE LECTURE-ID = "
                + lectureId + ", QUESTION-ID = " + id
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));

        try {
            return ResponseEntity.ok(questionService.getQuestion(
                    id, lectureId, httpBody.getRemoteAddr()));
        } catch (QuestionNotFoundException e) {
            log.warning("QUESTION NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (LectureNotFoundException e) {
            log.warning("LECTURE NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (UserBannedException userBannedException) {
            log.warning("USER BANNED");
            return ResponseEntity.status(403).build();
        }
    }

    /**
     * Creates a new answer to a specified question.
     * @param body answer details.
     * @param lectureId lecture id of the lecture where the answer is posted.
     * @param id id of the question to post the answer for.
     * @return return Answer entity
     */
    @PostMapping("{lectureId}/questions/{id}/answer")
    public ResponseEntity<Answer> answerQuestion(HttpServletRequest httpBody,
                                                 @RequestBody AnswerCreateRequest body,
                                 @PathVariable long lectureId, @PathVariable long id) {
        log.info("REQUEST TO ANSWER QUESTION WITH PATH VARIABLE LECTURE-ID = "
                + lectureId + ", QUESTION-ID = " + id
                + ", POST INFORMATION = " + body
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            return ResponseEntity.ok(
                    answerService.createAnswer(body.getContent(), body.getUserID(), id, lectureId));
        } catch (LectureNotFoundException e) {
            log.warning("LECTURE NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (QuestionNotFoundException e) {
            log.warning("QUESTION NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (UserNotFoundException e) {
            log.warning("USER NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (QuestionNotClaimedException e) {
            log.warning("QUESTION NOT CLAIMED");
            return ResponseEntity.badRequest().build();
        } catch (InvalidArgumentsException e) {
            log.warning("INVALID ARGUMENT");
            return ResponseEntity.badRequest().build();
        }
    }

    /**
     * Marks a question as answered.
     * @param lectureId the lecture it belongs to
     * @param id the id of question to be marked
     * @return void
     */
    @PostMapping("{lectureId}/questions/{id}/markAnswered")
    public ResponseEntity<Void> markAsAnswered(HttpServletRequest httpBody,
                                               @PathVariable long lectureId,
                                               @PathVariable long id) {
        log.info("REQUEST TO MARK QUESTION AS ANSWERED WITH PATH VARIABLE LECTURE-ID = "
                + lectureId + ", QUESTION-ID = " + id
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));

        try {
            answerService.markAsAnswered(lectureId, id);
            return ResponseEntity.ok().build();
        } catch (LectureNotFoundException e) {
            log.warning("EXCEPTION");
            return ResponseEntity.notFound().build();
        } catch (QuestionNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Gets upvotes of a specific question.
     * @param lectureId lecture id of the lecture where the answer is posted.
     * @param id id of the question to upvote.
     * @return void entity.
     */
    @GetMapping("{lectureId}/questions/{id}/upvote")
    public ResponseEntity<Set<User>> getQuestionUpvotes(HttpServletRequest httpBody,
            @PathVariable long lectureId, @PathVariable long id) {
        log.info("REQUEST TO GET QUESTION UPVOTES WITH PATH VARIABLE LECTURE-ID = "
                + lectureId + ", QUESTION-ID = " + id
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            return ResponseEntity.ok(questionService.getQuestionUpvotes(id, lectureId));
        } catch (LectureNotFoundException e) {
            log.warning("LECTURE NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (QuestionNotFoundException e) {
            log.warning("QUESTION NOT FOUND");
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Upvotes a specified question.
     * @param lectureId lecture id of the lecture where the answer is posted.
     * @param id id of the question to upvote.
     * @param request information about the upvote request.
     * @return void
     */
    @PostMapping("{lectureId}/questions/{id}/upvote")
    public ResponseEntity<Void> upvoteQuestion(HttpServletRequest httpBody,
            @PathVariable long lectureId, @PathVariable long id,
            @RequestBody QuestionUpvoteRequest request) {
        log.info("REQUEST TO UPVOTE AN QUESTION WITH PATH VARIABLE LECTURE-ID = "
                + lectureId + ", QUESTION-ID = " + id
                + ", POST INFORMATION = " + request
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            questionService.voteOnQuestion(id, lectureId,
                    request.getUserId(), httpBody.getRemoteAddr());
            return ResponseEntity.ok().build();
        } catch (LectureNotFoundException e) {
            log.warning("LECTURE NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (QuestionNotFoundException e) {
            log.warning("QUESTION NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (UserNotFoundException e) {
            log.warning("USER NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (UserBannedException e) {
            log.warning("USER BANNED");
            return ResponseEntity.status(403).build();
        } catch (QuestionSelfUpvoteException userBannedException) {
            log.warning("SELF UPVOTE");
            return ResponseEntity.status(403).build();
        }
    }

    /**
     * Deletes a question.
     * @param lectureId lecture id of the lecture in which the question should be deleted.
     * @param questionID id of the question to delete.
     */
    @DeleteMapping("{lectureId}/questions/{id}")
    public ResponseEntity<Void> deleteQuestion(HttpServletRequest httpBody,
                               @PathVariable long lectureId,
                               @PathVariable("id") long questionID) {
        log.info("REQUEST TO DELETE AN QUESTION WITH PATH VARIABLE LECTURE-ID = "
                + lectureId + ", QUESTION-ID = " + questionID
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            questionService.deleteQuestion(questionID);
            return ResponseEntity.ok().build();
        } catch (QuestionNotFoundException e) {
            log.warning("QUESTION NOT FOUND");
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * End a lecture.
     * @param lecture Lecture to be ended.
     */
    @PostMapping("/end")
    public void endLecture(HttpServletRequest httpBody, @RequestBody Lecture lecture) {
        log.info("REQUEST TO END LECTURE WITH PATH VARIABLE LECTURE-ID = "
                + lecture
                + ", WITH POST INFORMATION = " + lecture
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            lectureService.endLecture(lecture.getId(), lecture.getModeratorCode());
        } catch (LectureNotFoundException e) {
            log.warning("LECTURE NOT FOUND");
        } catch (UnauthorizedException e) {
            log.warning("UNAUTHORIZED ACTION");
        }
    }

    /**
     * Deletes a reaction.
     * @param lectureId lecture id of the lecture in which the reaction should be deleted.
     * @param reactionId id of the reaction that will be deleted.
     */
    @DeleteMapping("{lectureId}/reactions/{reactionId}")
    public ResponseEntity<Void> deleteReaction(HttpServletRequest httpBody,
                               @PathVariable long lectureId,
                               @PathVariable long reactionId) {
        log.info("REQUEST TO DELETE REACTION WITH PATH VARIABLE LECTURE-ID = "
                + lectureId + ", REACTION-ID = " + reactionId
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            lectureService.deleteReaction(reactionId);
            return ResponseEntity.ok().build();
        } catch (ReactionNotFoundException e) {
            log.warning("REACTION NOT FOUND");
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Reacts with a slower for Speed poll.
     * @param lectureId lecture id of the lecture.
     * @param type booolean for slower or faster reaction.
     * @return void entity.
     */
    @PostMapping("{lectureId}/reaction/{type}")
    public ResponseEntity<Reaction> speedReaction(HttpServletRequest httpBody,
            @PathVariable long lectureId, @PathVariable boolean type) {

        log.info("REQUEST TO MAKE SPEED REACTION WITH PATH VARIABLE LECTURE-ID = "
                + lectureId
                + ", REACTION TYPE " + type
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            return ResponseEntity.ok(lectureService.speedReaction(lectureId, type));
        } catch (LectureNotFoundException e) {
            log.warning("LECTURE NOT FOUND");
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Lists Reactions from a lecture.
     * @param lectureId Lecture from which to list reactions.
     * @return List of reactions.
     */
    @GetMapping("{lectureId}/reactions")
    public ResponseEntity<List<Reaction>> listReactions(HttpServletRequest httpBody,
                                                        @PathVariable("lectureId") long lectureId) {
        log.info("REQUEST TO RETRIEVE SPEED REACTION WITH PATH VARIABLE LECTURE-ID = "
                + lectureId
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            return ResponseEntity.ok(lectureService.listReactions(lectureId));
        } catch (LectureNotFoundException e) {
            log.warning("LECTURE NOT FOUND");
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Let a user claim a question.
     * @param id The ID of the question.
     * @param userId The ID of the user.
     * @return void entity.
     */
    @PostMapping("/questions/{id}/claim/{userId}")
    public ResponseEntity<Void> claimQuestion(HttpServletRequest httpBody,
                                              @PathVariable long id,
                                              @PathVariable long userId) {

        log.info("REQUEST TO CLAIM QUESTION WITH PATH VARIABLE QUESTION-ID = "
                + id
                + ", USER-ID = " + userId
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            questionService.claimQuestion(id, userId);
            return ResponseEntity.ok().build();
        } catch (QuestionNotFoundException e) {
            log.warning("QUESTION NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (UserNotFoundException e) {
            log.warning("USER NOT FOUND");
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Bans a user.
     * @param lectureId Lecture in which to ban the user.
     * @param user User to be banned.
     * @return Whether the banning was succesful.
     */
    @PostMapping("{lectureId}/ban")
    public ResponseEntity<Void> banUser(HttpServletRequest httpBody,
                                        @PathVariable long lectureId,
                                        @RequestParam long user) {
        log.info("REQUEST TO BAN USER WITH PATH VARIABLE QUESTION-ID = "
                + lectureId
                + ", USER-ID = " + user
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            lectureService.banUser(lectureId, user);
            return ResponseEntity.ok().build();
        } catch (LectureNotFoundException e) {
            log.warning("LECTURE NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (UserNotFoundException e) {
            log.warning("USER NOT FOUND");
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Unbans a user.
     * @param lectureId Lecture in which to unban the user.
     * @param ip IP address of the user to be unbanned.
     * @return Whether the unban was successful.
     */
    @PostMapping("{lectureId}/unban")
    public ResponseEntity<Void> unbanUser(HttpServletRequest httpBody,
                                          @PathVariable long lectureId,
                                          @RequestParam String ip) {
        log.info("REQUEST TO UNBAN USER WITH PATH VARIABLE QUESTION-ID = "
                + lectureId
                + ", USER-IP-ADDRESS = " + ip
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            lectureService.unbanUser(lectureId, ip);
            return ResponseEntity.ok().build();
        } catch (LectureNotFoundException e) {
            log.warning("LECTURE NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (BanNotFoundException e) {
            log.warning("BAN NOT FOUND");
            return ResponseEntity.notFound().build();
        }
    }



    /**
     * Create the poll by the lecturer/moderator.
     * @param lectureId id of lecture
     * @param poll poll to be created
     * @return resulting lecture
     */
    @PostMapping("{lectureId}/poll")
    public ResponseEntity<Lecture> createPoll(HttpServletRequest httpBody,
                                              @PathVariable("lectureId") long lectureId,
                                              @RequestBody PollCreateRequest poll) {
        log.info("REQUEST TO CREATE POLL QUESTION WITH PATH VARIABLE QUESTION-ID = "
                + lectureId
                + ", POST INFORMATION = " + poll
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            return ResponseEntity.ok(lectureService.createPoll(lectureId, poll));
        } catch (LectureNotFoundException e) {
            log.warning("LECTURE NOT FOUND");
            return ResponseEntity.notFound().build();
        } catch (UnauthorizedException e) {
            log.warning("UNAUTHORIZED ACTION");
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Respond to the poll by student.
     * @param lectureId id of lecture
     * @param answer the answer chosen
     * @return resulting lecture
     */
    @PostMapping("{lectureId}/poll/answer")
    public ResponseEntity<Lecture> answerPoll(HttpServletRequest httpBody,
            @PathVariable long lectureId, @RequestBody PollAnswerRequest answer) {

        log.info("REQUEST TO ANSWER POLL QUESTION WITH PATH VARIABLE QUESTION-ID = "
                + lectureId
                + ", POST INFORMATION = " + answer
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            lectureService.answerPoll(lectureId, answer);
            return ResponseEntity.ok().build();
        } catch (LectureNotFoundException e) {
            log.warning("EXCEPTION");
            return ResponseEntity.notFound().build();
        } catch (UserNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (UnauthorizedException e) {
            return ResponseEntity.status(403).build();
        }
    }

    /**
     * Closes the poll, students can no longer answer.
     * @param lectureId id of lecture
     * @param code code of the moderator
     * @return resulting lecture
     */
    @PostMapping("{lectureId}/closePoll")
    public ResponseEntity<Void> closePoll(HttpServletRequest httpBody,
            @PathVariable long lectureId, @RequestParam String code) {
        log.info("REQUEST TO CLOSE POLL QUESTION WITH PATH VARIABLE QUESTION-ID = "
                + lectureId
                + ", MODERATOR-ACCESS-CODE = " + code
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            lectureService.closePoll(lectureId, code);
            return ResponseEntity.ok().build();
        } catch (LectureNotFoundException e) {
            log.warning("EXCEPTION");
            return ResponseEntity.notFound().build();
        } catch (UnauthorizedException e) {
            return ResponseEntity.status(403).build();
        } catch (PollNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Closes the poll, students can no longer answer.
     * @param lectureId id of lecture
     * @param code code of the moderator
     * @return resulting lecture
     */
    @PostMapping("{lectureId}/removePoll")
    public ResponseEntity<Void> removePoll(HttpServletRequest httpBody,
            @PathVariable long lectureId, @RequestParam String code) {
        log.info("REQUEST TO REMOVE POLL QUESTION WITH PATH VARIABLE QUESTION-ID = "
                + lectureId
                + ", MODERATOR-ACCESS-CODE = " + code
                + " - BY USER = " + makeUserInfomation(httpBody.getRemoteAddr()));
        try {
            lectureService.removePoll(lectureId, code);
            return ResponseEntity.ok().build();
        } catch (LectureNotFoundException e) {
            log.warning("EXCEPTION");
            return ResponseEntity.notFound().build();
        } catch (UnauthorizedException e) {
            return ResponseEntity.status(403).build();
        } catch (PollNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (PollNotClosedException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    private String makeUserInfomation(String ipAddress) {
        Optional<User> userOptional = userRepository.findFirstByIpAddressEquals(ipAddress);

        StringBuilder name = new StringBuilder("UNKNOWN-NAME");
        if (!userOptional.isEmpty()) {
            name = new StringBuilder(userOptional.get().getName());
        }
        return " NAME = " + name.toString() + ", WITH IP-ADRESS = " + ipAddress;
    }


}
