package nl.tudelft.oopp.application.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.Data;

@Data
@Entity
public class Reaction {
    @Id
    @GeneratedValue
    private long id;
    /**
     * True - slower reaction; False - faster reaction.
     */
    private boolean type;

    @ManyToOne
    @JsonIgnore
    private Lecture lecture;
}
