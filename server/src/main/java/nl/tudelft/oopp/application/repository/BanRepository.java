package nl.tudelft.oopp.application.repository;

import java.util.List;
import java.util.Optional;
import nl.tudelft.oopp.application.entity.Ban;
import nl.tudelft.oopp.application.entity.Lecture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BanRepository extends JpaRepository<Ban, Long> {
    List<Ban> findAllByBanIp(String banIp);

    Optional<Ban> findByBanIpAndLecture(String banIp, Lecture lecture);

    List<Ban> findAllByLectureEquals(Lecture lecture);
}
