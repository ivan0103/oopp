package nl.tudelft.oopp.application.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.util.List;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import lombok.Data;

@Data
@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Poll {

    @Id
    @GeneratedValue
    private long id;

    private String quiz;

    /**
     * options for the poll question.
     */
    @ElementCollection(targetClass = String.class)
    private List<String> options;
    /**
     * for bit masking purposes.
     */
    private int answer;
    /**
     * for registering the responses from students.
     */
    @ElementCollection(targetClass = Integer.class)
    private List<Integer> statistics;
    /**
     * for poll ended.
     */
    private boolean closed;
}
