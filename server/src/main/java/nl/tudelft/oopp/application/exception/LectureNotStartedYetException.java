package nl.tudelft.oopp.application.exception;

import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class LectureNotStartedYetException extends Exception {
    private final Date scheduledDate;
}
