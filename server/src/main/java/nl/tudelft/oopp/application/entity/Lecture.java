package nl.tudelft.oopp.application.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.util.Date;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import lombok.Data;

@Data
@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Lecture {
    @Id
    @GeneratedValue
    private long id;
    /**
     * Name of the lecture.
     */
    private String name;
    /**
     * Name of the course.
     */
    private String course;
    /**
     * Code for students.
     */
    private String studentCode;
    /**
     * Code for moderators.
     */
    private String moderatorCode;
    /**
     * Time at which the lecture is scheduled.
     */
    private Date time;

    /**
     * Users in this lecture.
     */
    @OneToMany
    @JsonIgnore
    private Set<User> users;
    /**
     * Questions in this lecture.
     */
    @OneToMany
    @JsonIgnore
    private Set<Question> questions;
    /**
     * End signal for this lecture.
     */
    private boolean ended;

    @OneToOne(targetEntity = Poll.class)
    private Poll poll;
}
