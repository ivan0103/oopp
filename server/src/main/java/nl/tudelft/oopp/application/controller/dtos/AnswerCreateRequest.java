package nl.tudelft.oopp.application.controller.dtos;

import lombok.Data;

@Data
public class AnswerCreateRequest {
    private long userID;
    private String content;
}
