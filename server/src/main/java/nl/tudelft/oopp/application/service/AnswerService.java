package nl.tudelft.oopp.application.service;

import java.util.Optional;
import lombok.extern.java.Log;
import nl.tudelft.oopp.application.entity.Answer;
import nl.tudelft.oopp.application.entity.Lecture;
import nl.tudelft.oopp.application.entity.Question;
import nl.tudelft.oopp.application.entity.User;
import nl.tudelft.oopp.application.exception.InvalidArgumentsException;
import nl.tudelft.oopp.application.exception.LectureNotFoundException;
import nl.tudelft.oopp.application.exception.QuestionNotClaimedException;
import nl.tudelft.oopp.application.exception.QuestionNotFoundException;
import nl.tudelft.oopp.application.exception.UserNotFoundException;
import nl.tudelft.oopp.application.repository.LectureRepository;
import nl.tudelft.oopp.application.repository.QuestionRepository;
import nl.tudelft.oopp.application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@Log
public class AnswerService {

    private final QuestionRepository questionRepository;
    private final LectureRepository lectureRepository;
    private final UserRepository userRepository;

    /**
     * Sets up answer service.
     */
    @Autowired
    public AnswerService(QuestionRepository questionRepository,
                         LectureRepository lectureRepository, UserRepository userRepository) {
        this.questionRepository = questionRepository;
        this.lectureRepository = lectureRepository;
        this.userRepository = userRepository;
    }


    /**
     * Saves an answer with the question it belongs to.
     * @param content The answer sent from the client
     * @param questionID The id of question it belongs to
     * @param lectureID The lecture of the question.
     * @param userID User id of the user posting the answer.
     * @return The answer saved in the repository
     * @throws LectureNotFoundException if lecture does not exist
     * @throws QuestionNotFoundException if question does not exist
     * @throws UserNotFoundException if claiming user can be found
     * @throws QuestionNotClaimedException if question is not claimed
     * @throws InvalidArgumentsException if arguments are not valid
     */
    public Answer createAnswer(String content, long userID, long questionID, long lectureID)
            throws LectureNotFoundException, QuestionNotFoundException,
            UserNotFoundException, QuestionNotClaimedException, InvalidArgumentsException {
        // check for null
        if (StringUtils.isEmpty(content)) {
            throw new InvalidArgumentsException();
        }

        // get user
        Optional<User> userOptional = userRepository.findById(userID);
        if (userOptional.isEmpty()) {
            throw new UserNotFoundException();
        }

        // get lecture
        Optional<Lecture> lectureOptional = lectureRepository.findById(lectureID);
        if (lectureOptional.isEmpty()) {
            throw new LectureNotFoundException();
        }

        // get question
        Optional<Question> questionOptional = questionRepository
                .findById(questionID);
        if (questionOptional.isEmpty()) {
            throw new QuestionNotFoundException();
        }
        Question question = questionOptional.get();

        if (question.getClaimedBy() == null
                || question.getClaimedBy().getId() != userID) {
            if (question.getClaimedBy() == null) {
                System.out.printf("Null <>");
            }
            throw new QuestionNotClaimedException();
        }

        // create answer
        Answer answer = new Answer();
        answer.setAnswer(content);
        answer.setAnswerer(userOptional.get());

        // set answer to question
        question.setAnswer(answer);
        questionRepository.save(question);

        return answer;
    }


    /**
     * Marks a question as answered.
     * @param lectureId the id of Lecture it belongs to
     * @param questionId the id of Question that should be marked
     * @throws LectureNotFoundException thrown when lecture is not found
     * @throws QuestionNotFoundException thrown when question is not found
     */
    public Question markAsAnswered(long lectureId, long questionId)
            throws LectureNotFoundException, QuestionNotFoundException {
        Optional<Lecture> lecture = lectureRepository.findById(lectureId);
        if (lecture.isEmpty()) {
            throw new LectureNotFoundException();
        }
        Optional<Question> question = questionRepository.findById(questionId);
        if (question.isEmpty()) {
            throw new QuestionNotFoundException();
        }

        // mark as answered + save question again
        Question questionResult = question.get();
        questionResult.setAnswer(new Answer());
        return questionRepository.save(questionResult);
    }
}
