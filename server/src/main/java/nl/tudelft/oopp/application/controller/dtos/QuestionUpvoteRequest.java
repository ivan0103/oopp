package nl.tudelft.oopp.application.controller.dtos;

import lombok.Data;

@Data
public class QuestionUpvoteRequest {
    private long userId;
}
