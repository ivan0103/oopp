package nl.tudelft.oopp.application.controller.dtos;

import lombok.Data;

@Data
public class PollAnswerRequest {
    private int answerOption;
    private long userId;

    public PollAnswerRequest(int answerOption, long userId) {
        this.answerOption = answerOption;
        this.userId = userId;
    }
}