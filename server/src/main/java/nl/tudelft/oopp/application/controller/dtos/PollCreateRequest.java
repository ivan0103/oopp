package nl.tudelft.oopp.application.controller.dtos;

import java.util.List;
import lombok.Data;


@Data
public class PollCreateRequest {
    private String quiz;
    private List<String> options;
    private int answer;
    private String moderatorCode;

    /**
     * Constructor for the poll create request.
     * @param quiz title of poll question
     * @param options the options of the poll
     * @param answer the answer of the poll
     * @param moderatorCode the moderator code
     */
    public PollCreateRequest(String quiz, List<String> options, int answer, String moderatorCode) {
        this.quiz = quiz;
        this.options = options;
        this.answer = answer;
        this.moderatorCode = moderatorCode;
    }
}