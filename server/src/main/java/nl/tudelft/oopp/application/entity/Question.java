package nl.tudelft.oopp.application.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Question {
    @Id
    @GeneratedValue
    private long id;
    private String title;

    @Size(max = 1000)
    private String content;

    /**
     * Emulates the boolean answered field.
     * @return Whether the question was answered.
     */
    @JsonInclude
    public boolean answered() {
        return answer != null;
    }

    /**
     * User that claimed this question.
     */
    @ManyToOne
    private User claimedBy;

    /**
     * Set of users that upvoted this question.
     */
    private int upvotes;
    /**
     * User that created the question.
     */
    @ManyToOne
    private User author;
    /**
     * User that created the question.
     */
    @JsonIgnore
    @ManyToOne
    private Lecture lecture;
    /**
     * Answer to the question if it exists.
     */
    @Embedded
    private Answer answer;

    /**
     * Date at when this question is posted.
     */
    private long date;
}
