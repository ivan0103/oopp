package nl.tudelft.oopp.application.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.Data;


@Data
@Entity
public class User {
    @Id
    @GeneratedValue
    private long id;

    /**
     * Name of the user.
     */
    private String name;
    /**
     * Role of the user in the lecture.
     */
    private UserRole role;
    /**
     * Address of the user.
     */
    private String ipAddress;
    @JsonIgnore
    @OneToMany(
            mappedBy = "author",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<Question> questions;
    @JsonIgnore
    @ManyToOne
    private Lecture lecture;
}