package nl.tudelft.oopp.application.controller.questiontile;

import javafx.scene.control.ListCell;
import nl.tudelft.oopp.application.controller.LecturerPageController;
import nl.tudelft.oopp.application.data.Question;

public class QuestionListViewCell extends ListCell<Question> {

    QuestionTileController tileController;

    /**
     * Loads a new list view cell.
     */
    public QuestionListViewCell() {
        QuestionTileController.display().thenAccept((tile) -> {
            this.tileController = tile;
        });
    }

    @Override
    public void updateItem(Question question, boolean empty) {
        super.updateItem(question, empty);
        if (tileController == null || empty || question == null) {
            setGraphic(null);
        } else {
            tileController.setInfo(1, question);
            setGraphic(tileController.getBox());
        }
    }
}
