package nl.tudelft.oopp.application.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import nl.tudelft.oopp.application.communication.ServerCommunication;
import nl.tudelft.oopp.application.communication.dtos.PollCreateRequest;
import nl.tudelft.oopp.application.data.Lecture;

public class PollCreateController {

    @FXML
    private BorderPane createPollPane;
    @FXML
    private GridPane optionsGridPane;
    @FXML
    private Button addOption;
    @FXML
    private Button publishButton;
    @FXML
    private TextArea pollQuestion;

    public static Lecture lecture;
    private static int numOptions;

    public static List<Integer> answerOptions;

    /**
     * Displays this page for creating the poll.
     */
    @SneakyThrows
    public static CompletableFuture<Void> display() {
        // load fxml
        FXMLLoader fxmlLoader = new FXMLLoader(
                PollCreateController.class.getResource("/createPoll.fxml"));
        Parent root = fxmlLoader.load();

        // create new window
        CompletableFuture<Void> future = new CompletableFuture();
        Platform.runLater(() -> {
            Stage stage = new Stage();
            stage.setTitle("Poll Create");
            stage.setScene(new Scene(root));
            stage.show();
            future.complete(null);
        });

        return future;
    }

    /**
     * Sets the information for this poll.
     * @param currentLecture the lecture the poll belongs to.
     */
    public static void setInfo(Lecture currentLecture) {
        lecture = currentLecture;
        numOptions = 0;
        answerOptions = new ArrayList<>();
    }

    /**
     * Adds another option to the poll question.
     */
    public void addOption() {
        optionsGridPane.setHgap(2.0);
        TextArea option = new TextArea();
        option.setMaxHeight(8.0);
        Button markAnswer = new Button("   Mark as answer   ");
        markAnswer.setId("" + answerOptions.size());
        markAnswer.setStyle("-fx-background-color:#008000");

        Button deleteOption = new Button("X");
        deleteOption.setStyle("-fx-background-color:#921e38");
        deleteOption.setId("" + answerOptions.size());

        answerOptions.add(0);
        System.out.println("" + numOptions);

        if (numOptions >= 9) {
            addOption.setDisable(true);
        }
        
        markAnswer.setOnAction(e -> {
            int pos = Integer.parseInt(markAnswer.getId());
            if (answerOptions.get(pos) == 1) {
                answerOptions.set(pos, 0);
                markAnswer.setStyle("-fx-background-color:#008000");
                markAnswer.setText("   Mark as answer   ");
            } else {
                answerOptions.set(pos, 1);
                markAnswer.setStyle("-fx-background-color:#FF8C00");
                markAnswer.setText("Unmark as answer");

            }
        }); // magic


        deleteOption.setOnAction(e -> {
            numOptions--;
            if (numOptions <= 9) {
                addOption.setDisable(false);
            }
            int row = Integer.parseInt(deleteOption.getId());
            System.out.println("Checking " + row);
            answerOptions.set(row, -1);
            optionsGridPane.getChildren().removeIf(node -> GridPane.getRowIndex(node) == row);
        });
        optionsGridPane.add(option, 0, numOptions);
        optionsGridPane.add(markAnswer, 1, numOptions);
        optionsGridPane.add(deleteOption, 2, numOptions);
        numOptions++;
    }

    /**
     * Turn the pos(th) bit of the given number on.
     * @param num the number to be turned on the bit.
     * @param pos the position of the bit to be turned on.
     * @return a new number with pos(th) bit be turned on.
     */
    private int turnOnBitAt(int num, int pos) {
        return num | (1 << pos);
    }


    /**
     * Publishes the poll.
     */
    public CompletableFuture<Void> publishPoll() {
        // add quiz question
        String quiz = pollQuestion.getText();

        // add options
        ObservableList<Node> children = optionsGridPane.getChildren();
        List<String> options = new ArrayList<>();
        for (Node node : children) {
            if (optionsGridPane.getColumnIndex(node) == 0) {
                TextArea option = (TextArea) node;
                options.add(option.getText());
            }
        }
        // send to server
        return ServerCommunication.createPoll(lecture.getId(),
            new PollCreateRequest(quiz, options, answer(), lecture.getModeratorCode()))
                .thenRun(() -> Platform
                    .runLater(() -> publishButton.getScene().getWindow().hide()));
    }

    /**
     * Marks the answer of a poll.
     * @return an integer as bitmask answer.
     */
    public int answer() {
        int res = 0;
        int cnt = 0;
        for (int i = 0; i < answerOptions.size(); i++) {
            if (answerOptions.get(i) != -1) {
                if (answerOptions.get(i) == 1) {
                    res = turnOnBitAt(res, cnt);
                }
                cnt++;
            }
        }
        return res;
    }
}
