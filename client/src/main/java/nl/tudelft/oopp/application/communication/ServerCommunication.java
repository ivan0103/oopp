package nl.tudelft.oopp.application.communication;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import nl.tudelft.oopp.application.communication.dtos.AnswerCreateRequest;
import nl.tudelft.oopp.application.communication.dtos.CheckCodeResponse;
import nl.tudelft.oopp.application.communication.dtos.LectureCreateRequest;
import nl.tudelft.oopp.application.communication.dtos.LectureJoinResponse;
import nl.tudelft.oopp.application.communication.dtos.PollAnswerRequest;
import nl.tudelft.oopp.application.communication.dtos.PollCreateRequest;
import nl.tudelft.oopp.application.communication.dtos.QuestionCreateRequest;
import nl.tudelft.oopp.application.communication.dtos.QuestionEditRequest;
import nl.tudelft.oopp.application.communication.dtos.QuestionUpvoteRequest;
import nl.tudelft.oopp.application.data.Answer;
import nl.tudelft.oopp.application.data.FilterOption;
import nl.tudelft.oopp.application.data.Lecture;
import nl.tudelft.oopp.application.data.Question;
import nl.tudelft.oopp.application.data.Reaction;
import nl.tudelft.oopp.application.data.SortOption;
import nl.tudelft.oopp.application.data.User;
import nl.tudelft.oopp.application.exception.ServerErrorException;
import nl.tudelft.oopp.application.exception.UserBannedException;

/**
 * For communication with the server
 * Documentation:
 * https://www.javadoc.io/doc/com.google.code.gson/gson/latest/com.google.gson/com/google/gson/Gson.html
 */
public class ServerCommunication {

    private static final String SERVER_URL = "http://localhost:8080";
    private static final HttpClient client = HttpClient.newBuilder().build();
    private static final Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();

    /**
     * Calls a GET on a specified endpoint on the backend.
     * Parses the result as an object of the template class.
     * @param endpoint the endpoint to connect to
     * @param template the class of object
     * @return response object is returned
     */
    private static <T> CompletableFuture<T> get(String endpoint, Type template)
            throws ServerErrorException {
        return CompletableFuture.supplyAsync(() -> {
            // create request
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(SERVER_URL  + endpoint))
                    .header("Content-Type", "application/json")
                    .GET()
                    .build();
            System.out.println("GET to: " + endpoint);

            // call server
            HttpResponse<String> response;
            try {
                response = client.send(request, HttpResponse.BodyHandlers.ofString());
            } catch (IOException | InterruptedException exception) {
                throw new CompletionException(exception);
            }
            if (response.statusCode() != 200) {
                System.out.println("GET ERROR - STATUS CODE " + response.statusCode());
                throw new ServerErrorException(response.statusCode());
            }

            // parse response
            return gson.fromJson(response.body(), template);
        });
    }

    /**
     * Calls a POST on a specified endpoint on the backend.
     * Parses the result as an object of the template class.
     * @param endpoint Endpoint to connect to.
     * @param template Class of the object.
     * @param body Object passed as parameter.
     * @return Object of the response.
     */
    private static <T> CompletableFuture<T> post(String endpoint, Object body, Type template)
            throws ServerErrorException {
        return CompletableFuture.supplyAsync(() -> {
            // create request
            String bodyRequest = gson.toJson(body);
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(SERVER_URL  + endpoint))
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(bodyRequest))
                    .build();
            System.out.println("POST to: " + endpoint);
            System.out.println(bodyRequest);

            // call server
            HttpResponse<String> response;
            try {
                response = client.send(request, HttpResponse.BodyHandlers.ofString());
            } catch (IOException | InterruptedException exception) {
                throw new CompletionException(exception);
            }
            if (response.statusCode() != 200) {
                throw new ServerErrorException(response.statusCode());
            }

            // parse response
            return gson.fromJson(response.body(), template);
        });
    }

    /**
     * Calls a PUT on a specified endpoint on the backend.
     * Parses the result as an object of the template class.
     * @param endpoint End point of PUT request.
     * @param template Class of the object.
     * @param body Object passed as parameter.
     * @return Object of response.
     */
    private static <T> CompletableFuture<T> put(String endpoint, Object body, Type template)
            throws ServerErrorException {
        return CompletableFuture.supplyAsync(() -> {
            // create request
            String bodyRequest = gson.toJson(body);
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(SERVER_URL  + endpoint))
                    .header("Content-Type", "application/json")
                    .PUT(HttpRequest.BodyPublishers.ofString(bodyRequest))
                    .build();
            System.out.println("PUT to: " + endpoint);
            System.out.println(bodyRequest);

            // call server
            HttpResponse<String> response;
            try {
                response = client.send(request, HttpResponse.BodyHandlers.ofString());
            } catch (IOException | InterruptedException exception) {
                throw new CompletionException(exception);
            }
            if (response.statusCode() != 200) {
                throw new ServerErrorException(response.statusCode());
            }

            // parse response
            return gson.fromJson(response.body(), template);
        });
    }

    /**
     * Calls a DELETE on a specified endpoint on the backend.
     * Parses the result as an object of the template class.
     * @param endpoint Endpoint of request
     * @param template Class of the object
     * @return a completable future.
     */
    private static <T> CompletableFuture<T> delete(String endpoint, Type template)
            throws ServerErrorException {
        return CompletableFuture.supplyAsync(() -> {
            // create request
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(SERVER_URL  + endpoint))
                    .header("Content-Type", "application/json")
                    .DELETE()
                    .build();
            System.out.println("DELETE to: " + endpoint);

            // call server
            HttpResponse<String> response;
            try {
                response = client.send(request, HttpResponse.BodyHandlers.ofString());
            } catch (IOException | InterruptedException exception) {
                throw new CompletionException(exception);
            }
            if (response.statusCode() != 200) {
                throw new ServerErrorException(response.statusCode());
            }

            // parse response
            return gson.fromJson(response.body(), template);
        });
    }

    /**
     * Makes a HTTP POST request to the server to create a lecture room.
     *
     * @param request all the information needed to create a lecture room
     * @return Lecture object representing the lecture room
     * @throws InterruptedException exception
     * @throws IOException exception
     * @throws ServerErrorException exception
     */
    public static CompletableFuture<Lecture> createLecture(LectureCreateRequest request)
            throws ServerErrorException {
        return post("/lecture", request, Lecture.class);
    }

    /**
     * Makes a HTTP POST request to the server to join a lecture room.
     *
     * @param user The alias entered in the name field.
     * @return A User object created by the server.
     * @throws InterruptedException exception
     * @throws IOException exception
     * @throws ServerErrorException exception
     * @throws UserBannedException thrown if the user is banned.
     */
    public static CompletableFuture<LectureJoinResponse> joinLecture(User user, String code)
            throws UserBannedException, ServerErrorException {
        return ServerCommunication.<LectureJoinResponse>post(
                "/join?code=" + code, user, LectureJoinResponse.class)
                .exceptionally((e) -> {
                    if (e instanceof ServerErrorException
                            && ((ServerErrorException) e).getStatus() == 403) {
                        throw new UserBannedException();
                    } else {
                        throw new CompletionException(e);
                    }
                });
    }

    /**
     * Makes a HTTP GET request to get a list of lectures.
     * @param filter "future" - Lectures scheduled in the future.
     *               "past" - Lectures scheduled in the past.
     *               null - All lectures.
     * @return The list of lectures.
     * @throws ServerErrorException exception
     */
    public static CompletableFuture<List<Lecture>> listLectures(String filter)
            throws ServerErrorException {
        return get("/lecture?filter=" + filter,
                new TypeToken<List<Lecture>>(){}.getType());
    }

    /**
     * Makes a HTTP GET request to get lecture info.
     * @param lectureId id of the lecture.
     * @return lecture info.
     * @throws ServerErrorException exception
     */
    public static CompletableFuture<Lecture> getLectureById(long lectureId)
            throws ServerErrorException {
        return get("/lecture/" + lectureId, Lecture.class);
    }

    /**
     * Makes a HTTP GET request to get lecture info.
     * @param joinCode join code of the lecture.
     * @return lecture info.
     * @throws ServerErrorException exception
     */
    public static CompletableFuture<Lecture> getLectureByJoinCode(String joinCode)
            throws ServerErrorException {
        return get("/join/info?code=" + joinCode, Lecture.class);
    }

    /**
     * Makes a HTTP POST request to the server to create a question.
     * @param question Question to create.
     * @return Created question.
     * @throws ServerErrorException exception
     */
    public static CompletableFuture<Question> createQuestion(
            QuestionCreateRequest question, long lectureID) throws ServerErrorException {
        return post("/lecture/" + lectureID + "/questions", question, Question.class);
    }

    /**
     * Makes a HTTP PUT request to the server to edit a question.
     * @param questionID The question id.
     * @param lectureID The lecture id.
     * @throws ServerErrorException exception
     */
    public static CompletableFuture<Question> editQuestion(QuestionEditRequest questionEditRequest,
                                                         long lectureID, long questionID)
            throws ServerErrorException {
        return put("/lecture/" + lectureID + "/questions/" + questionID,
                questionEditRequest, Question.class);
    }

    /**
     * Makes a HTTP DELETE request to the server to delete a question.
     *
     * @param questionID The question that should be deleted.
     * @throws ServerErrorException exception
     */
    public static CompletableFuture<Void> deleteQuestion(long lectureID, long questionID)
            throws ServerErrorException {
        return delete("/lecture/" + lectureID + "/questions/" + questionID, Question.class);
    }

    /**
     * Makes a HTTP DELETE request to the server to delete a reaction.
     * deletes all reactions every 5 minutes
     * @throws ServerErrorException exception
     */
    public static CompletableFuture<Void> deleteReaction(long lectureID, long reactionId)
            throws ServerErrorException {
        return delete("/lecture/" + lectureID + "/reactions/" + reactionId, Reaction.class);
    }

    /**
     * Lists questions of a given lecture.
     * @param lectureID id of the lecture for which to list questions.
     * @param filterOption filter by: answered or unanswered
     * @param sortOption sort by: upvotes, relevancy or time
     * @return list of questions.
     * @throws ServerErrorException exception
     * @throws UserBannedException thrown if the user is banned.
     */
    public static CompletableFuture<List<Question>> listQuestions(
            long lectureID, FilterOption filterOption, SortOption sortOption)
            throws ServerErrorException, UserBannedException {
        return ServerCommunication.<List<Question>>get(
                "/lecture/" + lectureID + "/questions?filter="
                        + filterOption.name() + "&sort=" + sortOption.name(),
                new TypeToken<List<Question>>(){}.getType())
                .exceptionally((e) -> {
                    if (e instanceof ServerErrorException
                            && ((ServerErrorException) e).getStatus() == 403) {
                        throw new UserBannedException();
                    } else {
                        throw new CompletionException(e);
                    }
                });
    }

    /**
     * Lists users of a given lecture.
     * @param lectureID id of the lecture for which to list users.
     * @return list of users.
     * @throws ServerErrorException exception
     */
    public static CompletableFuture<List<User>> listUsers(long lectureID)
            throws ServerErrorException {
        return get("/lecture/" + lectureID + "/users",
                            new TypeToken<List<User>>(){}.getType());
    }

    /**
     * Upvotes a question.
     * @param lectureID id of the lecture for which to upvote question.
     * @param questionID id of the question to upvote.
     * @throws ServerErrorException exception
     */
    public static CompletableFuture<Void> upvoteQuestion(
            long questionID, long lectureID, long userID) throws ServerErrorException {
        return post("/lecture/" + lectureID + "/questions/"
                + questionID + "/upvote", new QuestionUpvoteRequest(userID), Void.TYPE);
    }

    /**
     * Gets upvotes of a question.
     * @param questionID Id of the question.
     * @param lectureID Id of the lecture.
     * @return List of users that upvoted the question.
     * @throws ServerErrorException exception
     */
    public static CompletableFuture<List<User>> getQuestionUpvotes(long questionID, long lectureID)
            throws ServerErrorException {
        return get("/lecture/" + lectureID + "/questions/"
                + questionID + "/upvote", new TypeToken<List<User>>(){}.getType());
    }

    /**
     * Makes a HTTP GET request to the server to
     * check the join code.
     *
     * @param code The code that needs to be checked.
     * @return The result of the check.
     * @throws ServerErrorException exception
     */
    public static CompletableFuture<CheckCodeResponse> checkJoinCode(String code)
            throws ServerErrorException {
        return get("/checkcode?code=" + code, CheckCodeResponse.class);
    }

    /**
     * Makes a HTTP POST request to the server to
     * answer a question.
     * @param answerCreateRequest The answer object
     * @param lectureId the lectureId the answer is in
     * @param questionId the questionID the answer is for
     * @return an answer from the server
     * @throws ServerErrorException exception
     */
    public static CompletableFuture<Answer> answerQuestion(AnswerCreateRequest answerCreateRequest,
                                                           long lectureId, long questionId)
            throws ServerErrorException {
        return post("/lecture/" + lectureId
                + "/questions/" + questionId + "/answer", answerCreateRequest, Answer.class);
    }

    /**
     * Marks a question as answered.
     * @param lectureId the lecture the question belongs to
     * @param questionId the question being marked as answered
     * @throws ServerErrorException exception
     */
    public static CompletableFuture<Void> markAsAnswered(long lectureId, long questionId)
            throws ServerErrorException {
        return post("/lecture/" + lectureId + "/questions/" + questionId + "/markAnswered",
                null,  Void.TYPE);
    }


    public static CompletableFuture<Void> claimQuestion(long questionId, long userId)
            throws ServerErrorException {
        return post("/lecture/questions/" + questionId + "/claim/" + userId,
                null, Void.TYPE);
    }

    /**
     * Makes a "Speed up" reaction.
     * @param lectureID id of the lecture.
     * @param type false for Speed up button.
     * @throws ServerErrorException exception.
     */
    public static CompletableFuture<Reaction> speedReactionFaster(long lectureID, boolean type)
            throws ServerErrorException {
        return post("/lecture/" + lectureID + "/reaction/"
                + type, null, Reaction.class);
    }

    /**
     * Makes a "Slow Down" reaction.
     * @param lectureID id of the lecture.
     * @param type true for Slow down button.
     * @throws ServerErrorException exception.
     */
    public static CompletableFuture<Reaction> speedReactionSlower(long lectureID, boolean type)
            throws ServerErrorException {
        return post("/lecture/" + lectureID + "/reaction/"
                + type, null, Reaction.class);
    }

    /**
     * Lists Reactions of a given lecture.
     * @param lectureID id of the lecture for which to list reactions.
     * @return list of Reactions.
     * @throws ServerErrorException exception.
     */
    public static CompletableFuture<List<Reaction>> listReaction(long lectureID)
            throws ServerErrorException {
        return get("/lecture/" + lectureID + "/reactions",
                        new TypeToken<List<Reaction>>(){}.getType());
    }

    /**
     * Communicate with the server to end a lecture.
     * @param lecture the lecture to be ended
     */
    public static CompletableFuture<Void> endLecture(Lecture lecture) {
        return post("/lecture/end", lecture, Lecture.class);
    }

    /**
     * Creates a poll for a lecture.
     * @param lectureId id of lecture for creating a poll
     * @param poll poll to be created
     * @return the updated lecture
     */
    public static CompletableFuture<Lecture> createPoll(long lectureId, PollCreateRequest poll) {
        return post("/lecture/" + lectureId + "/poll", poll, Lecture.class);
    }

    /**
     * Closes the poll in the current lecture.
     * @param lectureId id of lecture for closing a poll
     * @param code code of user closing the poll
     */
    public static CompletableFuture<Void> closePoll(long lectureId, String code) {
        return post("/lecture/" + lectureId + "/closePoll?code=" + code, null, Void.TYPE);
    }


    /**
     * Removes the poll in the current lecture.
     * @param lectureId id of lecture for closing a poll
     * @param code code of user closing the poll
     */
    public static CompletableFuture<Void> removePoll(long lectureId, String code) {
        return post("/lecture/" + lectureId + "/removePoll?code=" + code, null, Void.TYPE);
    }


    /**
     * Sends an answer of poll to server.
     * @param lectureId the lecture the poll belongs to.
     * @param request the request containing the response
     */
    public static CompletableFuture<Void> answerPoll(long lectureId, PollAnswerRequest request) {
        return post("/lecture/" + lectureId + "/poll/answer", request, Lecture.class);
    }


    /** Ban a user in a lecture.
     * @param lectureId The id of the lecture.
     * @param userId The id of the user.
     * @throws ServerErrorException exception
     */
    public static CompletableFuture<Void> banUser(long lectureId, long userId)
            throws ServerErrorException {
        return post("/lecture/" + lectureId + "/ban?user=" + userId, null, Void.TYPE);
    }

    /**
     * Unban a user in a lecture.
     * @param lectureId The id of the lecture.
     * @param ipAddress The address of the banned user.
     * @throws ServerErrorException exception
     */
    public static CompletableFuture<Void> unbanUser(long lectureId, String ipAddress)
            throws ServerErrorException {
        return post("/lecture/" + lectureId + "/unban?ip=" + ipAddress, null, Void.TYPE);
    }
}












