package nl.tudelft.oopp.application.controller.questiontile;

import static java.util.concurrent.CompletableFuture.completedFuture;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import lombok.extern.java.Log;
import nl.tudelft.oopp.application.communication.ServerCommunication;
import nl.tudelft.oopp.application.controller.StudentPageController;
import nl.tudelft.oopp.application.data.Question;

@Log
public class QuestionTileController {

    @FXML
    public Label numQuestion;
    @FXML
    public Label questionTitle;
    @FXML
    public Label questionContent;
    @FXML
    public Label upvoteCount;
    @FXML
    public HBox box;
    @FXML
    public Label dateTime;
    @FXML
    public Label claimedByLabel;
    @FXML
    public Label answerLabel;
    @FXML
    private Question question;

    /**
     * Displays a new question tile.
     */
    public static CompletableFuture<QuestionTileController> display() {
        return CompletableFuture.supplyAsync(() -> {
            FXMLLoader fxmlLoader = new FXMLLoader(
                    QuestionTileController.class.getResource("/questionTile/questionTile.fxml"));
            try {
                fxmlLoader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return fxmlLoader.getController();
        });
    }

    /**
     * Updates displayed question info based on question data.
     * @param question The question data.
     */
    public void setInfo(int index, Question question) {
        if (question == null) {
            return;
        }
        this.question = question;
        numQuestion.setText("#");
        questionTitle.setText(question.getTitle());
        questionTitle.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        questionContent.setText(question.getContent()
                + ((question.getAuthor() != null) ? ("\n" + "Author: "
                + question.getAuthor().getName()) : ""));
        questionContent.setMinHeight(Region.USE_PREF_SIZE);
        questionContent.setWrapText(true);
        upvoteCount.setText(question.getUpvotes() + "");
        dateTime.setText(new Date(question.getDate()).toString());
        if (question.getClaimedBy() != null) {
            claimedByLabel.setText("Claimed by: "
                    + question.getClaimedBy().getName());
        }
        if (question.isAnswered()) {
            claimedByLabel.setText("Answered");
        }
        if (question.getAnswer() != null) {
            answerLabel.setText("Answer : " + question.getAnswer().getAnswer());
            answerLabel.setFont(Font.font("Arial", FontWeight.LIGHT, FontPosture.ITALIC, 13));
            answerLabel.setMinHeight(Region.USE_PREF_SIZE);
            answerLabel.setWrapText(true);
        }
    }

    private void fetchQuestionUpvotes() {
        ServerCommunication.getQuestionUpvotes(
                question.getId(), StudentPageController.lecture.getId())
            .thenAccept(upvoters -> {
                upvoteCount.setText(upvoters.size() + "");
            });
    }

    /**
     * Called when the user clicks the upvote button.
     */
    public void upvoteQuestion() {
        ServerCommunication.upvoteQuestion(
            this.question.getId(), StudentPageController.lecture.getId(),
            StudentPageController.user.getId());
    }


    /**
     * Deletes the question when the delete button is clicked,
     * (by the student who created the question).
     */
    @FXML
    CompletableFuture<Void> onDeleteClicked() {
        long questionId = question.getId();

        long authorId = question.getAuthor().getId();
        long userId = StudentPageController.user.getId();

        if (authorId == userId) {
            return ServerCommunication.deleteQuestion(
                    StudentPageController.lecture.getId(), questionId)
                    .exceptionally((e) -> {
                        log.info("Error deleting this question");
                        e.printStackTrace();
                        throw new CompletionException(e);
                    });
        } else {
            return completedFuture(null);
        }
    }

    public HBox getBox() {
        return box;
    }
}
