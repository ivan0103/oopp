package nl.tudelft.oopp.application.controller;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import lombok.extern.java.Log;
import nl.tudelft.oopp.application.communication.ServerCommunication;
import nl.tudelft.oopp.application.communication.dtos.LectureJoinResponse;
import nl.tudelft.oopp.application.data.User;
import nl.tudelft.oopp.application.data.UserRole;
import nl.tudelft.oopp.application.util.TimeUtil;

@Log
public class JoinPageController {

    private static final String JOIN_AS = "Join as ";

    @FXML
    private Button joinButton;

    @FXML
    private Label courseName;

    @FXML
    private Label lectureName;

    @FXML
    private Label timePeriod;

    @FXML
    public Label enterLabel;

    @FXML
    private TextField nameTextField;

    private String joinCode;


    /**
     * Display join lecture window.
     * Opens a new window.
     */
    @SneakyThrows
    public static CompletableFuture<Boolean> display(String joinCode) {
        // check join code
        log.info("Checking join code: " + joinCode);
        CompletableFuture<Boolean> future = new CompletableFuture();
        ServerCommunication.checkJoinCode(joinCode)
            .thenAccept((checkCodeResponse -> {
                if (!checkCodeResponse.isCorrect()) {
                    // show invalid code alert
                    Platform.runLater(() -> {
                        Alert alert = new Alert(Alert.AlertType.WARNING,
                                "The lecture you tried to join doesn't exist!", ButtonType.OK);
                        alert.show();
                        alert.setOnCloseRequest((e) -> {
                            future.complete(false);
                        });
                    });
                    return;
                } else if (checkCodeResponse.isBanned()) {
                    // show banned alert
                    Platform.runLater(() -> {
                        Alert alert = new Alert(Alert.AlertType.WARNING,
                                "You have been banned from this lecture!", ButtonType.OK);
                        alert.show();
                        alert.setOnCloseRequest((e) -> {
                            future.complete(false);
                        });
                    });
                    return;
                } else if (checkCodeResponse.getScheduledDate() != null) {
                    // show scheduled alert
                    Platform.runLater(() -> {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION,
                                "This lecture hasn't started yet.\nIt is scheduled at: "
                                        + checkCodeResponse.getScheduledDate(), ButtonType.OK);
                        alert.show();
                        alert.setOnCloseRequest((e) -> {
                            future.complete(false);
                        });
                    });
                    return;
                }

                // load fxml
                FXMLLoader fxmlLoader = new FXMLLoader(
                        JoinPageController.class.getResource("/joinPage.fxml"));
                Parent root;
                try {
                    root = fxmlLoader.load();
                } catch (IOException exception) {
                    throw new CompletionException(exception);
                }

                Platform.runLater(() -> {
                    // create new window
                    Stage stage = new Stage();
                    stage.setTitle("Enter a lecture");
                    stage.setScene(new Scene(root));

                    // set lecture info
                    JoinPageController controller = fxmlLoader.getController();
                    controller.setJoinCode(joinCode);
                    controller.setUserRole(checkCodeResponse.getUserRole());

                    // show window
                    stage.show();
                    stage.setMinHeight(500);
                    stage.setMinWidth(819);
                    future.complete(true);
                });
            }));
        return future;
    }

    /**
     * Sets the join code used by this join screen.
     * @param joinCode The join code.
     */
    public void setJoinCode(String joinCode) {
        this.joinCode = joinCode;
        joinButton.setDisable(false);
        setLectureInfo();
    }

    /**
     * Sets the user role shown on the join screen.
     * @param userRole The user role.
     */
    public void setUserRole(UserRole userRole) {
        joinButton.setText(JOIN_AS + userRole.name().toLowerCase());
    }

    /**
     * Called when the join button is clicked.
     */
    public void onJoinClicked() {
        // get username
        String username = nameTextField.getText();
        if (username == null || username.equals("")) {
            return;
        }

        // join lecture
        LectureJoinResponse joinResponse;
        try {
            joinResponse = ServerCommunication.joinLecture(new User(username), joinCode).get();
        } catch (Exception e) {
            log.warning("Error joining lecture!");
            e.printStackTrace();
            return;
        }

        // open appropriate window
        System.out.println("JOINED SUCCESSFUL " + "\n"
                + "Name of joiner: " + joinResponse.getUser().getName() + "\n"
                + "Role of joiner: " + joinResponse.getUser().getRole() + "\n"
                + "IP Address of joiner: " + joinResponse.getUser().getIpAddress());
        if (joinResponse.getUser().getRole() == UserRole.STUDENT) {
            System.out.println("STUDENT JOIN");
            StudentPageController.display(joinResponse.getUser(), joinResponse.getLecture())
                .thenRun(() -> Platform.runLater(() ->
                        ((Stage) joinButton.getScene().getWindow()).close()));
        } else if (joinResponse.getUser().getRole() == UserRole.LECTURER) {
            System.out.println("LECTURER/MODERATOR JOIN");
            LecturerPageController.display(joinResponse.getUser(), joinResponse.getLecture())
                    .thenRun(() -> Platform.runLater(() ->
                            ((Stage) joinButton.getScene().getWindow()).close()));
        }
    }

    /**
     * Sets the information on the page.
     */
    public CompletableFuture<Void> setLectureInfo() {
        return ServerCommunication.getLectureByJoinCode(joinCode)
            .thenAccept((lecture) -> Platform.runLater(() -> {
                courseName.setText(lecture.getCourse());
                courseName.setMaxWidth(Double.MAX_VALUE);
                AnchorPane.setLeftAnchor(courseName, 0.0);
                AnchorPane.setRightAnchor(courseName, 0.0);
                courseName.setAlignment(Pos.CENTER);

                lectureName.setText(lecture.getName());
                lectureName.setMaxWidth(Double.MAX_VALUE);
                AnchorPane.setLeftAnchor(lectureName, 0.0);
                AnchorPane.setRightAnchor(lectureName, 0.0);
                lectureName.setAlignment(Pos.CENTER);

                timePeriod.setText(TimeUtil.TIME_FORMAT.format(lecture.getTime())
                        + "\n" + TimeUtil.DATE_FORMAT.format(lecture.getTime()));
                System.out.println("The lecture info has been set!");
            }));
    }
}
