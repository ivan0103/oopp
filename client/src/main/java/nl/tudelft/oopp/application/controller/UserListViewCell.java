package nl.tudelft.oopp.application.controller;

import javafx.scene.control.ListCell;
import nl.tudelft.oopp.application.data.User;

public class UserListViewCell extends ListCell<User> {

    UserTileController tileController;

    public UserListViewCell() {
        UserTileController.display().thenAccept((tile) -> this.tileController = tile);
    }

    @Override
    public void updateItem(User user, boolean empty) {
        super.updateItem(user, empty);
        if (tileController == null || empty || user == null) {
            setGraphic(null);
        } else {
            tileController.setInfo(user);
            setGraphic(tileController.getBox());
        }
    }

}
