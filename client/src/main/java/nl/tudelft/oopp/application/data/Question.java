package nl.tudelft.oopp.application.data;

import java.util.Date;
import java.util.Set;
import lombok.Data;

@Data
public class Question {
    private long id;
    private String title;
    private String content;
    private boolean answered;
    private int upvotes;
    private User claimedBy;
    private User author;
    private Answer answer;
    private long date;
    private boolean isManipulated;


    /**
     * Method for writing a question into a human-friendly String.
     * @return - String that represents the question.
     */
    public String writeToFile() {
        String answeredString = "no";
        if (answered) {
            answeredString = "yes";
        }

        String answerString = "none";
        if (answer != null) {
            answerString = answer.getAnswer();
        }

        String claimedString = "not claimed";
        if (claimedBy != null) {
            claimedString = claimedBy.getName();
        }

        int upvotesString = upvotes;

        String result = "Question id: \t" + id
                + "\nTitle: \t\t" + title
                + "\nContent: \t" + content
                + "\nAnswered: \t" + answeredString
                + "\nClaimed by: \t" + claimedString
                + "\nUpvotes: \t" + upvotesString
                + "\nAuthor: \t" + author.getName()
                + "\nResponse: \t" + answerString;


        return result;
    }


}



