package nl.tudelft.oopp.application.communication.dtos;

import lombok.Data;

@Data
public class AnswerCreateRequest {
    private long userID;
    private String content;

    public AnswerCreateRequest(long userID, String content) {
        this.userID = userID;
        this.content = content;
    }
}
