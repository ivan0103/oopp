package nl.tudelft.oopp.application.controller.dialog;

import static java.util.concurrent.CompletableFuture.completedFuture;

import java.util.concurrent.CompletableFuture;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import lombok.extern.java.Log;
import nl.tudelft.oopp.application.controller.JoinPageController;

@Log
public class JoinCodeDialogController {

    @FXML
    TextField joinCodeField;

    /**
     * Displays the join code dialog.
     * Opens a new window with the dialog.
     */
    @SneakyThrows
    public static CompletableFuture<Void> display() {
        // load fxml
        FXMLLoader fxmlLoader = new FXMLLoader(
                JoinCodeDialogController.class.getResource("/dialogs/joinCodeDialog.fxml"));
        Parent root = fxmlLoader.load();

        // create new window
        CompletableFuture<Void> future = new CompletableFuture();
        Platform.runLater(() -> {
            Stage stage = new Stage();
            stage.setTitle("Join a lecture");
            stage.setScene(new Scene(root));
            stage.show();
            future.complete(null);
        });
        return future;
    }

    /**
     * Called when the user clicks the join lecture button.
     */
    @FXML
    CompletableFuture<Void> onJoinLectureButtonClicked() {
        // get code
        String joinCode = joinCodeField.getText();
        if (joinCode == null || joinCode.equals("")) {
            return completedFuture(null);
        }

        log.info("Joining lecture with code: " + joinCode);

        // join lecture
        return JoinPageController.display(joinCode)
            .thenAccept((success) -> {
                if (success) {
                    // close this dialog
                    Platform.runLater(() -> ((Stage) joinCodeField.getScene().getWindow()).close());
                }
            });
    }
}
