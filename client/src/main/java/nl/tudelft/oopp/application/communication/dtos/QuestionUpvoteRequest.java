package nl.tudelft.oopp.application.communication.dtos;

import lombok.Data;

@Data
public class QuestionUpvoteRequest {
    private final long userId;
}
