package nl.tudelft.oopp.application.controller;

import static java.util.concurrent.CompletableFuture.completedFuture;
import static java.util.concurrent.CompletableFuture.failedFuture;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.java.Log;
import nl.tudelft.oopp.application.communication.ServerCommunication;
import nl.tudelft.oopp.application.communication.dtos.AnswerCreateRequest;
import nl.tudelft.oopp.application.communication.dtos.QuestionEditRequest;
import nl.tudelft.oopp.application.controller.questiontile.LecturerListViewCell;
import nl.tudelft.oopp.application.controller.questiontile.LecturerTileController;
import nl.tudelft.oopp.application.controller.questiontile.QuestionTileController;
import nl.tudelft.oopp.application.data.FilterOption;
import nl.tudelft.oopp.application.data.Lecture;
import nl.tudelft.oopp.application.data.Poll;
import nl.tudelft.oopp.application.data.Question;
import nl.tudelft.oopp.application.data.Reaction;
import nl.tudelft.oopp.application.data.SortOption;
import nl.tudelft.oopp.application.data.User;
import nl.tudelft.oopp.application.exception.UserBannedException;
import nl.tudelft.oopp.application.util.TimeUtil;

@Log
@Data
public class LecturerPageController {
    /**
     * Attributes from the mainPageLecturerCopy fxml file.
     */
    @FXML
    private Label lectureName;
    @FXML
    private Label courseName;
    @FXML
    private MenuButton nameUser;
    @FXML
    private Label firstLetterUser;
    @FXML
    private GridPane questionPane;
    @FXML
    private Button exitButton;
    @FXML
    private Button allQuestions;
    @FXML
    private Button answeredFilterButton;
    @FXML
    private Button unAnsweredFilterButton;
    @FXML
    private MenuButton sortByMenu;
    @FXML
    private MenuItem relevanceSort;
    @FXML
    private MenuItem timeSort;
    @FXML
    private MenuItem upvoteSort;
    @FXML
    private Label numQuestion;
    @FXML
    private Label questionTitle;
    @FXML
    private Label questionText;
    @FXML
    private Label numUpvotes;
    @FXML
    private Button exportQuestion;
    @FXML
    private VBox leftBar;
    @FXML
    private Label modeLabel;
    @FXML
    private Button endLecture;
    @FXML
    private NumberAxis yyAxis;
    @FXML
    private  CategoryAxis xxAxis;
    @FXML
    private StackedBarChart<String, Number> reactionsChart;
    @FXML
    public ListView<Question> questionsList;
    @FXML
    private MenuButton inviteMenu;
    @FXML
    private Button createPoll;
    @FXML
    private Button closePoll;
    @FXML
    private BorderPane borderPollPane;
    @FXML
    private Label quizTitle;
    @FXML
    private ListView<Button> optionsPane;
    @FXML
    public BorderPane respondVbox;
    @FXML
    private TextArea respondText;
    @FXML
    private Button submitResponse;
    @FXML
    private Label questionTitleLabel;
    @FXML
    private Label type;
    @FXML
    private Circle circleLectureCourse;
    @FXML
    private BorderPane questionsBorderPane;
    @FXML
    private BorderPane listUsers;
    @FXML
    private Label count;
    @FXML
    private ListView<User> listMembers;
    @FXML
    private MenuItem memberList;

    ObservableList<Question> questionsListData = FXCollections.observableArrayList();
    ObservableList<User> usersListData = FXCollections.observableArrayList();

    private static User user;
    public static Lecture lecture;
    public static Lecture thisLecture;

    /**
     * Option for sorting. Set UPVOTE as default sorting method
     */
    private SortOption sortOption = SortOption.upvotes;

    /**
     * Option for filtering. Set ALL as default filtering method
     */
    private FilterOption filterOption = FilterOption.unanswered;

    private QuestionTileController questionTileController;
    private LecturerTileController lecturerTileController;
    // data for reactions and chart.
    ObservableList<Reaction> reactionsListData = FXCollections.observableArrayList();
    private final XYChart.Series<String, Number> series1 = new XYChart.Series<>();
    private final XYChart.Series<String, Number> series2 = new XYChart.Series<>();
    private Number slower;
    private Number faster;

    private boolean presentationMode = false;

    private Timeline timeline;

    /**
     * Initializes the controller.
     */
    @FXML
    public void initialize() {
        updateUserInformation(user);
        updateLectureInformation(lecture);
        questionsBorderPane.setBottom(null);
        questionsBorderPane.setRight(null);
        questionsBorderPane.setCenter(questionsList);
        respondVbox.setVisible(false);
        listUsers.setVisible(false);
        // edit chart.
        xxAxis.setAnimated(false);
        xxAxis.setCategories(FXCollections.observableArrayList("SLOWER", "FASTER"));
        series1.setName("SLOWER");
        series2.setName("FASTER");
        reactionsChart.getStylesheets().add("style.css");
        reactionsChart.getData().addAll(series1, series2);
        initQuestionsList();
        initUserList();
        autoLoad(1000);
        resetReaction(1000 * 60 * 5);
    }

    private void autoLoad(int milliSecond) {
        timeline = new Timeline();
        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(milliSecond),
            ae -> {
                updateLecture();
                if (noQuestionIsManipulated()) {
                    updateQuestions();
                }
                updateSpeedReaction();
                memberList();
            }));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    private boolean noQuestionIsManipulated() {
        for (Question questionsListDatum : questionsListData) {
            if (questionsListDatum.isManipulated()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Update lecture information.
     */
    public CompletableFuture<Void> updateLecture() {
        return ServerCommunication.getLectureByJoinCode(lecture.getModeratorCode())
                .thenAccept((lecture) -> Platform
                        .runLater(() -> updateLectureInformation(lecture)));
    }

    /**
     * Let's a specific user (lecturer/moderator) join this main page.
     * @param user User information.
     * @param lecture Lecture information.
     */
    @SneakyThrows
    public static CompletableFuture<Void> display(User user, Lecture lecture) {
        // set info
        LecturerPageController.user = user;
        LecturerPageController.lecture = lecture;

        // load fxml
        FXMLLoader fxmlLoader = new FXMLLoader(
                LecturerPageController.class.getResource("/mainPageLecturer.fxml"));
        Parent root = fxmlLoader.load();

        // create new window
        CompletableFuture<Void> future = new CompletableFuture();
        Platform.runLater(() -> {
            Stage stage = new Stage();
            stage.setTitle(lecture.getName() + " - " + lecture.getCourse()
                    + " (" + user.getRole() + ")");

            stage.setScene(new Scene(root));
            stage.show();
            stage.setMinHeight(540);
            stage.setMinWidth(960);
            future.complete(null);
        });

        return future;
    }

    /**
     * Updates user information in the ui.
     * @param user user info.
     */
    private void updateUserInformation(User user) {
        nameUser.setText(user.getName());
        firstLetterUser.setText(user.getName().substring(0, 1).toUpperCase());
    }

    /**
     * Retrieves the lecture that the current user is in.
     */
    private void updateLectureInformation(Lecture lecture) {
        thisLecture = lecture;
        lectureName.setText(lecture.getName());
        lectureName.setMinSize(Label.USE_PREF_SIZE, Label.USE_PREF_SIZE);
        courseName.setText(lecture.getCourse());
        // no poll available
        if (lecture.getPoll() == null) {
            createPoll.setDisable(false);
            borderPollPane.setVisible(false);
        } else {
            borderPollPane.setVisible(true);
            updatePoll(lecture);
        }
    }

    /**
     * Initializes the questions list.
     */
    public void initQuestionsList() {
        questionsList.setEditable(false);
        questionsList.setSelectionModel(null);
        questionsList.setCellFactory(listView -> new LecturerListViewCell(this));
        questionsList.setItems(questionsListData);
        updateQuestions();
    }

    /**
     * Fetches all the questions of this lecture and displays them on the screen.
     */
    public CompletableFuture<Void> updateQuestions() {
        // fetch questions from the server
        return ServerCommunication.listQuestions(
                lecture.getId(), filterOption, sortOption)
            // update list data
            .thenAccept((questions -> Platform.runLater(() -> {
                if (presentationMode) {
                    if (questions.size() < 6) {
                        questionsListData.setAll(questions);
                    } else {
                        for (int i = 5; i < questions.size(); i++) {
                            questions.remove(i);
                        }
                        questionsListData.setAll(questions);
                    }
                } else {
                    questionsListData.setAll(questions);
                }
            })))
            .exceptionally((e) -> {
                if (e instanceof UserBannedException) {
                    // user is banned, show alert and close page
                    timeline.stop();
                    Platform.runLater(() -> {
                        Alert alert = new Alert(Alert.AlertType.WARNING,
                                "You were banned from this lecture!\nThis window will close.",
                                ButtonType.CLOSE);
                        alert.show();
                        ((Stage) questionsList.getScene().getWindow()).close();
                    });
                } else {
                    log.warning("Error fetching the list of questions!");
                    e.printStackTrace();
                }
                throw new CompletionException(e);
            });
    }

    /**
     * Exporting question to a specific location on local machine.
     */
    public void exportQuestionToADirectory() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialFileName("MyQuestion");
        fileChooser.getExtensionFilters()
                .add(new FileChooser.ExtensionFilter("text file", "*.txt"));

        try {
            File file = fileChooser.showSaveDialog(new Stage());
            writeToFile(file.getAbsolutePath());
        } catch (Exception e) {
            System.out.println("UNSAVED");
        }
    }

    /**
     * Write the question to the path.
     * @param absolutePath path to save the question.
     */
    public CompletableFuture<Void> writeToFile(String absolutePath) {
        return ServerCommunication.listQuestions(
                lecture.getId(), filterOption, sortOption)
            .thenAccept((questions) -> {
                String lectureInformation = "Date: \t\t"
                        + TimeUtil.DATE_FORMAT.format(lecture.getTime())
                        + "\nTime: \t\t" + TimeUtil.TIME_FORMAT.format(lecture.getTime())
                        + "\nCourse: \t" + lecture.getCourse()
                        + "\nLecture: \t" + lecture.getName();
                try {
                    FileWriter fileWriter = new FileWriter(absolutePath);
                    fileWriter.write(lectureInformation + "\n\n");
                    for (Question question : questions) {
                        fileWriter.write(question.writeToFile() + "\n\n");
                    }
                    fileWriter.close();
                } catch (IOException e) {
                    throw new CompletionException(e);
                }
                System.out.println("DATA WRITTEN SUCCESSFUL");
            });
    }

    /**
     * Provides the presentation mode screen, showing only the top 5 most upvoted questions.
     */
    public void presentationModeClicked() {
        // Hiding left bar, showing that lecturer is in presentation mode
        leftBar.setVisible(false);
        leftBar.setMaxWidth(0);
        lectureName.setText("Presentation mode");
        modeLabel.setText("");
        circleLectureCourse.setVisible(false);
        courseName.setText("");
        inviteMenu.setVisible(false);
        respondVbox.setVisible(false);
        listUsers.setVisible(false);
        questionsBorderPane.setBottom(null);
        questionsBorderPane.setRight(null);
        lecturerTileController.editQuestion.setDisable(true);
        memberList.setDisable(true);

        presentationMode = true;
        updateQuestions();
    }

    /**
     * Provides the normal mode screen.
     */
    public void normalModeClicked() {
        // Going back to normal settings
        leftBar.setVisible(true);
        leftBar.setMaxWidth(250);
        modeLabel.setText("Normal mode");
        lecturerTileController.editQuestion.setDisable(false);
        memberList.setDisable(false);
        lectureName.setText(lecture.getName());
        circleLectureCourse.setVisible(true);
        courseName.setText(lecture.getCourse());
        inviteMenu.setVisible(true);

        presentationMode = false;
        updateQuestions();
    }

    /**
     * Auto update for the new questions.
     * @param milliSecond the update frequency.
     */
    public void resetReaction(int milliSecond) {
        Timeline timeline = new Timeline();
        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(milliSecond),
            ae -> reset()));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    /**
     * removes all reactions from the list, graph and database.
     */
    public void reset() {
        try {
            for (Reaction reaction : reactionsListData) {
                ServerCommunication.deleteReaction(lecture.getId(), reaction.getId()).get();
            }
            Platform.runLater(() -> {
                series1.getData().clear();
                series2.getData().clear();
                reactionsListData.clear();
            });
            updateSpeedReaction();
            log.info("Data reset!");
        } catch (CompletionException e) {
            log.info("Error deleting this reaction");
            e.printStackTrace();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Fetches all the Reactions of this lecture per second.
     */
    public CompletableFuture<Void> updateSpeedReaction() {
        // fetch Reactions from the server
        return ServerCommunication.listReaction(lecture.getId())
            .thenAccept((reactions) -> {
                reactionsListData.setAll(reactions);
                slower = reactionsListData.stream().filter(reaction -> reaction.isType()).count();
                faster = reactionsListData.stream().filter(reaction -> !reaction.isType()).count();
                Platform.runLater(this::updateGraph);
            })
            .exceptionally((e) -> {
                log.warning("Error updating Speed reaction bar chart!");
                e.printStackTrace();
                throw new CompletionException(e);
            });
    }

    /**
     * Updates the stacked bar chart for the reactions.
     * add wanted value, then remove it to stop constant addition.
     * and overflow the graph.
     */
    public void updateGraph() {
        int slow = slower.intValue();
        int fast = faster.intValue();
        series1.getData().addAll(new XYChart.Data<>("SLOWER", slower));
        series1.getData().addAll(new XYChart.Data<>("SLOWER", -slow));
        series2.getData().addAll(new XYChart.Data<>("FASTER", faster));
        series2.getData().addAll(new XYChart.Data<>("FASTER", -fast));
        if (slow + 10 >= yyAxis.getUpperBound()
                || fast + 10 >= yyAxis.getUpperBound()) {
            yyAxis.setUpperBound(yyAxis.getUpperBound() + 10);
        }
        log.info(reactionsListData.size() + " reactions! "
                + slower + " slower and " + faster + " faster!");
    }

    /**
     * Stop student to post any further questions when 'end lecture' is clicked.
     */
    public void endLectureClicked() {
        // Provide a whole lecture object to avoid malicious user to end a lecture using
        // just an id of that lecture.
        // The student's lecture object will not have moderator code attribute
        // moderatorCode = null for student.
        if (endLecture.getText().equals("End Session")) {
            endLecture.setText("Start Session");
        } else {
            endLecture.setText("End Session");
        }
        ServerCommunication.endLecture(lecture);
    }

    /**
     * Copy student link to the clip board when the user click on this.
     */
    public void inviteStudentClicked() {
        writeToClipboard(lecture.getStudentCode(), null);
        System.out.println("Copied student invite link");
    }

    /**
     * Copy moderator link to the clip board when the user click on this.
     */
    public void inviteModeratorClicked() {
        writeToClipboard(lecture.getModeratorCode(), null);
        System.out.println("Copied moderator invite link");
    }

    /**
     * Write something to system clipboard.
     * @param s String to write to the clipboard
     * @param owner Owner of the clipboard
     */
    public void writeToClipboard(String s, ClipboardOwner owner) {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable transferable = new StringSelection(s);
        clipboard.setContents(transferable, owner);
    }

    /**
     * Shows the response window for lecturer.
     */
    public void showResponseWindow(Question question) {
        questionsBorderPane.setBottom(respondVbox);
        type.setText("Answer Question");
        respondVbox.setVisible(true);
        questionTitleLabel.setText(question.getTitle());
        submitResponse.setOnAction(e -> {
            try {
                ServerCommunication.answerQuestion(
                        new AnswerCreateRequest(user.getId(), respondText.getText()),
                        lecture.getId(), question.getId());
                respondVbox.setVisible(false);
                questionsBorderPane.setBottom(null);
            } catch (Exception exception) {
                exception.printStackTrace();
                System.out.println("Error writing a response");
            }
        });
    }

    /**
     * Shows the response window for lecturer / moderator.
     */
    public void editQuestion(Question question) {
        if (listUsers.isVisible()) {
            listUsers.setBottom(respondVbox);
        }
        questionsBorderPane.setBottom(respondVbox);
        type.setText("Edit Question");
        respondText.setText(question.getContent());
        respondVbox.setVisible(true);
        questionTitleLabel.setText(question.getTitle());
        submitResponse.setOnAction(e -> {
            try {
                ServerCommunication.editQuestion(
                        new QuestionEditRequest(user.getId(), respondText.getText()),
                        lecture.getId(), question.getId());
                respondVbox.setVisible(false);
                questionsBorderPane.setBottom(null);
            } catch (Exception exception) {
                exception.printStackTrace();
                System.out.println("Error writing a response");
            }
        });
    }


    /**
     * Updates the poll if it exists.
     */
    public void updatePoll(Lecture lecture) {
        borderPollPane.setVisible(true);
        Poll poll = lecture.getPoll();
        quizTitle.setText(poll.getQuiz());
        if (poll.isClosed()) {
            closePoll.setText("Remove Poll");
            closePoll.setStyle("-fx-background-color:  #921e38");
        } else {
            closePoll.setText("Close Poll");
            closePoll.setStyle("-fx-background-color:  #6D7EF7");
        }

        // place statistics on pane
        showStats(poll);

    }

    /**
     * Get the bit at position pos(th) of the number num.
     * @param num the number to be extracted bit from.
     * @param pos the position to extract the bit from.
     * @return 0 or 1.
     */
    private int getBit(int num, int pos) {
        return (num >> pos) & 1;
    }

    private void showStats(Poll poll) {
        optionsPane.getItems().clear();
        int row = 0;
        int total = 0;
        int precision = 50;
        char choice = 'A';

        List<Integer> stats = poll.getStatistics();
        for (int i = 0; i < stats.size(); i++) {
            total += stats.get(i);
        }
        for (int i = 0; i < stats.size(); i++) {
            Label stat = new Label("" + stats.get(i));

            int percentage = 0;
            if (total != 0) {
                percentage = (int) ((double) stats.get(i) / total * 100.00);
            }

            Button statRepresentation = new Button(stats.get(i) + " ~ " + percentage + "%"
                    + generateString((int) (percentage / ((double) 100.00 / precision)), " "));

            if (getBit(poll.getAnswer(), i) == 1) {
                statRepresentation.setStyle("-fx-background-color:#008000; "
                        + "-fx-text-base-color: #b1b1b1; -fx-font-size:10.0");

            } else {
                statRepresentation.setStyle("-fx-background-color:#921e38; "
                        + "-fx-text-base-color: #b1b1b1; -fx-font-size:10.0");
            }

            optionsPane.getItems().add(statRepresentation);
            optionsPane.setCellFactory(param -> new ListCell<Button>() {
                @Override
                protected void updateItem(Button item, boolean empty) {
                    super.updateItem(item, empty);

                    if (empty || item == null) {
                        setText(null);
                        setStyle("-fx-background-color: #7d7d7d;");
                    } else {
                        //setText(item.getText());
                        setStyle("-fx-background-color: #7d7d7d;");
                        setGraphic(item);
                    }
                }
            });

        }
    }

    /**
     * Generating a string of n repeated substring.
     * @param n the length of the result string.
     * @param str the substring.
     * @return a new string that is a repeats of old substring.
     */
    public String generateString(int n, String str) {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < n; i++) {
            res.append(str);
        }
        return res.toString();
    }

    /**
     * Either opens or removes a poll (deleted from the lecture).
     */
    public void createPollClicked() {
        if (lecture.getPoll() == null) {
            PollCreateController.display();
            PollCreateController.setInfo(lecture);
            createPoll.setDisable(true);
        }
    }

    /**
     * Closes the current poll in lecture.
     */
    public void closePollClicked() {
        if (thisLecture.getPoll() != null) {
            if (!thisLecture.getPoll().isClosed()) {
                ServerCommunication.closePoll(thisLecture.getId(), thisLecture.getModeratorCode());
            } else {
                ServerCommunication.removePoll(thisLecture.getId(), thisLecture.getModeratorCode());
                borderPollPane.setVisible(false);
            }
        }
    }

    /**
     * Initializes the users list.
     */
    public void initUserList() {
        listMembers.setEditable(false);
        listMembers.setSelectionModel(null);
        listMembers.setCellFactory(listView -> new UserListViewCell());
        listMembers.setItems(usersListData);
        memberList();
    }

    /**
     * Shows the response window for lecturer.
     */
    public void showUserList() {
        listUsers.setVisible(true);
        questionsBorderPane.setBottom(null);
        questionsBorderPane.setRight(listUsers);
        questionsBorderPane.setCenter(questionsList);

        if (respondVbox.isVisible()) {
            listUsers.setBottom(respondVbox);
            questionsBorderPane.setBottom(respondVbox);
        }
    }

    /**
     * Closes user list.
     */
    public void closeListMembers() {
        questionsBorderPane.setRight(null);
        questionsBorderPane.setCenter(questionsList);
        listUsers.setVisible(false);
    }

    /**
     * Fetches all the members of this lecture and displays them on the screen.
     */
    public CompletableFuture<Void> memberList() {
        // fetch users from the server
        return ServerCommunication.listUsers(lecture.getId())
            .thenAccept((userList) ->
                // update user data
                Platform.runLater(() -> {
                    count.setText(String.valueOf(usersListData.size()));
                    usersListData.setAll(userList);
                }));
    }

    /**
     * Filters by unanswered.
     */
    public void filterUnanswered() {
        this.filterOption = FilterOption.unanswered;
    }

    /**
     * Filters by answered.
     */
    public void filterAnswered() {
        this.filterOption = FilterOption.answered;
    }


    public static User getUser() {
        return user;
    }
}
