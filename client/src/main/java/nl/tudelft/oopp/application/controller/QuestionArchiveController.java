package nl.tudelft.oopp.application.controller;

import static java.util.concurrent.CompletableFuture.completedFuture;
import static java.util.concurrent.CompletableFuture.failedFuture;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import lombok.extern.java.Log;
import nl.tudelft.oopp.application.communication.ServerCommunication;
import nl.tudelft.oopp.application.controller.dialog.JoinCodeDialogController;
import nl.tudelft.oopp.application.controller.questiontile.QuestionListViewCell;
import nl.tudelft.oopp.application.data.FilterOption;
import nl.tudelft.oopp.application.data.Lecture;
import nl.tudelft.oopp.application.data.Question;
import nl.tudelft.oopp.application.data.SortOption;

@Log
public class QuestionArchiveController {

    @FXML
    private Label pageTitle;
    @FXML
    private Pane infoPane;
    @FXML
    private TextField enterAccessCode;
    @FXML
    private Button seeQuestions;
    @FXML
    private Button backButton;
    @FXML
    private Button exportQuestions;
    @FXML
    private Button browseButton;
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private ListView questionsList;
    @FXML
    public AnchorPane anchorPane;


    ObservableList<Question> questionsListData = FXCollections.observableArrayList();
    private SortOption sortOption = SortOption.upvotes;
    private FilterOption filterOption = FilterOption.all;
    private static Lecture lecture;

    /**
     * Displays the question archive window.
     */
    @SneakyThrows
    public static CompletableFuture<Void> display() {
        // load fxml
        FXMLLoader fxmlLoader = new FXMLLoader(
                QuestionArchiveController.class.getResource("/questionArchives.fxml"));
        Parent root = fxmlLoader.load();

        // create new window
        CompletableFuture<Void> future = new CompletableFuture();
        Platform.runLater(() -> {
            Stage stage = new Stage();
            stage.setTitle("Lecture archives");
            stage.setScene(new Scene(root));
            stage.show();
            future.complete(null);
        });

        return future;
    }

    /**
     * Initializes the page.
     */
    @FXML
    public void initialize() {
        exportQuestions.setVisible(false);
        scrollPane.setVisible(false);
        browseButton.setVisible(false);
        infoPane.setVisible(true);
    }

    /**
     * Retrieves the archived questions based on access code provided.
     */
    public CompletableFuture<Void> seeQuestions() {
        // fetch information
        return ServerCommunication.getLectureByJoinCode(
                enterAccessCode.getText())
            .thenAccept((pastLecture) -> Platform.runLater(() -> {
                // make questions visible
                lecture = pastLecture;
                infoPane.setVisible(false);
                scrollPane.setVisible(true);
                exportQuestions.setVisible(true);
                browseButton.setVisible(true);

                questionsList.setCellFactory(listView -> new QuestionListViewCell());

                System.out.println("received lecture info to set archive information "
                        + lecture.getName());
            }))
            .thenRun(this::displayQuestions);
    }


    /**
     * Displays the questions fetched.
     */
    public CompletableFuture<Void> displayQuestions() {
        //System.out.println("DEBUGER ->> " + filterOption + " " + sortOption);
        return ServerCommunication.listQuestions(
                lecture.getId(), filterOption, sortOption)
            .thenAccept((questions) -> {
                log.info("Fetched " + questions.size() + " questions!");

                // update list data
                Platform.runLater(() -> {
                    questionsListData.setAll(questions);
                });
            });
    }

    /**
     * Allows the user to return to creating a lecture.
     */
    public void goToHomePage() {
        JoinPageLecturerController.display();
        ((Stage) anchorPane.getScene().getWindow()).close();
    }

    /**
     * Exporting question to a specific location on local machine.
     */
    public void exportQuestionToADirectory() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialFileName("MyQuestion");
        fileChooser.getExtensionFilters()
                .add(new FileChooser.ExtensionFilter("text file", "*.txt"));

        try {
            File file = fileChooser.showSaveDialog(new Stage());
            writeToFile(file.getAbsolutePath());
        } catch (Exception e) {
            System.out.println("UNSAVED");
        }

    }

    /**
     * Write the question to the path.
     * @param absolutePath path to save the question.
     */
    public CompletableFuture<Void> writeToFile(String absolutePath) {
        return ServerCommunication.listQuestions(
                lecture.getId(), filterOption, sortOption)
            .thenAcceptAsync((questions) -> {
                try {
                    FileWriter fileWriter = new FileWriter(absolutePath);
                    for (Question question : questions) {
                        fileWriter.write(question.toString() + "\n\n");
                    }
                    fileWriter.close();
                    System.out.println("DATA WRITTEN SUCCESSFUL");
                } catch (IOException exception) {
                    throw new CompletionException(exception);
                }
            });
    }


    public void browse() {
        initialize();
    }

}
