package nl.tudelft.oopp.application;

import javafx.application.Platform;
import nl.tudelft.oopp.application.controller.JoinPageController;
import nl.tudelft.oopp.application.controller.JoinPageLecturerController;
import nl.tudelft.oopp.application.controller.dialog.JoinCodeDialogController;

public class MainApp {
    /**
     * Main Class for the application.
     * @param args Arguments which will be processed by the application
     */
    public static void main(String[] args) {
        Platform.startup(() -> initialize(args));
    }

    private static void initialize(String[] args) {
        if (args == null || args.length == 0) {
            JoinPageLecturerController.display();
        } else {
            JoinPageController.display(args[0]).thenAccept((result) -> {
                if (!result) {
                    JoinCodeDialogController.display();
                }
            });
        }
    }
}
