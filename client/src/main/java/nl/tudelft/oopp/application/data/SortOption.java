package nl.tudelft.oopp.application.data;

public enum SortOption {
    upvotes, time, relevancy
}
