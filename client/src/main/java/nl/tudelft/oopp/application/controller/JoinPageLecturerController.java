package nl.tudelft.oopp.application.controller;

import static java.util.concurrent.CompletableFuture.completedFuture;
import static java.util.concurrent.CompletableFuture.failedFuture;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import lombok.extern.java.Log;
import nl.tudelft.oopp.application.communication.ServerCommunication;
import nl.tudelft.oopp.application.communication.dtos.LectureCreateRequest;
import nl.tudelft.oopp.application.data.Lecture;

@Log
public class JoinPageLecturerController {
    @FXML
    private TextField courseName;
    @FXML
    private TextField lectureName;
    @FXML
    private TextField timeLecture;
    @FXML
    private Label fillInAllFieldsMessage;
    @FXML
    private AnchorPane joinScreen;
    @FXML
    private DatePicker datePicker;
    @FXML
    private Button createButton;

    private String lastTimeInput;

    /**
     * Displays the create lecture window.
     */
    @SneakyThrows
    public static CompletableFuture<Void> display() {
        // load fxml
        FXMLLoader fxmlLoader = new FXMLLoader(
                JoinPageLecturerController.class.getResource("/joinPageLecturer.fxml"));
        Parent root = fxmlLoader.load();

        // create new window
        CompletableFuture<Void> future = new CompletableFuture();
        Platform.runLater(() -> {
            Stage stage = new Stage();
            stage.setTitle("Create a new lecture");
            stage.setScene(new Scene(root));
            stage.show();
            stage.setMinHeight(500);
            stage.setMinWidth(819);
            future.complete(null);
        });

        return future;
    }

    @FXML
    void initialize() {
        // set time field to now
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        lastTimeInput = (hour < 10 ? "0" : "") + hour + ":" + (minute < 10 ? "0" : "") + minute;
        timeLecture.setText(lastTimeInput);

        // set date field to now
        datePicker.setValue(LocalDate.now());
    }

    /**
     * Allows the creation of a lecture room by a lecturer.
     */
    public void createLectureRoomClicked() {
        try {
            if (courseName == null || lectureName == null
                    || datePicker.getValue() == null || timeLecture == null) {
                fillInAllFieldsMessage.setText("Please fill in all the fields!");
            } else {
                // get date
                int year = datePicker.getValue().getYear();
                int month = datePicker.getValue().getMonthValue() - 1;
                int day = datePicker.getValue().getDayOfMonth();

                // get time
                Integer[] time = getChosenLectureTime();

                // parse time
                GregorianCalendar calendar = new GregorianCalendar();
                if (time == null) {
                    calendar.set(year, month, day);
                } else {
                    calendar.set(year, month, day, time[0], time[1]);
                }

                // create lecture
                Lecture resultLecture = ServerCommunication.createLecture(
                        new LectureCreateRequest(
                        lectureName.getText(), courseName.getText(), calendar.getTime()
                )).get();

                if (resultLecture.getTime().getTime() > new Date().getTime()) {
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setInitialFileName("MyLecture");
                    fileChooser.getExtensionFilters()
                            .add(new FileChooser.ExtensionFilter("text file", "*.txt"));

                    try {
                        File file = fileChooser.showSaveDialog(new Stage());
                        writeToFile(file.getAbsolutePath(), resultLecture);
                    } catch (Exception e) {
                        System.out.println("UNSAVED");
                    }

                    //reset all fields
                    initialize();
                    lectureName.setText("");
                    courseName.setText("");


                } else {
                    lectureCreated(resultLecture.getModeratorCode());
                    System.out.println("Successfully created lecture with name: "
                            + resultLecture.getName() + "\n"
                            + "Moderator Code: " + resultLecture.getModeratorCode() + "\n"
                            + "Student Code: " + resultLecture.getStudentCode() + "\n"
                            + resultLecture);

                }
            }
        } catch (Exception e) {
            log.warning("Error creating a room!");
            e.printStackTrace();
        }
    }

    /**
     * When a lecture is entered this will display the join window for moderator/lecturer.
     * @param moderatorCode the moderator code for the specific lecture
     */
    public CompletableFuture<Void> lectureCreated(String moderatorCode) {
        fillInAllFieldsMessage.setText("");
        return JoinPageController.display(moderatorCode)
            .thenAccept((success) -> {
                if (success) {
                    // close this window
                    Platform.runLater(() -> ((Stage) joinScreen.getScene().getWindow()).close());
                }
            });
    }

    /**
     * Write the question to the path.
     * @param absolutePath path to save the question.
     */
    public void writeToFile(String absolutePath, Lecture resultLecture) {
        try {
            String lectureInformation = "Moderator code:"
                    + "\n" + resultLecture.getModeratorCode()
                    + "\n\nStudent code:"
                    + "\n" + resultLecture.getStudentCode();
            FileWriter fileWriter = new FileWriter(absolutePath);
            fileWriter.write(lectureInformation);
            fileWriter.close();
            System.out.println("DATA WRITTEN SUCCESSFUL");
        } catch (Exception e) {
            System.out.println("SOMETHING WENT WRONG !!!");
        }

    }


    /**
     * Opens the lecture archive page.
     */
    public void seeArchive() {
        // open archive window
        QuestionArchiveController.display();

        // close this window
        ((Stage) joinScreen.getScene().getWindow()).close();
    }


    /**
     * Gets the lecture time chosen by the user.
     * @return Integer array.
     */
    private Integer[] getChosenLectureTime() {
        // verify not empty
        if (timeLecture.getText() == null || timeLecture.getText().length() == 0) {
            return null;
        }

        // get hour:minute
        Integer[] components = new Integer[2];
        try {
            List<Integer> numbers = Arrays.stream(timeLecture.getText().split(":"))
                    .map(Integer::parseInt).limit(2).collect(Collectors.toList());
            int i = 0;
            while (numbers.size() > 0) {
                components[i] = numbers.remove(0);
                i++;
            }
        } catch (Exception e) {
            return null;
        }

        // ensure constraints
        components[0] = Math.max(Math.min(components[0], 12), 0);
        components[1] = Math.max(Math.min(components[1], 60), 0);

        return components;
    }

    @FXML
    void onTimeTextChanged() {
        // check for illegal characters
        if (!isTimeInputValid(timeLecture.getText())) {
            int pos = timeLecture.getCaretPosition();
            timeLecture.setText(lastTimeInput);
            timeLecture.positionCaret(pos);
        } else {
            lastTimeInput = timeLecture.getText();
        }
    }

    private boolean isTimeInputValid(String timeInput) {
        // scan input for errors
        boolean hasSeparator = false;
        int consecutiveDigits = 0;
        Scanner sc = new Scanner(timeInput);
        sc.useDelimiter("");
        while (sc.hasNext()) {
            char character = sc.next().charAt(0);
            if (character == ':') {
                if (hasSeparator) {
                    // more than 1 separator
                    return false;
                } else {
                    // remember separator
                    hasSeparator = true;
                    consecutiveDigits = 0;
                }
            } else {
                // must be number
                if (!Character.isDigit(character)) {
                    return false;
                } else {
                    if (consecutiveDigits < 2) {
                        consecutiveDigits++;
                    } else {
                        // more than 2 consecutive digits
                        return false;
                    }
                }
            }
        }

        // time needs to have 1 separator :
        return hasSeparator;
    }

    /**
     * When "open saved lecture" is clicked this will read in the file containing the lecture code.
     */
    public void savedLectureClicked() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters()
                .add(new FileChooser.ExtensionFilter("text file", "*.txt"));

        try {
            File file = fileChooser.showOpenDialog(new Stage());
            readFromFile(file.getAbsolutePath());
        } catch (Exception e) {
            System.out.println("UNSAVED");
        }

    }

    /**
     * Reads the moderator code from the saved file.
     * @param absolutePath path where the .txt file with moderator code is saved
     */
    public void readFromFile(String absolutePath) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(absolutePath));
            br.readLine();
            String moderatorCode = br.readLine();
            lectureCreated(moderatorCode);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
