package nl.tudelft.oopp.application.communication.dtos;

import lombok.Data;

@Data
public class QuestionCreateRequest {
    private final long userID;
    private final String title;
    private final String content;
    private final long date;
}
