package nl.tudelft.oopp.application.communication.dtos;

import java.util.Date;
import lombok.Data;

@Data
public class LectureCreateRequest {
    private final String name;
    private final String course;
    private final Date time;

    /**
     * Constructor for LectureCreateRequest.
     * @param name The title of the lecture.
     * @param course The name of the course.
     * @param time The time the lecture will take place.
     */
    public LectureCreateRequest(String name, String course, Date time) {
        this.name = name;
        this.course = course;
        this.time = time;
    }
}
