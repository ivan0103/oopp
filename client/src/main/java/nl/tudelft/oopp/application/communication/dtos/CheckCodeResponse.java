package nl.tudelft.oopp.application.communication.dtos;

import java.util.Date;
import lombok.Data;
import nl.tudelft.oopp.application.data.UserRole;

@Data
public class CheckCodeResponse {
    /**
     * Whether the supplied code is correct.
     */
    private final boolean correct;
    /**
     * User role associated with the code.
     */
    private final UserRole userRole;
    /**
     * Whether the user was banned from this lecture.
     */
    private final boolean banned;
    /**
     * Date at which the lecture was scheduled if the lecture is not open yet.
     */
    private final Date scheduledDate;
}
