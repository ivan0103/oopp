package nl.tudelft.oopp.application.data;

import java.util.List;
import lombok.Data;

@Data
public class Poll {
    private long id;

    private String quiz;
    /**
     * options for the poll question.
     */
    private List<String> options;
    /**
     * for bit masking purposes.
     */
    private int answer;
    /**
     * for registering the responses from students.
     */
    private List<Integer> statistics;
    /**
     * for poll ended.
     */
    private boolean closed;
}
