package nl.tudelft.oopp.application.data;

import lombok.Data;

@Data
public class User {

    private long id;
    private String name;
    private String ipAddress;
    private UserRole role;

    public User(String name) {
        this.name = name;
    }

    public User() {
    }
}
