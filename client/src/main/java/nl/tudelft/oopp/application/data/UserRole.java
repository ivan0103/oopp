package nl.tudelft.oopp.application.data;

/**
 * Represents 3 main roles for user: Student, Moderator, and Lecturer.
 */
public enum UserRole { STUDENT, MODERATOR, LECTURER }
