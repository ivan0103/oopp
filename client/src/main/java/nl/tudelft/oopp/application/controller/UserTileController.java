package nl.tudelft.oopp.application.controller;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import lombok.extern.java.Log;
import nl.tudelft.oopp.application.data.User;

@Log
public class UserTileController {

    @FXML
    private Label firstLetterUser;
    @FXML
    private Label nameUser;
    @FXML
    private Label id;
    @FXML
    public HBox box;
    @FXML
    private User user;

    /**
     * Displays a new question tile.
     */
    public static CompletableFuture<UserTileController> display() {
        return CompletableFuture.supplyAsync(() -> {
            FXMLLoader fxmlLoader = new FXMLLoader(
                    UserTileController.class.getResource("/userTile.fxml"));
            try {
                fxmlLoader.load();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return fxmlLoader.getController();
        });
    }

    /**
     * Updates user information in the ui.
     * @param user user info.
     */
    public void setInfo(User user) {
        this.user = user;
        nameUser.setText(user.getName());
        firstLetterUser.setText(user.getName().substring(0, 1).toUpperCase());
        id.setText(user.getRole().name());
    }

    public HBox getBox() {
        return box;
    }
}
