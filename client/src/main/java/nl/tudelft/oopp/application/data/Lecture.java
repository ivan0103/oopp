package nl.tudelft.oopp.application.data;

import java.util.Date;
import lombok.Data;

@Data
public class Lecture {
    private long id;
    private String name;
    private String course;
    private String studentCode;
    private String moderatorCode;
    private boolean ended;
    private Date time;
    private Poll poll;
}
