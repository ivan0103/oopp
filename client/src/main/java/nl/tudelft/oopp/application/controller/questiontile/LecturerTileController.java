package nl.tudelft.oopp.application.controller.questiontile;

import static java.util.concurrent.CompletableFuture.completedFuture;
import static java.util.concurrent.CompletableFuture.failedFuture;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import lombok.extern.java.Log;
import nl.tudelft.oopp.application.communication.ServerCommunication;
import nl.tudelft.oopp.application.controller.LecturerPageController;
import nl.tudelft.oopp.application.controller.StudentPageController;
import nl.tudelft.oopp.application.data.Question;
import nl.tudelft.oopp.application.data.User;

@Log
public class LecturerTileController {

    @FXML
    public Label numQuestion;
    @FXML
    public Label questionTitle;
    @FXML
    public Label questionContent;
    @FXML
    public Label upvoteCount;
    @FXML
    public HBox box;

    @FXML
    public MenuItem answerQuestion;
    @FXML
    public MenuItem markAsAnswered;
    @FXML
    public MenuItem deleteQuestion;
    @FXML
    public MenuItem editQuestion;
    @FXML
    public MenuItem claimQuestion;
    @FXML
    public Label claimedByLabel;
    @FXML
    public Label answerLabel;
    @FXML
    private MenuButton questionMenu;

    private Question question;
    private LecturerPageController lecturerController;

    private static FXMLLoader fxmlLoader;

    /**
     * Create a new tile controller.
     */
    public static LecturerTileController display() {
        fxmlLoader = new FXMLLoader(
                LecturerTileController.class
                        .getResource("/questionTile/questionLecturerTile.fxml"));
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return fxmlLoader.getController();
    }

    public void setLecturerController(LecturerPageController controller) {
        this.lecturerController = controller;
    }

    /**
     * Updates displayed question info based on question data.
     * @param question The question data.
     */
    public void setInfo(int index, Question question) {
        this.question = question;
        numQuestion.setText("#");
        questionTitle.setText(question.getTitle());
        questionTitle.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        questionContent.setText(question.getContent()
                + (question.getAuthor() != null ? ("\n" + "Author: "
                + question.getAuthor().getName()) : ""));
        questionContent.setMinHeight(Region.USE_PREF_SIZE);
        questionContent.setWrapText(true);
        upvoteCount.setDisable(true);

        // claimed
        if (question.getClaimedBy() != null) {
            claimedByLabel.setText("Claimed by: "
                    + question.getClaimedBy().getName());
            // this user can not respond
            if (!question.getClaimedBy().equals(LecturerPageController.getUser())) {
                answerQuestion.setDisable(true);
            } else {
                answerQuestion.setDisable(false);
            }

        }

        // not claimed
        if (question.getClaimedBy() == null) {
            answerQuestion.setDisable(true);
        }

        // has response
        if (question.getAnswer() != null) {
            answerLabel.setText("Answer : " + question.getAnswer().getAnswer());
            answerLabel.setFont(Font.font("Arial", FontWeight.LIGHT, FontPosture.ITALIC, 13));
            answerLabel.setMinHeight(Region.USE_PREF_SIZE);
            answerLabel.setWrapText(true);
        }

        // already answered
        if (question.isAnswered()) {
            markAsAnswered.setText("Answered");
            claimedByLabel.setText("Answered");
            markAsAnswered.setDisable(true);
            claimQuestion.setDisable(true);
            answerQuestion.setDisable(true);
        }

        questionMenu.setOnShowing(e -> {
            System.out.println("This is now showing");
            question.setManipulated(true);
        });
        questionMenu.setOnHidden(e -> {
            System.out.println("This is now hidden");
            question.setManipulated(false);
        });
    }

    /**
     * Fetches the upvotes for a question.
     */
    private CompletableFuture<Void> fetchQuestionUpvotes() {
        return ServerCommunication.getQuestionUpvotes(
                question.getId(), StudentPageController.lecture.getId())
                .thenAccept((upvoters) -> {
                    Platform.runLater(() -> upvoteCount.setText(upvoters.size() + ""));
                })
                .exceptionally((e) -> {
                    log.warning("Error fetching upvotes for question: " + question.getId());
                    e.printStackTrace();
                    throw new CompletionException(e);
                });
    }

    /**
     * Marks a question as answered.
     */
    public CompletableFuture<Void> markAnswer() {
        // marks a question as answered
        return ServerCommunication.markAsAnswered(
                LecturerPageController.lecture.getId(), question.getId())
                .exceptionally((e) -> {
                    log.info("Error marking question as answered");
                    e.printStackTrace();
                    throw new CompletionException(e);
                });
    }

    /**
     * Allows the moderator to answer a question.
     */
    public void answerQuestion() {
        if (question.getClaimedBy() == null) {
            answerQuestion.setDisable(true);
        }
        lecturerController.showResponseWindow(question);
    }

    public HBox getBox() {
        return box;
    }

    @FXML
    CompletableFuture<Void> onClaimClicked() {
        return ServerCommunication.claimQuestion(question.getId(),
                LecturerPageController.getUser().getId())
                .exceptionally((e) -> {
                    log.info("Error claiming question");
                    e.printStackTrace();
                    throw new CompletionException(e);
                });
    }

    @FXML
    CompletableFuture<Void> onBanUserClicked() {
        return ServerCommunication.banUser(
                LecturerPageController.lecture.getId(),
                question.getAuthor().getId())
                .thenRun(() -> onDeleteClicked())
                .exceptionally((e) -> {
                    log.info("Error banning user");
                    e.printStackTrace();
                    throw new CompletionException(e);
                });
    }

    /**
     * Deletes the question when the delete button is clicked.
     */
    @FXML
    CompletableFuture<Void> onDeleteClicked() {
        long questionId = question.getId();
        return ServerCommunication.deleteQuestion(
                LecturerPageController.lecture.getId(), questionId)
                .exceptionally((e) -> {
                    log.info("Error deleting this question");
                    e.printStackTrace();
                    throw new CompletionException(e);
                });
    }

    /**
     * Edits the question when the edit menu button is clicked.
     */
    @FXML
    void edit() {
        lecturerController.editQuestion(question);
    }
}
