package nl.tudelft.oopp.application.controller;

import static java.util.concurrent.CompletableFuture.completedFuture;
import static java.util.concurrent.CompletableFuture.failedFuture;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import lombok.SneakyThrows;
import lombok.extern.java.Log;
import nl.tudelft.oopp.application.communication.ServerCommunication;
import nl.tudelft.oopp.application.communication.dtos.PollAnswerRequest;
import nl.tudelft.oopp.application.communication.dtos.QuestionCreateRequest;
import nl.tudelft.oopp.application.controller.questiontile.QuestionListViewCell;
import nl.tudelft.oopp.application.data.FilterOption;
import nl.tudelft.oopp.application.data.Lecture;
import nl.tudelft.oopp.application.data.Poll;
import nl.tudelft.oopp.application.data.Question;
import nl.tudelft.oopp.application.data.Reaction;
import nl.tudelft.oopp.application.data.SortOption;
import nl.tudelft.oopp.application.data.User;
import nl.tudelft.oopp.application.exception.UserBannedException;

@Log
public class StudentPageController {
    /**
     * Attributes from the mainPageStudent fxml file.
     */
    @FXML
    private TextArea enterQuestionTitle;
    @FXML
    private TextArea enterQuestionText;
    @FXML
    private Label lectureName;
    @FXML
    private Label courseName;
    @FXML
    private MenuButton nameUser;
    @FXML
    private Label firstLetterUser;
    @FXML
    public ListView<Question> questionsList;
    @FXML
    private Label pollQuestionTitle;
    @FXML
    private ListView<Button> pollOptionList;
    @FXML
    private BorderPane listUsers;
    @FXML
    private Label count;
    @FXML
    private ListView<User> listMembers;
    @FXML
    private BorderPane questionsBorderPane;

    ObservableList<User> usersListData = FXCollections.observableArrayList();

    /**
     * Store for the current questions data.
     */
    ObservableList<Question> questionsListData = FXCollections.observableArrayList();
    /**
     * Current user object.
     */
    public static User user;
    /**
     * Current lecture object.
     */
    public static Lecture lecture;

    /**
     * Option for sorting. Set UPVOTE as default sorting method
     */
    private SortOption sortOption = SortOption.upvotes;

    /**
     * Option for filtering. Set ALL as default filtering method
     */
    private FilterOption filterOption = FilterOption.unanswered;

    /**
     * Previous state of the lecture: Ended or not Ended.
     */
    private boolean disabled;
    /**
     * Previous closing state of poll.
     */
    private boolean pollClosed;
    /**
     * Previous removing state of poll.
     */
    private boolean pollRemoved;
    /**
     * Answer for the polls of this user.
     */
    private int pollAnswer;

    /**
     * Buttons to choose option from the polls.
     */
    private List<Button> optionButtons;

    private Timeline timeline;

    /**
     * Let's a specific user (student) join this main page.
     * @param user User information.
     * @param lecture Lecture information.
     */
    @SneakyThrows
    public static CompletableFuture<Void> display(User user, Lecture lecture) {
        // set info
        StudentPageController.user = user;
        StudentPageController.lecture = lecture;

        // load fxml
        FXMLLoader fxmlLoader = new FXMLLoader(
                StudentPageController.class.getResource("/mainPageStudent.fxml"));
        Parent root = fxmlLoader.load();

        // create new window
        CompletableFuture<Void> future = new CompletableFuture();
        Platform.runLater(() -> {
            Stage stage = new Stage();
            stage.setTitle(lecture.getName() + " - " + lecture.getCourse());
            stage.setScene(new Scene(root));
            stage.show();
            stage.setMinHeight(540);
            stage.setMinWidth(960);
            future.complete(null);
        });

        return future;
    }

    /**
     * Initializes the controller.
     */
    @FXML
    public void initialize() {
        updateUserInformation(user);
        updateLectureInformation(lecture);
        initQuestionsList();
        listUsers.setVisible(false);
        questionsBorderPane.setRight(null);
        initUserList();
        // Set up the time (in millisecond) to load lecture's information.
        autoLoadAll(1000);
    }

    /**
     * Auto load lecture's content.
     * @param milliSecond the frequency for reloading
     */
    public void autoLoadAll(int milliSecond) {
        timeline = new Timeline();
        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(milliSecond),
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent ae) {
                        updateLecture();
                        updateQuestions();
                        memberList();
                        //System.out.println("Updated!");
                    }
                }));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    /**
     * Update lecture information.
     */
    public CompletableFuture<Void> updateLecture() {
        return ServerCommunication.getLectureByJoinCode(

            lecture.getStudentCode()).thenAccept((lecture) -> {
                StudentPageController.lecture = lecture;
                this.updateLectureInformation(lecture);
            });
    }

    /**
     * Set option for sorting.
     * option = relevancy
     */
    public void setOptionRelevancy() {
        sortOption = SortOption.relevancy;
    }

    /**
     * Set option for sorting.
     * option = time
     */
    public void setOptionTime() {
        sortOption = SortOption.time;
    }

    /**
     * Let's a specific user (student) join this main page.
     * Set option for sorting.
     * option = upvote
     */
    public void setOptionUpvote() {
        sortOption = SortOption.upvotes;
    }

    /**
     * Updates user information in the ui.
     *
     * @param user user info.
     */
    private void updateUserInformation(User user) {
        nameUser.setText(user.getName());
        firstLetterUser.setText(user.getName().substring(0, 1).toUpperCase());
    }

    /**
     * Retrieves the lecture that the current user is in.
     */
    private void updateLectureInformation(Lecture lecture) {
        try {
            lectureName.setText(lecture.getName());
            lectureName.setMinSize(Label.USE_PREF_SIZE, Label.USE_PREF_SIZE);
            courseName.setText(lecture.getCourse());

            if (lecture.isEnded() && !disabled) {
                enterQuestionText.setDisable(true);
                enterQuestionTitle.setDisable(true);
                disabled = true;
            } else if (!lecture.isEnded() && disabled) {
                enterQuestionText.setDisable(false);
                enterQuestionTitle.setDisable(false);
                disabled = false;
            }

            updatePollInformation();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the bit at position pos(th) of the number num.
     * @param num the number to be extracted bit from.
     * @param pos the position to extract the bit from.
     * @return 0 or 1.
     */
    private int getBit(int num, int pos) {
        return (num >> pos) & 1;
    }

    /**
     * Show stats.
     */
    private void showStats() {
        System.out.println("SHOW STATS IS CALLED");
        Poll poll = lecture.getPoll();
        int answer = poll.getAnswer();
        int total = 0;
        int precision = 50;

        List<Integer> stats = poll.getStatistics();
        for (int i = 0; i < stats.size(); i++) {
            total += stats.get(i);
        }

        for (int i = 0; i < optionButtons.size(); i++) {
            Button button = optionButtons.get(i);
            button.setVisible(false);
        }
        optionButtons.clear();
        for (int i = 0; i < poll.getStatistics().size(); i++) {
            Button button = new Button();

            //System.out.println("Closed - " + i + ": " + getBit(answer, i));

            int percentage = 0;
            if (total != 0) {
                percentage = (int) ((double) stats.get(i) / total * 100.00);
            }

            button.setText(percentage + "% "
                    + generateString((int) (percentage / ((double) 100.00 / precision)), " "));


            if (getBit(answer, i) == 1) {
                button.setStyle("-fx-background-color:#008000;"
                        + " -fx-text-base-color: #b1b1b1; -fx-font-size:10.0");

            } else {
                //System.out.println("Checking red");
                button.setStyle("-fx-background-color:#921e38;"
                        + " -fx-text-base-color: #b1b1b1; -fx-font-size:10.0");
            }
            optionButtons.add(button);
        }

        pollOptionList.getItems().clear();
        pollOptionList.getItems().addAll(optionButtons);
        System.out.println("SIZE $$$$ ->>> " + pollOptionList.getItems().size());

    }

    /**
     * Generating a string of n repeated substring.
     * @param n the length of the result string.
     * @param str the substring.
     * @return a new string that is a repeats of old substring.
     */
    public String generateString(int n, String str) {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < n; i++) {
            res.append(str);
        }
        return res.toString();
    }

    /**
     * Update the poll information.
     * Check if poll is closed, removed, or ready to be fetched.
     */
    public void updatePollInformation() {
        Poll poll = lecture.getPoll();
        if (poll == null) {
            if (!pollRemoved) {
                // Reset the poll information.
                pollQuestionTitle.setText((char) 0 + "");
                pollQuestionTitle.setVisible(false);
                pollOptionList.setVisible(false);
                optionButtons = null;
                pollRemoved = true;
            }
        } else {
            pollRemoved = false;
            if (poll.isClosed() && !pollClosed) {
                showStats();
                pollClosed = true;
            } else if (!poll.isClosed()) {
                pollClosed = false;
                initPollInformation();
            }
        }

    }

    /**
     * In case the poll is ready to be fetched, this method will be called to fetch the
     * poll information to the screen.
     */
    private void initPollInformation() {
        Poll poll = lecture.getPoll();

        // This poll has already been initialized.
        if (pollQuestionTitle.getText().equals(poll.getQuiz())) {
            return;
        }

        // Init the poll information.
        pollQuestionTitle.setText(poll.getQuiz());
        pollQuestionTitle.setVisible(true);
        pollOptionList.setVisible(true);
        pollOptionList.getItems().clear();

        optionButtons = new ArrayList<>();
        for (int i = 0; i < poll.getOptions().size(); i++) {
            Button button = new Button((char) ('A' + i) + ". " + poll.getOptions().get(i));
            if (!poll.isClosed()) {
                button.setStyle("-fx-background-color:  #6D7EF7");
            }
            button.setOnAction(e -> {
                if (!poll.isClosed()) {
                    int pos = button.getText().charAt(0) - 'A';
                    ServerCommunication.answerPoll(lecture.getId(),
                            new PollAnswerRequest(pos, user.getId()));
                    button.setStyle("-fx-background-color:#FF8C00");
                    button.setDisable(true);
                }
            });
            optionButtons.add(button);
        }
        pollOptionList.getItems().addAll(optionButtons);
        pollOptionList.setCellFactory(param -> new ListCell<Button>() {
            @Override
            protected void updateItem(Button item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null) {
                    setText(null);
                    setStyle("-fx-background-color: #7d7d7d;");
                } else {
                    //setText(item.getText());
                    setStyle("-fx-background-color: #7d7d7d;");
                    setGraphic(item);
                }
            }
        });
    }

    /**
     * Initializes the questions list.
     */
    public void initQuestionsList() {
        questionsList.setEditable(false);
        questionsList.setSelectionModel(null);
        questionsList.setCellFactory(listView -> new QuestionListViewCell());
        questionsList.setItems(questionsListData);
        updateQuestions();
    }

    /**
     * Fetches all the questions of this lecture and displays them on the screen.
     */
    public CompletableFuture<Void> updateQuestions() {
        return ServerCommunication
                .listQuestions(lecture.getId(), filterOption, sortOption)
                .thenAccept((questions -> {
                    // update list data
                    Platform.runLater(() -> {
                        questionsListData.setAll(questions);
                    });
                }))
                .exceptionally((e) -> {
                    if (e instanceof UserBannedException) {
                        // user is banned, show alert and close page
                        timeline.stop();
                        Platform.runLater(() -> {
                            Alert alert = new Alert(Alert.AlertType.WARNING,
                                    "You were banned from this lecture!\nThis window will close.",
                                    ButtonType.CLOSE);
                            alert.show();
                            ((Stage) questionsList.getScene().getWindow()).close();
                        });
                    }
                    throw new CompletionException(e);
                });
    }

    /**
     * Creates a question after a student submitted one via the enterQuestionButton.
     */
    public CompletableFuture<Void> sendQuestionClicked() {
        // create question request
        return ServerCommunication.createQuestion(new QuestionCreateRequest(
                user.getId(), enterQuestionTitle.getText(), enterQuestionText.getText(),
                new Date().getTime()), lecture.getId())
                .thenAccept((question -> {
                    if (question != null) {
                        System.out.println("Successfully created question\n"
                                + "Title: " + question.getTitle() + "\n"
                                + "Description: " + question.getContent());
                    }

                    // clear input fields
                    enterQuestionText.clear();
                    enterQuestionTitle.clear();
                }))
                .thenRun(this::updateQuestions);
    }

    /**
     * Copy student link to the clip board when the user click on this.
     */
    public void inviteStudentClicked() {
        writeToClipboard(lecture.getStudentCode(), null);
        System.out.println("Copied student invite link");
    }

    /**
     * Write something to system clipboard.
     * @param s String to write to the clipboard
     * @param owner Owner of the clipboard
     */
    public void writeToClipboard(String s, ClipboardOwner owner) {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable transferable = new StringSelection(s);
        clipboard.setContents(transferable, owner);
    }

    /**
     * Creates a Reaction after a student presses the Slow-Down(Turtle) button.
     */
    public CompletableFuture<Reaction> slower() {
        return ServerCommunication.speedReactionSlower(
                StudentPageController.lecture.getId(), true)
                .exceptionally((e) -> {
                    log.info("Error reacting with too-slow!");
                    e.printStackTrace();
                    throw new CompletionException(e);
                });
    }

    /**
     * Creates a Reaction after a student presses the Speed-Up(Rocket) button.
     */
    public CompletableFuture<Reaction> faster() {
        return ServerCommunication.speedReactionFaster(
                StudentPageController.lecture.getId(), false)
                .exceptionally((e) -> {
                    log.info("Error reacting with too-fast!");
                    e.printStackTrace();
                    throw new CompletionException(e);
                });
    }

    /**
     * Initializes the users list.
     */
    public void initUserList() {
        listMembers.setEditable(false);
        listMembers.setSelectionModel(null);
        listMembers.setCellFactory(listView -> new UserListViewCell());
        listMembers.setItems(usersListData);
        memberList();
    }

    /**
     * Shows the response window for lecturer.
     */
    public void showUserList() {
        listUsers.setVisible(true);
        questionsBorderPane.setRight(listUsers);
        questionsBorderPane.setCenter(questionsList);
    }

    /**
     * Closes user list.
     */
    public void closeListMembers() {
        questionsBorderPane.setRight(null);
        questionsBorderPane.setCenter(questionsList);
        listUsers.setVisible(false);
    }

    /**
     * Fetches all the members of this lecture and displays them on the screen.
     */
    public CompletableFuture<Void> memberList() {
        // fetch users from the server
        return ServerCommunication.listUsers(lecture.getId())
                .thenAccept((users) -> {
                    // update user data
                    Platform.runLater(() -> {
                        count.setText(String.valueOf(usersListData.size()));
                        usersListData.setAll(users);
                    });
                });
    }

    /**
     * Filters by answered.
     */
    public void filterAnswered() {
        filterOption = FilterOption.answered;
    }

    /**
     * Filters by unanswered.
     */
    public void filterUnanswered() {
        filterOption = FilterOption.unanswered;
    }
}
