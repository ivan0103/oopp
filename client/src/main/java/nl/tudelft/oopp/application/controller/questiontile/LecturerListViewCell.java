package nl.tudelft.oopp.application.controller.questiontile;

import javafx.scene.control.ListCell;
import nl.tudelft.oopp.application.controller.LecturerPageController;
import nl.tudelft.oopp.application.data.Question;


public class LecturerListViewCell extends ListCell<Question> {
    private LecturerPageController lecturerController;
    private LecturerTileController tileController;

    /**
     * Creates a new list view cell.
     * @param controller the lecture page controller.
     */
    public LecturerListViewCell(LecturerPageController controller) {
        this.lecturerController = controller;
        this.tileController = LecturerTileController.display();
        this.tileController.setLecturerController(lecturerController);
    }

    @Override
    public void updateItem(Question question, boolean empty) {
        super.updateItem(question, empty);
        if (empty || question == null) {
            setGraphic(null);
        } else {
            tileController.setInfo(1, question);
            setGraphic(tileController.getBox());
        }
    }
}
