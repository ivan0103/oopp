package nl.tudelft.oopp.application.communication.dtos;

import lombok.Data;

@Data
public class QuestionEditRequest {
    private long userID;
    private String content;

    public QuestionEditRequest(long userId, String content) {
        this.userID = userId;
        this.content = content;
    }
}
