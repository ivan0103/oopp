package nl.tudelft.oopp.application.communication.dtos;

import lombok.Data;
import nl.tudelft.oopp.application.data.Lecture;
import nl.tudelft.oopp.application.data.User;

@Data
public class LectureJoinResponse {
    private final Lecture lecture;
    private final User user;
}
