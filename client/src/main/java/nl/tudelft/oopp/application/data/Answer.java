package nl.tudelft.oopp.application.data;

import lombok.Data;

@Data
public class Answer {
    private long id;
    private String answer;
}
