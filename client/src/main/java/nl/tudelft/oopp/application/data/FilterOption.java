package nl.tudelft.oopp.application.data;

public enum FilterOption {
    answered, unanswered, all;
}
