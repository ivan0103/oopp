package nl.tudelft.oopp.application.exception;

import java.util.concurrent.CompletionException;
import lombok.Getter;

@Getter
public class ServerErrorException extends CompletionException {
    private final int status;

    public ServerErrorException(int statusCode) {
        this.status = statusCode;
    }
}
