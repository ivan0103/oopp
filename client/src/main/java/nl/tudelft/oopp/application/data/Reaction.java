
package nl.tudelft.oopp.application.data;

import lombok.Data;

@Data
public class Reaction {
    private long id;
    private boolean type;
}