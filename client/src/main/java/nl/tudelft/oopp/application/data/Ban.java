package nl.tudelft.oopp.application.data;

import lombok.Data;

@Data
public class Ban {
    @SuppressWarnings("checkstyle:JavadocVariable")
    private final String id;
    /**
     * The IP of the user that was banned.
     */
    private final String banIp;
    /**
     * When was this ban created.
     */
    private final String timestamp;

    /**
     * Empty Constructor.
     */
    public Ban() {
        this.id = "";
        this.banIp = "";
        this.timestamp = "";
    }
}
