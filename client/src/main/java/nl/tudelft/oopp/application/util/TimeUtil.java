package nl.tudelft.oopp.application.util;

import java.text.SimpleDateFormat;

public class TimeUtil {
    /**
     * Formats a date into a date string format.
     */
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
    /**
     * Formats a date into a time string format.
     */
    public static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");
}
