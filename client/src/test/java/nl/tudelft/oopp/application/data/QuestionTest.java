package nl.tudelft.oopp.application.data;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import org.junit.jupiter.api.Test;

class QuestionTest {

    @Test
    void writeToFile() {
        Question question = new Question();
        question.setId(4);
        question.setTitle("Testing the title");
        question.setContent("Testing the content");
        question.setAnswered(true);
        question.setAuthor(new User("Tester"));
        Answer answer = new Answer();
        answer.setAnswer("Testing the answer");
        question.setAnswer(answer);

        String writeToFileTest = "Question id: \t4"
                + "\nTitle: \t\tTesting the title"
                + "\nContent: \tTesting the content"
                + "\nAnswered: \tyes"
                + "\nClaimed by: \tnot claimed"
                + "\nUpvotes: \t0"
                + "\nAuthor: \tTester"
                + "\nResponse: \tTesting the answer";

        assertEquals(writeToFileTest, question.writeToFile());

    }
}