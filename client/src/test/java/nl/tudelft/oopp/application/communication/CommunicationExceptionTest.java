package nl.tudelft.oopp.application.communication;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import nl.tudelft.oopp.application.communication.dtos.AnswerCreateRequest;
import nl.tudelft.oopp.application.communication.dtos.LectureCreateRequest;
import nl.tudelft.oopp.application.communication.dtos.PollAnswerRequest;
import nl.tudelft.oopp.application.communication.dtos.PollCreateRequest;
import nl.tudelft.oopp.application.communication.dtos.QuestionCreateRequest;
import nl.tudelft.oopp.application.data.FilterOption;
import nl.tudelft.oopp.application.data.Lecture;
import nl.tudelft.oopp.application.data.SortOption;
import nl.tudelft.oopp.application.data.User;
import org.junit.jupiter.api.Test;

public class CommunicationExceptionTest {
    @Test
    void createLectureException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.createLecture(new LectureCreateRequest(
                    "name", "course", new Date())).get();
        });
    }

    @Test
    void joinLectureException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.joinLecture(new User("name"), "code").get();
        });
    }

    @Test
    void getLectureByJoinCodeException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.getLectureByJoinCode("code").get();
        });
    }

    @Test
    void createQuestionException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.createQuestion(new QuestionCreateRequest(
                    -1, "title", "content", -1), -1).get();
        });
    }

    @Test
    void deleteQuestionException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.deleteQuestion(-1, -1).get();
        });
    }

    @Test
    void deleteReactionException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.deleteReaction(-1, -1).get();
        });
    }

    @Test
    void listQuestionException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.listQuestions(
                    -1, FilterOption.answered, SortOption.relevancy).get();
        });
    }

    @Test
    void upvoteQuestionException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.upvoteQuestion(-1, -1, -1).get();
        });
    }

    @Test
    void getQuestionUpvotesException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.getQuestionUpvotes(-1, -1).get();
        });
    }

    @Test
    void checkJoinCodeException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.checkJoinCode("code").get();
        });
    }

    @Test
    void answerQuestionException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.answerQuestion(new AnswerCreateRequest(
                    -1, "content"), -1,-1).get();
        });
    }

    @Test
    void markAsAnsweredException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.markAsAnswered(-1,-1).get();
        });
    }

    @Test
    void claimQuestionException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.claimQuestion(-1,-1).get();
        });
    }

    @Test
    void speedReactionFasterException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.speedReactionFaster(-1,true).get();
        });
    }

    @Test
    void speedReactionSlowerException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.speedReactionSlower(-1,true).get();
        });
    }

    @Test
    void listReactionException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.listReaction(-1).get();
        });
    }

    @Test
    void endLectureException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.endLecture(new Lecture()).get();
        });
    }

    @Test
    void createPollException() {
        assertThrows(Exception.class, () -> {
            List<String> options = new ArrayList<>();
            ServerCommunication.createPoll(
                    -1, new PollCreateRequest("Quiz",
                            options, -1, "code")).get();
        });
    }

    @Test
    void closePollException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.closePoll(-1, "code").get();
        });

    }

    @Test
    void removePollException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.removePoll(-1, "code").get();
        });

    }

    @Test
    void answerPollException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.answerPoll(-1,
                    new PollAnswerRequest(-1, -1)).get();
        });

    }

    @Test
    void banUserException() {
        assertThrows(Exception.class, () -> {
            ServerCommunication.banUser(-1, -1).get();
        });

    }
}
