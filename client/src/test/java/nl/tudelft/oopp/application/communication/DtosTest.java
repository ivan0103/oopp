package nl.tudelft.oopp.application.communication;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Date;
import nl.tudelft.oopp.application.communication.dtos.AnswerCreateRequest;
import nl.tudelft.oopp.application.communication.dtos.CheckCodeResponse;
import nl.tudelft.oopp.application.communication.dtos.LectureCreateRequest;
import nl.tudelft.oopp.application.communication.dtos.LectureJoinResponse;
import nl.tudelft.oopp.application.communication.dtos.PollAnswerRequest;
import nl.tudelft.oopp.application.communication.dtos.QuestionCreateRequest;
import nl.tudelft.oopp.application.communication.dtos.QuestionEditRequest;
import nl.tudelft.oopp.application.communication.dtos.QuestionUpvoteRequest;
import nl.tudelft.oopp.application.data.Lecture;
import nl.tudelft.oopp.application.data.User;
import nl.tudelft.oopp.application.data.UserRole;
import org.junit.jupiter.api.Test;


public class DtosTest {

    @Test
    void answerCreateRequestTest() {
        assertNotNull(new AnswerCreateRequest(1, "content"));
    }

    @Test
    void checkCodeResponseTest() {
        assertNotNull(new CheckCodeResponse(
                true, UserRole.MODERATOR, true, new Date()));
    }

    @Test
    void lectureCreateRequestTest() {
        assertNotNull(new LectureCreateRequest("name", "course", new Date()));
    }

    @Test
    void lectureJoinResponseTest() {
        assertNotNull(new LectureJoinResponse(new Lecture(), new User("name")));
    }

    @Test
    void pollAnswerRequestTest() {
        assertNotNull(new PollAnswerRequest(1, 1));
    }

    @Test
    void questionCreateRequest() {
        assertNotNull(new QuestionCreateRequest(
                1, "title", "content", 0));
    }

    @Test
    void questionEditRequest() {
        assertNotNull(new QuestionEditRequest(-1, "content"));
    }

    @Test
    void questionUpvoteRequest() {
        assertNotNull(new QuestionUpvoteRequest(1));
    }
}
