package nl.tudelft.oopp.application.communication;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class SampleTest {
    @Test
    public void testSample() {
        assertEquals("Sample", "Sample");
    }
}
