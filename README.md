Client: ![Client coverage](https://gitlab.ewi.tudelft.nl/cse1105/2019-2020/organisation/repository-template/badges/master/coverage.svg?job=client-test)
Server: ![Server coverage](https://gitlab.ewi.tudelft.nl/cse1105/2019-2020/organisation/repository-template/badges/master/coverage.svg?job=server-test)


# Starting template

This README will need to contain a description of your project, how to run it, how to set up the development environment, and who worked on it.
This information can be added throughout the course, except for the names of the group members.
Add your own name (do not add the names for others!) to the section below.

## Description of project

## Group members


| 📸 | Name | Email |
|---|---|---|
| <img src="https://www.petguide.com/wp-content/uploads/2017/12/Weird-cat-remake-Home-Alone.jpg" alt="drawing" width="75" height = "50"/> | Tan Nguyen | NguyenManhTan@student.tudelft.nl |
| ![](https://eu.ui-avatars.com/api/?name=JS&length=4&size=50&color=DDD&background=777&font-size=0.325) | Juul Schnitzler | J.B.Schnitzler@student.tudelft.nl |
| ![](https://eu.ui-avatars.com/api/?name=JJ&length=4&size=50&color=DDD&background=777&font-size=0.325) | Jan Jaap van Laar | J.J.vanlaar@student.tudelft.nl |
| ![](https://eu.ui-avatars.com/api/?name=IV&length=4&size=50&color=000&background=FBB&font-size=0.325) | Ivan Virovski | I.Virovski-1@student.tudelft.nl |
| ![](https://eu.ui-avatars.com/api/?name=MR&length=4&size=50&color=FFF&background=c60337&font-size=0.325) | Mateusz Rębacz | M.Rebacz@student.tudelft.nl |
| ![](https://eu.ui-avatars.com/api/?name=CC&length=4&size=50&color=FFF&background=6272a4&font-size=0.325) | Chao Chen | C.Chen-18@student.tudelft.nl |
<!-- Instructions (remove once assignment has been completed -->
<!-- - Add (only!) your own name to the table above (use Markdown formatting) -->
<!-- - Mention your *student* email address -->
<!-- - Preferably add a recognisable photo, otherwise add your GitLab photo -->
<!-- - (please make sure the photos have the same size) --> 

## How to run it

## How to contribute to it

## Copyright / License (opt.)
